library cassandra_scylla_cortex.tests.connection;

import 'dart:async';
import 'dart:io';
import 'package:cassandra_scylla_cortex/cql.dart' as cql;
import 'package:cassandra_scylla_cortex/src/exceptions.dart' as cql_ex;
import 'package:test/test.dart';
import 'mocks/compression.dart' as compress;
import 'mocks/mocks.dart' as mock;

void main({bool enableLogger = false}) {
  if (enableLogger) {
    mock.initLogger();
  }

  const String serverHost = '127.0.0.1';
  const int serverPort = 32000;
  final mock.MockServer server = mock.MockServer();
  cql.Connection? conn;

  group('Connection', () {
    setUp(() {
      conn = null;
      return server.listen(serverHost, serverPort);
    });

    tearDown(() {
      server.compression = null;
      final List<Future> cleanupFutures = [server.shutdown()];
      if (conn != null) {
        cleanupFutures.add(conn!.close(drain: false));
      }

      return Future.wait(cleanupFutures);
    });

    test('frame parsing exception should be wrapped in an ExceptionMessage',
        () async {
      final cql.PoolConfiguration config = cql.PoolConfiguration();
      // We have modified this event message to use stream id 0 and
      // include a malformed INET field. This should trigger an exception when we try to parse it
      server.setReplayList(['malformed_frame_v3.dump']);
      conn = cql.Connection('conn-0', serverHost, serverPort, config: config);

      void handleError(Exception e, trace) {
        expect(e, isA<Exception>());
        expect(
          e.toString(),
          equals('Exception: Could not decode INET type of length 6'),
        );
        expect(trace, isNotNull);
      }

      try {
        await conn!.open();
        await conn!.execute(cql.Query('SELECT * FROM test'));
      } on Exception catch (e, trace) {
        handleError(e, trace);
      }
    });

    test('stream reservation timeout exception', () async {
      final cql.PoolConfiguration config = cql.PoolConfiguration(
        protocolVersion: cql.ProtocolVersion.v2,
        streamReservationTimeout: const Duration(milliseconds: 10),
        streamsPerConnection: 1,
      );
      conn = cql.Connection('conn-0', serverHost, serverPort, config: config);

      void handleError(e) {
        expect(e, isA<cql_ex.StreamReservationException>());
        expect(e.toString(), startsWith('StreamReservationException'));
      }

      try {
        await conn!.open();
        conn!.execute(cql.Query('SELECT * FROM test')).catchError((_) {});
        await Future.delayed(
          const Duration(milliseconds: 10),
          () => conn!.execute(cql.Query('SELECT * FROM test')),
        );
      } catch (error) {
        handleError(error);
      }
    });

    test('V2 handshake', () {
      final cql.PoolConfiguration config =
          cql.PoolConfiguration(protocolVersion: cql.ProtocolVersion.v2);
      conn = cql.Connection('conn-0', serverHost, serverPort, config: config);
      return conn!.open();
    });

    test('V2 handshake (using default pool configuration)', () {
      conn = cql.Connection('conn-0', serverHost, serverPort);
      return conn!.open();
    });

    test('V2 handshake and keyspace selection', () {
      server.setReplayList(['void_result_v2.dump']);
      final cql.PoolConfiguration config =
          cql.PoolConfiguration(protocolVersion: cql.ProtocolVersion.v2);
      conn = cql.Connection('conn-0', serverHost, serverPort, config: config);
      return conn!.open();
    });

    test('V3 handshake', () {
      final cql.PoolConfiguration config = cql.PoolConfiguration();
      conn = cql.Connection('conn-0', serverHost, serverPort, config: config);
      return conn!.open();
    });

    test('lost exception', () async {
      final cql.PoolConfiguration config = cql.PoolConfiguration(
        maxConnectionAttempts: 1,
        connectionTimeout: const Duration(milliseconds: 100),
      );
      conn = cql.Connection('conn-0', serverHost, serverPort, config: config);

      try {
        await conn!.open();
        await server.shutdown();
        await conn!.execute(cql.Query('SELECT * FROM foo'));
        expect(false, isTrue);
      } catch (error) {
        expect(error, isA<cql_ex.ConnectionFailedException>());
      }
    });

    test('default keyspace (V2)', () async {
      server.setReplayList(['set_keyspace_v2.dump']);
      final cql.PoolConfiguration config = cql.PoolConfiguration();
      conn = cql.Connection(
        'conn-0',
        serverHost,
        serverPort,
        config: config,
        defaultKeyspace: 'test',
      );

      await expectLater(conn!.open(), completes);
      await expectLater(conn!.close(), completes);
    });

    test('query (V2)', () {
      server.setReplayList(['select_v2.dump']);
      final cql.PoolConfiguration config = cql.PoolConfiguration();
      conn = cql.Connection('conn-0', serverHost, serverPort, config: config);
      expect(
        () async {
          await conn!.open();
          return conn!.execute(cql.Query('SELECT * from test.type_test'));
        }(),
        completion((cql.RowsResultMessage res) {
          expect(res.rows.length, equals(1));
          final Map<String, Object?> row = res.rows.first;
          final Map<String, Object?> expectedValues = {
            'ascii_type': 'text4',
            'bigint_type': 9223372036854775807,
            'bool_type': true,
            'inet_type': InternetAddress('192.168.169.102'),
            'int_type': 32238493,
            'list_type': [100, 200],
            'map_type': {100: 'the test', 200: 'the result'},
            'set_type': [100, 200],
            'text_type': 'This is a long UTF8 κείμενο',
            'uuid_type': cql.Uuid('550e8400-e29b-41d4-a716-446655440000'),
            'varchar_type': 'Arbitary long text goes here',
            'varint_type':
                BigInt.parse('-3123091212904812093120938120938120312890'),
          };
          expectedValues.forEach((String? fieldName, Object? fieldValue) {
            expect(row[fieldName], equals(fieldValue));
          });
          return true;
        }),
      );
    });

    test('query (V3)', () {
      server.setReplayList(['select_v3.dump']);
      final cql.PoolConfiguration config = cql.PoolConfiguration();
      conn = cql.Connection('conn-0', serverHost, serverPort, config: config);
      expect(
        () async {
          await conn!.open();
          return conn!.execute(cql.Query('SELECT * from test.user_profiles'));
        }(),
        completion((cql.RowsResultMessage res) {
          expect(res.rows.length, equals(1));
          final Map<String, Object?> row = res.rows.first;
          final Map<String, Object?> expectedValues = {
            'login': 'test_user',
            'addresses': {
              'home': {
                'street': '123 Test Str.',
                'city': 'San Fransisco',
                'zip': 94110,
                'phones': [
                  {
                    'number': '123 444 5555',
                    'tags': ['direct line', 'preferred']
                  },
                  {
                    'number': '123 444 6666',
                    'tags': ['fax']
                  }
                ]
              }
            },
            'email': 'tuser@test.com',
            'first_name': 'Test',
            'last_name': 'User'
          };
          expectedValues.forEach((String? fieldName, Object? fieldValue) {
            expect(row[fieldName], equals(fieldValue));
          });
          return true;
        }),
      );
    });

    group('with compression:', () {
      group('mock-SNAPPY:', () {
        setUp(() {
          server.compression = cql.Compression.snappy;
        });

        tearDown(() {
          cql.unregisterCodec(cql.CompressionExt().encode(cql.Compression.lz4));
          cql.unregisterCodec(
            cql.CompressionExt().encode(cql.Compression.snappy),
          );
        });

        test('Codec encode exception handling', () async {
          // Register codec for the mock server
          cql.registerCodec(
            cql.CompressionExt().encode(cql.Compression.snappy),
            compress.MockCompressionCodec(true),
          );

          final cql.PoolConfiguration config = cql.PoolConfiguration(
            protocolVersion: cql.ProtocolVersion.v2,
            compression: cql.Compression.snappy,
          );

          conn =
              cql.Connection('conn-0', serverHost, serverPort, config: config);

          void handleError(Exception e) {
            expect(
              e.toString(),
              startsWith(
                "DriverException: An error occurred while invoking '${cql.Compression.snappy}' codec (compression)",
              ),
            );
          }

          try {
            await conn!.open();
            await conn!.execute(cql.Query('SELECT * from test.type_test'));
          } on Exception catch (e) {
            handleError(e);
          }
        });

        test('server responds with unexpected compressed frame', () async {
          server.setReplayList(['void_result_v2.dump']);

          // Register codec for the mock server
          cql.registerCodec(
            cql.CompressionExt().encode(cql.Compression.snappy),
            compress.MockCompressionCodec(),
          );

          final cql.PoolConfiguration config =
              cql.PoolConfiguration(protocolVersion: cql.ProtocolVersion.v2);

          conn =
              cql.Connection('conn-0', serverHost, serverPort, config: config);

          void handleError(Exception e) {
            expect(
              e.toString(),
              equals(
                'DriverException: Server responded with an unexpected compressed frame',
              ),
            );
          }

          try {
            await conn!.open();
            await conn!.execute(cql.Query('SELECT * from test.type_test'));
          } on Exception catch (e) {
            handleError(e);
          }
        });

        test('Using a compression codec', () {
          server.setReplayList(['void_result_v2.dump']);

          // Register codec
          cql.registerCodec(
            cql.CompressionExt().encode(cql.Compression.snappy),
            compress.MockCompressionCodec(),
          );

          final cql.PoolConfiguration config = cql.PoolConfiguration(
            protocolVersion: cql.ProtocolVersion.v2,
            compression: cql.Compression.snappy,
          );

          conn =
              cql.Connection('conn-0', serverHost, serverPort, config: config);

          expect(
            () async {
              await conn!.open();
              return conn!.execute(cql.Query('SELECT * from test.type_test'));
            }(),
            completion((cql.ResultMessage res) {
              return res is cql.VoidResultMessage;
            }),
          );
        });
      });

      group('mock-LZ4:', () {
        setUp(() {
          server.compression = cql.Compression.lz4;
        });

        tearDown(() {
          cql.unregisterCodec(cql.CompressionExt().encode(cql.Compression.lz4));
          cql.unregisterCodec(
            cql.CompressionExt().encode(cql.Compression.snappy),
          );
        });

        test('Using a compression codec', () {
          server.compression = cql.Compression.lz4;
          server.setReplayList(['void_result_v2.dump']);

          // Register codec
          cql.registerCodec(
            cql.CompressionExt().encode(cql.Compression.lz4),
            compress.MockCompressionCodec(),
          );

          final cql.PoolConfiguration config = cql.PoolConfiguration(
            protocolVersion: cql.ProtocolVersion.v2,
            compression: cql.Compression.lz4,
          );
          conn =
              cql.Connection('conn-0', serverHost, serverPort, config: config);

          expect(
            () async {
              await conn!.open();
              return conn!.execute(cql.Query('SELECT * from test.type_test'));
            }(),
            completion((cql.ResultMessage res) {
              return res is cql.VoidResultMessage;
            }),
          );
        });

        test('Codec decode exception handling', () async {
          server.setReplayList(['void_result_v2.dump']);

          // Register a mock codec for the mock server (pretend its LV4) and the SNAPPY codec
          // with throw on decode for our client. When we receive the server response a decode exception
          // will be thrown
          cql.registerCodec(
            cql.CompressionExt().encode(cql.Compression.lz4),
            compress.MockCompressionCodec(),
          );
          cql.registerCodec(
            cql.CompressionExt().encode(cql.Compression.snappy),
            compress.MockCompressionCodec(false, true),
          );

          final cql.PoolConfiguration config = cql.PoolConfiguration(
            protocolVersion: cql.ProtocolVersion.v2,
            compression: cql.Compression.snappy,
          );

          conn = cql.Connection(
            'conn-0',
            serverHost,
            serverPort,
            config: config,
          );

          void handleError(Exception e) {
            expect(
              e.toString(),
              startsWith(
                "DriverException: An error occurred while invoking '${cql.Compression.snappy}' codec (decompression)",
              ),
            );
          }

          try {
            await conn!.open();
            await conn!.execute(cql.Query('SELECT * from test.type_test'));
          } on Exception catch (e) {
            handleError(e);
          }
        });

        test('Missing compression codec exception', () {
          expect(
            () => cql.PoolConfiguration(
              protocolVersion: cql.ProtocolVersion.v2,
              compression: cql.Compression.snappy,
            ),
            throwsA(
              (e) =>
                  e is ArgumentError &&
                  e.message ==
                      "A compression codec needs to be registered via registerCodec() for type 'Compression.snappy'",
            ),
          );
        });
      });
    });
  });
}
