library cassandra_scylla_cortex.tests.type_guess;

import 'dart:io';
import 'dart:typed_data';
import 'package:cassandra_scylla_cortex/src/types.dart';
import 'package:test/test.dart';
import 'mocks/custom.dart' as custom;
import 'mocks/mocks.dart' as mock;

void main({bool enableLogger = false}) {
  if (enableLogger) {
    mock.initLogger();
  }

  group('Collection type:', () {
    test('isCollection(LIST)', () {
      expect(DataTypeExt().isCollection(DataType.set), isTrue);
    });
    test('isCollection(SET)', () {
      expect(DataTypeExt().isCollection(DataType.list), isTrue);
    });
    test('isCollection(MAP)', () {
      expect(DataTypeExt().isCollection(DataType.map), isTrue);
    });
    test('isCollection(TUPLE)', () {
      expect(DataTypeExt().isCollection(DataType.tuple), isFalse);
    });
  });

  group('TypeSpec.toString():', () {
    test('ASCII', () {
      final TypeSpec ts = TypeSpec(DataType.ascii);
      expect(ts.toString(), equals('ascii'));
    });

    test('CUSTOM', () {
      final custom.CustomJson customJson = custom.CustomJson({});
      final TypeSpec ts = TypeSpec(DataType.custom)
        ..customTypeClass = customJson.customTypeClass;
      expect(
        ts.toString(),
        equals('CustomType<${customJson.customTypeClass}>'),
      );
    });

    test('LIST', () {
      final TypeSpec ts =
          TypeSpec(DataType.list, valueSubType: TypeSpec(DataType.inet));
      expect(ts.toString(), equals('List<inet>'));
    });

    test('SET', () {
      final TypeSpec ts =
          TypeSpec(DataType.set, valueSubType: TypeSpec(DataType.timestamp));
      expect(ts.toString(), equals('Set<timestamp>'));
    });

    test('MAP', () {
      final TypeSpec ts = TypeSpec(
        DataType.map,
        keySubType: TypeSpec(DataType.timestamp),
        valueSubType: TypeSpec(DataType.int),
      );
      expect(ts.toString(), equals('Map<timestamp, int>'));
    });

    test('UDT', () {
      final TypeSpec ts = TypeSpec(DataType.udt)
        ..keyspace = 'test'
        ..udtName = 'phone'
        ..udtFields = {
          'tags':
              TypeSpec(DataType.list, valueSubType: TypeSpec(DataType.ascii))
        };
      expect(ts.toString(), equals('{test.phone: {tags: List<ascii>}}'));
    });

    test('TUPLE', () {
      final TypeSpec ts = TypeSpec(DataType.tuple)
        ..tupleFields = [
          TypeSpec(DataType.int),
          TypeSpec(DataType.ascii),
          TypeSpec(DataType.timestamp)
        ];
      expect(ts.toString(), equals('([int, ascii, timestamp])'));
    });
  });

  group('Type guess:', () {
    test('BOOL', () {
      expect(
        DataTypeExt().guessForValue(true),
        equals(DataType.boolean),
      );
      expect(
        DataTypeExt().guessForValue(false),
        equals(DataType.boolean),
      );
    });

    test('DOUBLE', () {
      expect(
        DataTypeExt().guessForValue(3.145),
        equals(DataType.double),
      );
    });

    test('INT', () {
      expect(DataTypeExt().guessForValue(3), equals(DataType.int));
    });

    test('BIGINT', () {
      expect(
        DataTypeExt().guessForValue(9223372036854775807),
        equals(DataType.bigint),
      );
    });

    test('VARINT', () {
      expect(
        DataTypeExt().guessForValue(
          BigInt.parse('9223372036854775807000000'),
        ),
        equals(DataType.varint),
      );
    });

    test('VARCHAR', () {
      expect(
        DataTypeExt().guessForValue('test123 123'),
        equals(DataType.varchar),
      );
    });

    test('UUID', () {
      expect(
        DataTypeExt().guessForValue(Uuid.simple()),
        equals(DataType.uuid),
      );

      expect(
        DataTypeExt().guessForValue(Uuid.timeBased()),
        equals(DataType.uuid),
      );

      expect(
        DataTypeExt().guessForValue(Uuid.timeBased().toString()),
        equals(DataType.uuid),
      );
    });

    test('BLOB', () {
      expect(
        DataTypeExt().guessForValue(Uint8List.fromList([0xff])),
        equals(DataType.blob),
      );
    });

    test('TIMESTAMP', () {
      expect(
        DataTypeExt().guessForValue(DateTime.now()),
        equals(DataType.timestamp),
      );
    });

    test('INET', () {
      expect(
        DataTypeExt().guessForValue(InternetAddress('127.0.0.1')),
        equals(DataType.inet),
      );
    });

    test('LIST', () {
      expect(
        DataTypeExt().guessForValue(['test123 123', 1, 2, 3.14]),
        equals(DataType.list),
      );
    });

    test('SET', () {
      final Set set = {'a', 'b'};
      expect(
        DataTypeExt().guessForValue(set),
        equals(DataType.set),
      );
    });

    test('MAP', () {
      expect(
        DataTypeExt().guessForValue({'foo': 'bar'}),
        equals(DataType.map),
      );
    });

    test('TUPLE', () {
      expect(
        DataTypeExt().guessForValue(Tuple.fromIterable([1, 2, 3])),
        equals(DataType.tuple),
      );
    });

    test('No guess', () {
      expect(
        DataTypeExt().guessForValue(const SocketException('foo')),
        isNull,
      );
    });
  });
}
