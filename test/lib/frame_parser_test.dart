library cassandra_scylla_cortex.tests.frame_grabber;

import 'dart:async';
import 'dart:typed_data';
import 'package:cassandra_scylla_cortex/src/exceptions.dart';
import 'package:cassandra_scylla_cortex/src/protocol.dart';
import 'package:cassandra_scylla_cortex/src/types.dart';
import 'package:test/test.dart';
import 'mocks/mocks.dart' as mock;

void main({bool enableLogger = false}) {
  if (enableLogger) {
    mock.initLogger();
  }

  late StreamController<Uint8List> streamController;
  late Stream<Frame> frameGrabber;

  group('Frame parser (V2):', () {
    setUp(() {
      streamController = StreamController<Uint8List>();
      frameGrabber =
          streamController.stream.transform(FrameParser().transformer);
    });

    test('READY message', () {
      frameGrabber.listen(
        expectAsync1((Frame frame) {
          expect(frame.header!.opcode, equals(Opcode.ready));
        }),
      );
      mock.writeMessage(
        streamController,
        OpcodeExt().encode(Opcode.ready),
        headerVersion: HeaderVersion.responseV2,
      );
    });

    test('EVENT message (-1 streamId)', () {
      frameGrabber.listen(
        expectAsync1((Frame frame) {
          expect(frame.header!.opcode, equals(Opcode.event));
          expect(frame.header!.streamId, equals(-1));
        }),
      );
      mock.writeMessage(
        streamController,
        OpcodeExt().encode(Opcode.event),
        headerVersion: HeaderVersion.responseV2,
        streamId: -1,
      );
    });

    test('InvalidFrame exception (illegal opcode)', () {
      ExceptionMessage? error;
      Timer.run(
        expectAsync0(() {
          expect(error, isA<ExceptionMessage>());
          expect(
            (error?.exception as DriverException).message,
            equals(
              'Unknown frame with opcode 0x${0xFF.toRadixString(16)} and payload size 0x0',
            ),
          );
        }),
      );

      runZonedGuarded(
        () {
          frameGrabber.listen((Frame frame) {
            throw Exception('Should not have parsed $frame');
          });
          mock.writeMessage(
            streamController,
            0xFF,
            headerVersion: HeaderVersion.responseV2,
          );
        },
        (e, stack) {
          error = e as ExceptionMessage;
        },
      );
    });

    test('InvalidFrame exception (illegal length)', () {
      ExceptionMessage? error;
      Timer.run(
        expectAsync0(() {
          expect(error, isA<ExceptionMessage>());
          expect(
            (error?.exception as DriverException).message,
            equals(
              'Frame size cannot be larger than ${FrameHeader.maxLengthInBytes} bytes. Attempted to read ${FrameHeader.maxLengthInBytes + 1} bytes',
            ),
          );
        }),
      );

      runZonedGuarded(
        () {
          frameGrabber.listen((Frame frame) {
            throw Exception('Should not have parsed $frame');
          });
          mock.writeMessage(
            streamController,
            OpcodeExt().encode(Opcode.ready),
            overrideLength: FrameHeader.maxLengthInBytes + 1,
            headerVersion: HeaderVersion.responseV2,
          );
        },
        (e, stack) {
          error = e as ExceptionMessage;
        },
      );
    });
  });

  group('Frame parser (V3):', () {
    setUp(() {
      streamController = StreamController(sync: false);
      frameGrabber =
          streamController.stream.transform(FrameParser().transformer);
    });

    test('READY message', () {
      frameGrabber.listen(
        expectAsync1((Frame frame) {
          expect(frame.header!.opcode, equals(Opcode.ready));
        }),
      );
      mock.writeMessage(
        streamController,
        OpcodeExt().encode(Opcode.ready),
        headerVersion: HeaderVersion.responseV2,
      );
    });

    test('EVENT message (-1 streamId)', () {
      frameGrabber.listen(
        expectAsync1((Frame frame) {
          expect(frame.header!.opcode, equals(Opcode.event));
          expect(frame.header!.streamId, equals(-1));
        }),
      );
      mock.writeMessage(
        streamController,
        OpcodeExt().encode(Opcode.event),
        headerVersion: HeaderVersion.responseV2,
        streamId: -1,
      );
    });

    test('InvalidFrame exception (illegal opcode)', () {
      ExceptionMessage? error;
      Timer.run(
        expectAsync0(() {
          expect(error, isA<ExceptionMessage>());
          expect(
            (error?.exception as DriverException).message,
            equals(
              'Unknown frame with opcode 0x${0xFF.toRadixString(16)} and payload size 0x0',
            ),
          );
        }),
      );

      runZonedGuarded(
        () {
          frameGrabber.listen((Frame frame) {
            throw Exception('Should not have parsed $frame');
          });
          mock.writeMessage(
            streamController,
            0xFF,
            headerVersion: HeaderVersion.responseV2,
          );
        },
        (e, stack) {
          error = e as ExceptionMessage;
        },
      );
    });

    test('InvalidFrame exception (illegal length)', () {
      ExceptionMessage? error;
      Timer.run(
        expectAsync0(() {
          expect(error, isA<ExceptionMessage>());
          expect(
            (error?.exception as DriverException).message,
            equals(
              'Frame size cannot be larger than ${FrameHeader.maxLengthInBytes} bytes. Attempted to read ${FrameHeader.maxLengthInBytes + 1} bytes',
            ),
          );
        }),
      );

      runZonedGuarded(
        () {
          frameGrabber.listen((Frame frame) {
            throw Exception('Should not have parsed $frame');
          });
          mock.writeMessage(
            streamController,
            OpcodeExt().encode(Opcode.ready),
            overrideLength: FrameHeader.maxLengthInBytes + 1,
            headerVersion: HeaderVersion.responseV2,
          );
        },
        (e, stack) {
          error = e as ExceptionMessage;
        },
      );
    });
  });
}
