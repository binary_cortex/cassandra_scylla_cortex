// library cassandra_scylla_cortex.tests.enums;

import 'package:cassandra_scylla_cortex/src/types.dart';
import 'package:test/test.dart';

class EnumTest<T extends Enum, K> {
  List<T> values;
  EnumMap<T, K> map;
  String name;

  EnumTest(this.name, this.values, this.map);
}

List<EnumTest> testEnums = [
  EnumTest('BatchType', BatchType.values, BatchTypeExt()),
  EnumTest('Compression', Compression.values, CompressionExt()),
  EnumTest('Consistency', Consistency.values, ConsistencyExt()),
  EnumTest('DataType', DataType.values, DataTypeExt()),
  EnumTest('ErrorCode', ErrorCode.values, ErrorCodeExt()),
  EnumTest(
    'EventRegistrationType',
    EventRegistrationType.values,
    EventRegistrationTypeExt(),
  ),
  EnumTest('EventType', EventType.values, EventTypeExt()),
  EnumTest('HeaderFlag', HeaderFlag.values, HeaderFlagExt()),
  EnumTest('HeaderVersion', HeaderVersion.values, HeaderVersionExt()),
  EnumTest('Opcode', Opcode.values, OpcodeExt()),
  EnumTest('ProtocolVersion', ProtocolVersion.values, ProtocolVersionExt()),
  EnumTest('QueryFlag', QueryFlag.values, QueryFlagExt()),
  EnumTest('ResultType', ResultType.values, ResultTypeExt()),
  EnumTest('RowResultFlag', RowResultFlag.values, RowResultFlagExt()),
];

void main({bool enableLogger = false}) {
  for (final tester in testEnums) {
    group('${tester.name} enum', () => {});
    for (final enumValue in tester.values) {
      test(enumValue, () {
        final encoded = tester.map.decode(tester.map.encode(enumValue));
        expect(encoded, equals(enumValue));
      });
    }
  }
}
