library cassandra_scylla_cortex.tests.compression;

import 'dart:convert';
import 'dart:typed_data';

class RotConverter extends Converter<Uint8List, Uint8List> {
  final bool throwOnConvert;
  final int _key;

  const RotConverter(this._key, this.throwOnConvert);

  @override
  Uint8List convert(Uint8List input) {
    if (throwOnConvert) {
      throw Exception('Something has gone awfully wrong...');
    }
    final Uint8List result = Uint8List(input.length);

    for (int i = 0; i < input.length; i++) {
      result[i] = (input[i] + _key) % 256;
    }

    return result;
  }
}

class MockCompressionCodec extends Codec<Uint8List, Uint8List> {
  bool throwOnEncode;
  bool throwOnDecode;

  // For our test apply ROT-13 to compress/decompress
  late RotConverter _encoder;
  late RotConverter _decoder;

  MockCompressionCodec(
      [this.throwOnEncode = false, this.throwOnDecode = false]) {
    _encoder = RotConverter(13, throwOnEncode);
    _decoder = RotConverter(-13, throwOnDecode);
  }

  @override
  Converter<Uint8List, Uint8List> get encoder {
    return _encoder;
  }

  @override
  Converter<Uint8List, Uint8List> get decoder {
    return _decoder;
  }
}
