library cassandra_scylla_cortex.tests.mocks;

import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'package:cassandra_scylla_cortex/src/protocol.dart';
import 'package:cassandra_scylla_cortex/src/stream.dart';
import 'package:cassandra_scylla_cortex/src/types.dart';
import 'package:logging/logging.dart';

final Logger mockLogger = Logger('MockLogger');
bool initializedLogger = false;

void initLogger() {
  if (initializedLogger == true) {
    return;
  }
  initializedLogger = true;
  hierarchicalLoggingEnabled = true;
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((LogRecord rec) {
    print(
      '[${rec.level.name}]\t[${rec.time}]\t[${rec.loggerName}]:\t${rec.message}',
    );
  });
}

void writeMessage(
  Sink targetSink,
  int opcode, {
  ProtocolVersion protocolVersion = ProtocolVersion.v2,
  HeaderVersion headerVersion = HeaderVersion.requestV2,
  int streamId = 0,
  int flags = 0,
  int? overrideLength,
  List<int> data = const [],
}) {
  final TypeEncoder typeEncoder = TypeEncoder(protocolVersion);

  // Allocate header buffer
  final Uint8List buf = Uint8List(
    protocolVersion == ProtocolVersion.v2
        ? FrameHeader.sizeInBytesV2
        : FrameHeader.sizeInBytesV3,
  );
  final ByteData headerBytes = ByteData.view(buf.buffer);

  // Encode header
  int offset = 0;
  headerBytes
    ..setUint8(offset++, HeaderVersionExt().encode(headerVersion))
    ..setUint8(offset++, flags);

  // Encode stream id (V2 uses a byte, V3 uses a short)
  if (protocolVersion == ProtocolVersion.v2) {
    headerBytes.setInt8(offset++, streamId);
  } else {
    headerBytes.setInt16(offset, streamId);
    offset += 2;
  }

  // Encode remaining frame data
  headerBytes
    ..setUint8(offset++, opcode)
    ..setUint32(offset++, overrideLength ?? data.length);

  // Prepend the header to the writer buffer queue
  typeEncoder.writer!.addFirst(buf);
  typeEncoder.writer!.addLast(Uint8List.fromList(data));

  // Pipe everything to the sink
  typeEncoder.writer!.pipe(targetSink);
}

TypeDecoder createDecoder(TypeEncoder fromEncoder) {
  // Pipe encoded data to a reader
  final ChunkedInputReader reader = ChunkedInputReader();
  fromEncoder.writer!.chunks.forEach(reader.add);

  // Read to a buffer
  final Uint8List buffer = Uint8List(reader.length);
  reader.read(buffer, reader.length);

  // Return decoder
  return TypeDecoder.fromBuffer(
    ByteData.view(buffer.buffer),
    fromEncoder.protocolVersion,
  );
}

class MockServer {
  Compression? compression;
  ServerSocket? _server;
  List<Socket> clients = [];
  List<String>? _replayDumpFileList;
  List<String>? _replayAuthDumpFileList;
  String? _pathToDumps;
  Duration? responseDelay;
  Future _replayFuture = Future.value();

  MockServer() {
    List<String> pathSegments = Platform.script.pathSegments
        .getRange(0, Platform.script.pathSegments.length - 1)
        .toList();
    // Hack: make sure we can find our dump files from the main test or from individual tests
    if (!pathSegments.contains('lib')) {
      pathSegments = List.from(pathSegments);
      pathSegments.add('lib');
    }
    _pathToDumps = 'test/lib/frame_dumps/';
//        '${Platform.pathSeparator}${pathSegments.join(Platform.pathSeparator)}${Platform.pathSeparator}frame_dumps${Platform.pathSeparator}';
  }

  Future<void> shutdown() async {
    _replayDumpFileList = null;
    _replayAuthDumpFileList = null;

    if (_server != null) {
      mockLogger
          .info('Shutting down server [${_server!.address}:${_server!.port}]');

      final List<Future> cleanupFutures = [
        _replayFuture,
        () async {
          await _server!.close();
          return Future.delayed(const Duration(milliseconds: 20));
        }(),
      ];

      for (final client in clients) {
        client.destroy();
      }

      clients.clear();
      _server = null;

      await Future.wait(cleanupFutures);
    }
  }

  void disconnectClient(int clientIndex) {
    if (clients.length > clientIndex) {
      final Socket client = clients.removeAt(clientIndex);
      mockLogger.info(
        'Disconnecting client [${client.remoteAddress.host}:${client.remotePort}]',
      );
      client.destroy();
    }
  }

  Uint8List _applyCompression(List<int> originalPayload) {
    //mockLogger.fine('Applying '${_compression}' compression to frame of size ${originalPayload.length}');
    final Uint8List payload = Uint8List.fromList(originalPayload);

    // Detect version
    final ProtocolVersion version = (payload[0] ==
                HeaderVersionExt().encode(HeaderVersion.requestV2) ||
            payload[0] == HeaderVersionExt().encode(HeaderVersion.responseV2))
        ? ProtocolVersion.v2
        : ProtocolVersion.v3;

    // Create encoder for assembling compressed output
    final TypeEncoder encoder = TypeEncoder(version);

    // Create header and body views
    final ByteData headerView = ByteData.view(
      payload.buffer,
      0,
      version == ProtocolVersion.v2
          ? FrameHeader.sizeInBytesV2
          : FrameHeader.sizeInBytesV3,
    );
    final Uint8List bodyView = Uint8List.view(
      payload.buffer,
      version == ProtocolVersion.v2
          ? FrameHeader.sizeInBytesV2
          : FrameHeader.sizeInBytesV3,
      payload.lengthInBytes - headerView.lengthInBytes,
    );

    // Compress body
    final Uint8List? compressedBody =
        getCodec(CompressionExt().encode(compression!))!.encode(bodyView);

    // Assemble compressed payload:
    encoder.writer!.addLast(compressedBody);

    // Toggle header compression flag and update body size
    headerView.setUint8(
      1,
      headerView.getUint8(1) | HeaderFlagExt().encode(HeaderFlag.compression),
    );
    headerView.setUint32(
      version == ProtocolVersion.v2 ? 4 : 5,
      encoder.writer!.lengthInBytes,
    );

    // Prepend header to writer blocks
    encoder.writer!.addFirst(
      Uint8List.view(headerView.buffer, 0, headerView.lengthInBytes),
    );

    // concat everything together and cleanup
    final Uint8List compressedOutput = encoder.writer!.joinChunks();
    encoder.writer!.clear();
    return compressedOutput;
  }

  Uint8List _patchStreamId(List<int> originalPayload, int? streamId) {
    if (streamId == null) {
      return originalPayload as Uint8List;
    }
    final Uint8List payload = Uint8List.fromList(originalPayload);

    // Detect version
    final ProtocolVersion version = (payload[0] ==
                HeaderVersionExt().encode(HeaderVersion.requestV2) ||
            payload[0] == HeaderVersionExt().encode(HeaderVersion.responseV2))
        ? ProtocolVersion.v2
        : ProtocolVersion.v3;

    // Path stream id
    final ByteData headerView = ByteData.view(
      payload.buffer,
      0,
      version == ProtocolVersion.v2
          ? FrameHeader.sizeInBytesV2
          : FrameHeader.sizeInBytesV3,
    );

    if (version == ProtocolVersion.v2) {
      headerView.setInt8(2, streamId);
    } else {
      headerView.setInt16(2, streamId);
    }

    return payload;
  }

  Future<void> replayFile(
    int clientIndex,
    String filename, [
    int? streamId,
  ]) async {
    Future<void> onReplay() async {
      if (clientIndex > clients.length - 1) {
        throw ArgumentError('Invalid client index');
      }
      final File dumpFile = File('$_pathToDumps$filename');
      final List<int> response = compression == null
          ? _patchStreamId(dumpFile.readAsBytesSync(), streamId)
          : _applyCompression(
              _patchStreamId(dumpFile.readAsBytesSync(), streamId),
            );
      clients[clientIndex].add(response);
      return clients[clientIndex].flush();
    }

    await _replayFuture;
    if (responseDelay != null) {
      await Future.delayed(responseDelay!, onReplay);
    } else {
      await onReplay();
    }
  }

  Future<void> listen(String host, int port) async {
    // final Completer completer = Completer();
    mockLogger.info('Binding MockServer to $host:$port');

    final ServerSocket server = await ServerSocket.bind(host, port);
    _server = server;
    mockLogger.info('[$host:$port] Listening for incoming connections');
    _server!.listen(_handleConnection);
  }

  void setReplayList(Iterable<String> list) {
    _replayDumpFileList = List.from(list);
  }

  void setAuthReplayList(Iterable<String> list) {
    _replayAuthDumpFileList = List.from(list);
  }

  void _handleClientFrame(Socket client, Frame frame) {
    //mockLogger.fine('Client [${client.remoteAddress.host}:${client.remotePort}][SID: ${frame.header.streamId}] sent ${Opcode.nameOf(frame.header.opcode)} frame with len 0x${frame.header.length.toRadixString(16)}]');
    // Complete handshake and event registration messages
    if (frame.header!.opcode == Opcode.startup &&
        (_replayAuthDumpFileList == null || _replayAuthDumpFileList!.isEmpty)) {
      writeMessage(
        client,
        OpcodeExt().encode(Opcode.ready),
        streamId: frame.header!.streamId,
      );
    } else if (frame.header!.opcode == Opcode.register) {
      writeMessage(
        client,
        OpcodeExt().encode(Opcode.ready),
        streamId: frame.header!.streamId,
      );
    } else if (_replayAuthDumpFileList != null &&
        _replayAuthDumpFileList!.isNotEmpty) {
      // Respond with the next payload in replay list
      _replayFuture = replayFile(
        clients.indexOf(client),
        _replayAuthDumpFileList!.removeAt(0),
        frame.header!.streamId,
      );
    } else if (_replayDumpFileList != null && _replayDumpFileList!.isNotEmpty) {
      // Respond with the next payload in replay list
      _replayFuture = replayFile(
        clients.indexOf(client),
        _replayDumpFileList!.removeAt(0),
        frame.header!.streamId,
      );
    }
  }

  void _handleClientError(Socket client, err, trace) {
    mockLogger.info(
      // ignore: avoid_dynamic_calls
      'Client [${client.remoteAddress.host}:${client.remotePort}] error ${err.exception.message}',
    );
    // ignore: avoid_dynamic_calls
    mockLogger.info('${err.stackTrace}');
  }

  void _handleConnection(Socket client) {
    clients.add(client);
    mockLogger.info(
      'Client [${client.remoteAddress.host}:${client.remotePort}] connected',
    );

    client
        .transform(FrameParser().transformer)
        .transform(FrameDecompressor(compression).transformer)
        .listen(
          (frame) => _handleClientFrame(client, frame),
          onError: (err, trace) => _handleClientError(client, err, trace),
        );
  }
}

class MockChunkedOutputWriter extends ChunkedOutputWriter {
  int forcedLengthInBytes = 0;

  @override
  int get lengthInBytes => forcedLengthInBytes;
}

class MockAuthenticator extends Authenticator {
  @override
  String get authenticatorClass {
    return 'com.achilleasa.FooAuthenticator';
  }

  @override
  Uint8List? answerChallenge(Uint8List? challenge) {
    return null;
  }
}
