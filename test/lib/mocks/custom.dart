library cassandra_scylla_cortex.tests.custom;

import 'dart:convert';
import 'dart:typed_data';
import 'package:cassandra_scylla_cortex/src/types.dart';

class CustomJson implements CustomType {
  Map? payload;

  @override
  String get customTypeClass => 'com.achilleasa.cassandra.cqltypes.Json';

  CustomJson(this.payload);
}

class CustomJsonEncoder extends Converter<CustomJson, Uint8List?> {
  @override
  Uint8List? convert(CustomJson input) {
    return input.payload == null
        ? null
        : Uint8List.fromList(json.encode(input.payload).codeUnits);
  }
}

class CustomJsonDecoder extends Converter<Uint8List, CustomJson> {
  @override
  CustomJson convert(Uint8List input) {
    final Map payload = const JsonDecoder().convert(utf8.decode(input)) as Map;
    return CustomJson(payload);
  }
}

class CustomJsonCodec extends Codec<CustomJson, Uint8List?> {
  final CustomJsonEncoder _encoder = CustomJsonEncoder();
  final CustomJsonDecoder _decoder = CustomJsonDecoder();

  @override
  Converter<CustomJson, Uint8List?> get encoder {
    return _encoder;
  }

  @override
  Converter<Uint8List, CustomJson> get decoder {
    return _decoder;
  }
}
