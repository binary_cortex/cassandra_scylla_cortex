library cassandra_scylla_cortex.tests.pool_config;

import 'package:cassandra_scylla_cortex/cql.dart' as cql;
import 'package:test/test.dart';

void main({bool enableLogger = false}) {
  group('Pool config:', () {
    test('invalid streams per connection (V2 protocol)', () {
      expect(
        () => cql.PoolConfiguration(
          protocolVersion: cql.ProtocolVersion.v2,
          streamsPerConnection: 1024,
        ),
        throwsA(
          predicate(
            (dynamic ex) =>
                ex is ArgumentError &&
                ex.message ==
                    "Invalid value for option 'streamsPerConnection'. Expected a value between 1 and 128 when using V2 prototcol",
          ),
        ),
      );
    });

    test('invalid protocol', () {
      expect(
        () => cql.PoolConfiguration(
          streamsPerConnection: 65536,
        ),
        throwsA(
          predicate(
            (dynamic ex) =>
                ex is ArgumentError &&
                ex.message ==
                    "Invalid value for option 'streamsPerConnection'. Expected a value between 1 and 3768 when using V3 prototcol",
          ),
        ),
      );
    });
  });
}
