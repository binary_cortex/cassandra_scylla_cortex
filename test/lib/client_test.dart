library cassandra_scylla_cortex.tests.client;

import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'package:cassandra_scylla_cortex/cql.dart' as cql;
import 'package:test/test.dart';
import 'mocks/custom.dart' as custom;
import 'mocks/mocks.dart' as mock;

void main({bool enableLogger = true}) {
  if (enableLogger) {
    mock.initLogger();
  }

  const String serverHost = '127.0.0.1';
  const int serverPort = 32000;
  final mock.MockServer server = mock.MockServer();
  const int server2Port = 32001;
  final mock.MockServer server2 = mock.MockServer();

  // Delay server2 responses to make sure that clients *always* connect
  // first so our tests (especially the event ones) execute as they should
  server2.responseDelay = const Duration(milliseconds: 10);

  group('Client exceptions:', () {
    test('Empty host list', () {
      expect(() => cql.Client.fromHostList([]), throwsArgumentError);
    });
  });

  test('empty hosts exception', () {
    expect(
      () => cql.SimpleConnectionPool.fromHostList(
        [],
        cql.PoolConfiguration(),
      ),
      throwsA(
        (e) => e is ArgumentError && e.message == 'Host list cannot be empty',
      ),
    );
  });

  final poolConstructors = [
    cql.SimpleConnectionPool.fromHostList,
    cql.RevisedConnectionPool.fromHostList,
  ];

  for (final constructor in poolConstructors) {
    group('Connection pool: $constructor', () {
      cql.Client? client;

      setUp(() async {
        await Future.wait([
          server.listen(serverHost, serverPort),
          server2.listen(serverHost, server2Port)
        ]);
      });

      tearDown(() {
        final cleanupFutures = <Future>[server.shutdown(), server2.shutdown()];

        if (client != null) {
          cleanupFutures.add(client!.shutdown());
          client = null;
        }

        return Future.wait(cleanupFutures);
      });

      group('misc:', () {
        test('Client with default pool conf', () {
          client = cql.Client.fromHostList(
            ['$serverHost:$serverPort'],
            poolConstructor: constructor,
          );
          expect(
            client!.connectionPool.poolConfig!.connectionsPerHost,
            equals(1),
          );
        });

        test('Fail to connect to any pool host', () async {
          client = cql.Client.fromHostList(
            ['$serverHost:${serverPort + 3}'],
            poolConstructor: constructor,
            poolConfig: cql.PoolConfiguration(
              reconnectWaitTime: const Duration(milliseconds: 1),
              maxConnectionAttempts: 5,
              connectionTimeout: const Duration(milliseconds: 200),
            ),
          );

          try {
            await client!.connectionPool.connect();
            expect(false, isTrue);
          } catch (error) {
            expect(error, isA<cql.NoHealthyConnectionsException>());
          }
        });

        test('Connection to at least one node in the pool', () async {
          client = cql.Client.fromHostList(
            ['$serverHost:$serverPort', '$serverHost:${serverPort + 3000}'],
            poolConstructor: constructor,
            poolConfig: cql.PoolConfiguration(
              reconnectWaitTime: const Duration(milliseconds: 100),
              maxConnectionAttempts: 5,
            ),
          );

          await client!.connectionPool.connect();
          await client!.shutdown();
        });
      });

      group('authentication:', () {
        group('password authenticator exceptions', () {
          test('empty username', () {
            expect(
              () => cql.PasswordAuthenticator('', 'foo'),
              throwsArgumentError,
            );
          });

          test('empty password', () {
            expect(
              () => cql.PasswordAuthenticator('foo', ''),
              throwsArgumentError,
            );
          });
        });

        test('Authentication provider exception', () {
          server.setAuthReplayList(['authenticate_v3.dump']);

          client = cql.Client.fromHostList(
            ['$serverHost:$serverPort'],
            poolConstructor: constructor,
            poolConfig: cql.PoolConfiguration(
              autoDiscoverNodes: false,
            ),
          );

          void handleError(e) {
            expect(e, isA<cql.AuthenticationException>());
            expect(
              e.toString(),
              equals(
                "AuthenticationException: Server requested 'org.apache.cassandra.auth.PasswordAuthenticator' authenticator but no authenticator specified",
              ),
            );
          }

          client!.connectionPool
              .connect()
              .catchError(expectAsync1(handleError));
        });

        test('Different authenticator exception', () {
          server.setAuthReplayList(['authenticate_v3.dump']);

          client = cql.Client.fromHostList(
            ['$serverHost:$serverPort'],
            poolConstructor: constructor,
            poolConfig: cql.PoolConfiguration(
              autoDiscoverNodes: false,
              authenticator: mock.MockAuthenticator(),
            ),
          );

          void handleError(e) {
            expect(e, isA<cql.AuthenticationException>());
            expect(
              e.toString(),
              equals(
                "AuthenticationException: Server requested 'org.apache.cassandra.auth.PasswordAuthenticator' authenticator but a '${client!.connectionPool.poolConfig!.authenticator!.authenticatorClass}' authenticator was specified instead",
              ),
            );
          }

          client!.connectionPool
              .connect()
              .catchError(expectAsync1(handleError));
        });

        test('User/pass mismatch exception', () {
          server.setAuthReplayList(
              ['authenticate_v3.dump', 'auth_error_v3.dump']);

          client = cql.Client.fromHostList(
            ['$serverHost:$serverPort'],
            poolConstructor: constructor,
            poolConfig: cql.PoolConfiguration(
              autoDiscoverNodes: false,
              authenticator: cql.PasswordAuthenticator('foo', 'bar'),
            ),
          );

          void handleError(e) {
            expect(e, isA<cql.AuthenticationException>());
            expect(
              e.toString(),
              equals(
                'AuthenticationException: Username and/or password are incorrect',
              ),
            );
          }

          client!.connectionPool
              .connect()
              .catchError(expectAsync1(handleError));
        });

        test('Auth success (multi challenge-response)', () async {
          server.setAuthReplayList([
            'authenticate_v3.dump',
            'auth_challenge_v3.dump',
            'auth_challenge_v3.dump',
            'auth_challenge_v3.dump',
            'auth_success_v3.dump'
          ]);
          server.setReplayList(['select_tuple_v3.dump']);

          client = cql.Client.fromHostList(
            ['$serverHost:$serverPort'],
            poolConstructor: constructor,
            poolConfig: cql.PoolConfiguration(
              autoDiscoverNodes: false,
              authenticator: cql.PasswordAuthenticator('foo', 'bar'),
            ),
          );

          await client!.query(cql.Query('SELECT * FROM test.test_type'));
        });
      });

      group('execute:', () {
        test('select from invalid collection (V3)', () async {
          server.setReplayList(['error_v3.dump']);
          client = cql.Client.fromHostList(
            ['$serverHost:$serverPort'],
            poolConstructor: constructor,
            poolConfig: cql.PoolConfiguration(
              autoDiscoverNodes: false,
            ),
          );

          void handleError(e) {
            expect(e, isA<cql.CassandraException>());
            expect(
              e.toString(),
              equals('CassandraException: unconfigured columnfamily foo'),
            );
          }

          try {
            await client!.execute(
              cql.Query(
                'SELECT * from test.foo',
                consistency: cql.Consistency.localOne,
              ),
            );
          } catch (error) {
            handleError(error);
          }
        });

        test('set keyspace', () async {
          server.setReplayList(['set_keyspace_v2.dump']);

          final cql.ConnectionPool pool = constructor(
            ['$serverHost:$serverPort'],
            cql.PoolConfiguration(autoDiscoverNodes: false),
          );

          client = cql.Client.withPool(pool);

          final cql.ResultMessage? message =
              await client!.execute(cql.Query('USE test'));
          expect(message, isA<cql.SetKeyspaceResultMessage>());
          expect(
            (message! as cql.SetKeyspaceResultMessage).keyspace,
            equals('test'),
          );
        });

        test('query and process raw RowsResultMessage', () {
          server.setReplayList(['select_v2.dump']);
          client = cql.Client.fromHostList(
            ['$serverHost:$serverPort'],
            poolConstructor: constructor,
            poolConfig: cql.PoolConfiguration(autoDiscoverNodes: false),
          );
          expect(
            client!.execute(cql.Query('SELECT * from test.type_test')),
            completion((cql.ResultMessage message) {
              expect(message, isA<cql.RowsResultMessage>());
              final cql.RowsResultMessage res =
                  message as cql.RowsResultMessage;
              expect(res.rows.length, equals(1));
              final Map<String, Object?> row = res.rows.first;
              final Map<String, Object?> expectedValues = {
                'ascii_type': 'text4',
                'bigint_type': 9223372036854775807,
                'bool_type': true,
                'inet_type': InternetAddress('192.168.169.102'),
                'int_type': 32238493,
                'list_type': [100, 200],
                'map_type': {100: 'the test', 200: 'the result'},
                'set_type': [100, 200],
                'text_type': 'This is a long UTF8 κείμενο',
                'uuid_type': cql.Uuid('550e8400-e29b-41d4-a716-446655440000'),
                'varchar_type': 'Arbitary long text goes here',
                'varint_type':
                    BigInt.parse('-3123091212904812093120938120938120312890'),
              };
              expectedValues.forEach((String? fieldName, Object? fieldValue) {
                expect(row[fieldName], equals(fieldValue));
              });
              return true;
            }),
          );
        });

        test('alter statement and process raw RowsResultMessage', () {
          server.setReplayList(['schema_change_result_v2.dump']);
          client = cql.Client.fromHostList(
            ['$serverHost:$serverPort'],
            poolConstructor: constructor,
            poolConfig: cql.PoolConfiguration(autoDiscoverNodes: false),
          );
          expect(
            client!.execute(
              cql.Query('ALTER TABLE test.type_test ADD new_field int'),
            ),
            completion((cql.ResultMessage message) {
              expect(message, isA<cql.SchemaChangeResultMessage>());
              final cql.SchemaChangeResultMessage res =
                  message as cql.SchemaChangeResultMessage;
              expect(res.keyspace, equals('test'));
              expect(res.table, equals('type_test'));
              expect(res.change, equals('UPDATED'));
              return true;
            }),
          );
        });

        test(
            'it can handle execution of multiple queries scheduled synchronously',
            () {
          server.setReplayList(['select_v2.dump', 'select_v2.dump']);
          final client = cql.Client.fromHostList(
            ['$serverHost:$serverPort'],
            poolConstructor: constructor,
            poolConfig: cql.PoolConfiguration(autoDiscoverNodes: false),
          );
          final f1 = client.execute(cql.Query('SELECT * from test.type_test'));
          final f2 = client.execute(cql.Query('SELECT * from test.type_test'));

          expect(Future.wait([f1, f2]), completes);
        });
      });

      group('query:', () {
        group('SELECT:', () {
          group('custom types:', () {
            test('without custom type handler', () async {
              cql.unregisterCodec('com.achilleasa.cassandra.cqltypes.Json');
              server.setReplayList(['select_custom_type_v2.dump']);
              client = cql.Client.fromHostList(
                ['$serverHost:$serverPort'],
                poolConstructor: constructor,
                poolConfig: cql.PoolConfiguration(autoDiscoverNodes: false),
              );

              final rows = (await client!
                  .query(cql.Query('SELECT * from test.custom_types')))!;
              expect(rows.length, equals(1));

              final Map<String, Object?> row = rows.first;
              expect(row.length, equals(2));
              expect(row['login'], equals('test'));
              expect(row['custom'], isA<Uint8List>());
            });

            test('with custom type handler', () async {
              // Register custom type handler
              cql.registerCodec(
                'com.achilleasa.cassandra.cqltypes.Json',
                custom.CustomJsonCodec(),
              );

              server.setReplayList(['select_custom_type_v2.dump']);
              client = cql.Client.fromHostList(
                ['$serverHost:$serverPort'],
                poolConstructor: constructor,
                poolConfig: cql.PoolConfiguration(autoDiscoverNodes: false),
              );

              final rows = (await client!
                  .query(cql.Query('SELECT * from test.custom_types')))!;
              expect(rows.length, equals(1));

              final Map<String, Object?> row = rows.first;
              expect(row.length, equals(2));
              expect(row['login'], equals('test'));
              expect(row['custom'], isA<custom.CustomJson>());

              final custom.CustomJson customJson =
                  row['custom']! as custom.CustomJson;
              expect(customJson.payload!.containsKey('foo'), isTrue);
              expect(customJson.payload!['foo'], equals('bar'));
            });
          });

          test('tuple type', () {
            server.setReplayList(['select_tuple_v3.dump']);
            client = cql.Client.fromHostList(
              ['$serverHost:$serverPort'],
              poolConstructor: constructor,
              poolConfig: cql.PoolConfiguration(
                autoDiscoverNodes: false,
              ),
            );
            expect(
              client!.query(cql.Query('SELECT * from test.tuple_test')),
              completion((Iterable<Map<String, Object?>> rows) {
                expect(rows.length, equals(1));
                final Map<String, Object?> row = rows.first;
                final Map<String, Object?> expectedValues = {
                  'the_key': 1,
                  'the_tuple': cql.Tuple.fromIterable([10, 'foo', true])
                };
                expectedValues.forEach((String? fieldName, Object? fieldValue) {
                  expect(row[fieldName], equals(fieldValue));
                });
                return true;
              }),
            );
          });

          test('from unknown collection (V2)', () async {
            server.setReplayList(['error_v2.dump']);
            client = cql.Client.fromHostList(
              ['$serverHost:$serverPort'],
              poolConstructor: constructor,
              poolConfig: cql.PoolConfiguration(autoDiscoverNodes: false),
            );

            void handleError(e) {
              expect(e, isA<cql.CassandraException>());
              expect(
                e.toString(),
                equals('CassandraException: unconfigured columnfamily foo'),
              );
            }

            try {
              await client!.query(cql.Query('SELECT * from test.foo'));
            } catch (error) {
              handleError(error);
            }
          });

          test('normal', () {
            server.setReplayList(['select_v2.dump']);
            client = cql.Client.fromHostList(
              ['$serverHost:$serverPort'],
              poolConstructor: constructor,
              poolConfig: cql.PoolConfiguration(autoDiscoverNodes: false),
            );
            expect(
              client!.query(cql.Query('SELECT * from test.type_test')),
              completion((Iterable<Map<String, Object?>> rows) {
                expect(rows.length, equals(1));
                final Map<String, Object?> row = rows.first;
                final Map<String, Object?> expectedValues = {
                  'ascii_type': 'text4',
                  'bigint_type': 9223372036854775807,
                  'bool_type': true,
                  'inet_type': InternetAddress('192.168.169.102'),
                  'int_type': 32238493,
                  'list_type': [100, 200],
                  'map_type': {100: 'the test', 200: 'the result'},
                  'set_type': [100, 200],
                  'text_type': 'This is a long UTF8 κείμενο',
                  'uuid_type': cql.Uuid('550e8400-e29b-41d4-a716-446655440000'),
                  'varchar_type': 'Arbitary long text goes here',
                  'varint_type':
                      BigInt.parse('-3123091212904812093120938120938120312890'),
                };
                expectedValues.forEach((String? fieldName, Object? fieldValue) {
                  expect(row[fieldName], equals(fieldValue));
                });
                return true;
              }),
            );
          });
        });

        group('INSERT:', () {
          test('batch insert', () async {
            server.setReplayList(['void_result_v2.dump']);
            client = cql.Client.fromHostList(
              ['$serverHost:$serverPort'],
              poolConstructor: constructor,
              poolConfig: cql.PoolConfiguration(autoDiscoverNodes: false),
            );
            void handleResult(cql.ResultMessage? message) {
              expect(message, isA<cql.VoidResultMessage>());
            }

            const String query =
                'INSERT INTO page_view_counts (url_name, page_name, counter_value) VALUES (?, ?, ?)';
            final result = await client!.execute(
              cql.BatchQuery()
                ..add(
                  cql.Query(
                    query,
                    bindings: ['http://www.test.com', 'front_page', 1],
                  ),
                )
                ..add(
                  cql.Query(
                    query,
                    bindings: ['http://www.test.com', 'login_page', 2],
                  ),
                )
                ..add(
                  cql.Query(
                    query,
                    bindings: ['http://www.test.com', 'main_page', 3],
                  ),
                ),
            );
            handleResult(result);
          });

          test('batch insert with serial consistency (V3)', () async {
            server.setReplayList(['void_result_v3.dump']);
            client = cql.Client.fromHostList(
              ['$serverHost:$serverPort'],
              poolConstructor: constructor,
              poolConfig: cql.PoolConfiguration(
                autoDiscoverNodes: false,
              ),
            );

            const String query =
                'INSERT INTO page_view_counts (url_name, page_name, counter_value) VALUES (?, ?, ?)';
            final cql.ResultMessage? message = await client!.execute(
              cql.BatchQuery()
                ..serialConsistency = cql.Consistency.localSerial
                ..add(
                  cql.Query(
                    query,
                    bindings: ['http://www.test.com', 'front_page', 1],
                  ),
                )
                ..add(
                  cql.Query(
                    query,
                    bindings: ['http://www.test.com', 'login_page', 2],
                  ),
                )
                ..add(
                  cql.Query(
                    query,
                    bindings: ['http://www.test.com', 'main_page', 3],
                  ),
                ),
            );
            expect(message, isA<cql.VoidResultMessage>());
          });
        });
      });

      group('stream:', () {
        test('process streamed rows', () {
          server.setReplayList([
            'stream_v2_1of3.dump',
            'stream_v2_2of3.dump',
            'stream_v2_3of3.dump'
          ]);
          client = cql.Client.fromHostList(
            ['$serverHost:$serverPort'],
            poolConstructor: constructor,
            poolConfig: cql.PoolConfiguration(autoDiscoverNodes: false),
          );

          void streamCallback(Map<String, Object?> row) {}

          client!
              .stream(
                cql.Query('SELECT * FROM test.page_view_counts'),
                pageSize: 4,
              )
              .listen(expectAsync1(streamCallback, count: 10, max: 10));
        });

        test('process streamed rows (preferBiggerTcpPackets)', () {
          server.setReplayList([
            'stream_v2_1of3.dump',
            'stream_v2_2of3.dump',
            'stream_v2_3of3.dump'
          ]);
          client = cql.Client.fromHostList(
            ['$serverHost:$serverPort'],
            poolConstructor: constructor,
            poolConfig: cql.PoolConfiguration(
              autoDiscoverNodes: false,
              preferBiggerTcpPackets: true,
            ),
          );

          void streamCallback(Map<String, Object?> row) {}

          client!
              .stream(
                cql.Query('SELECT * FROM test.page_view_counts'),
                pageSize: 4,
              )
              .listen(expectAsync1(streamCallback, count: 10, max: 10));
        });

        test('pause/resume', () async {
          server.setReplayList([
            'stream_v2_1of3.dump',
            'stream_v2_2of3.dump',
            'stream_v2_3of3.dump'
          ]);
          client = cql.Client.fromHostList(
            ['$serverHost:$serverPort'],
            poolConstructor: constructor,
            poolConfig: cql.PoolConfiguration(autoDiscoverNodes: false),
          );

          // ignore: cancel_subscriptions
          late StreamSubscription streamSubscription;
          int rowCount = 0;

          void streamCallback(Map<String, Object?> row) {
            rowCount++;
            if (rowCount == 5) {
              streamSubscription.pause();
              Future.delayed(
                const Duration(milliseconds: 100),
                () => streamSubscription.resume(),
              );
            }
          }

          streamSubscription = client!
              .stream(
                cql.Query('SELECT * FROM test.page_view_counts'),
                pageSize: 4,
              )
              .listen(expectAsync1(streamCallback, count: 10, max: 10));
        });

        test('close', () {
          server.setReplayList([
            'stream_v2_1of3.dump',
            'stream_v2_2of3.dump',
            'stream_v2_3of3.dump'
          ]);
          client = cql.Client.fromHostList(
            ['$serverHost:$serverPort'],
            poolConstructor: constructor,
            poolConfig: cql.PoolConfiguration(autoDiscoverNodes: false),
          );

          late StreamSubscription streamSubscription;
          int rowCount = 0;

          void streamCallback(Map<String, Object?> row) {
            rowCount++;
            if (rowCount == 5) {
              streamSubscription.cancel();
            }
          }

          streamSubscription = client!
              .stream(
                cql.Query('SELECT * FROM test.page_view_counts'),
                pageSize: 4,
              )
              .listen(expectAsync1(streamCallback, count: 5, max: 5));
        });

        test('connection lost', () {
          server.setReplayList([
            'stream_v2_1of3.dump',
            'stream_v2_2of3.dump',
            'stream_v2_3of3.dump'
          ]);
          client = cql.Client.fromHostList(
            ['$serverHost:$serverPort'],
            poolConstructor: constructor,
            poolConfig: cql.PoolConfiguration(
              autoDiscoverNodes: false,
              connectionTimeout: const Duration(milliseconds: 200),
            ),
          );

          // ignore: cancel_subscriptions
          late StreamSubscription subscription;

          bool firstInvocation = true;

          void streamCallback(Map<String, Object?> row) {
            if (firstInvocation) {
              firstInvocation = false;
              subscription.pause();
              () async {
                await server.shutdown();
                return Future.delayed(
                  const Duration(milliseconds: 10),
                  () {
                    subscription.resume();
                  },
                );
              }();
            }
          }

          subscription = client!
              .stream(
            cql.Query('SELECT * FROM test.page_view_counts'),
            pageSize: 4,
          )
              .listen(
            streamCallback,
            onError: expectAsync1((e) {
              expect(e, isA<cql.NoHealthyConnectionsException>());
              expect(e.toString(), startsWith('NoHealthyConnectionsException'));
              client?.shutdown();
            }),
          );
        });

        test('connection lost; fallback to alt connection', () async {
          // @note This test periodically fails for both pools. Not sure why
          // Some form of race condition I assume
          server.setReplayList([
            'stream_v2_1of3.dump',
            'stream_v2_2of3.dump',
            'stream_v2_3of3.dump'
          ]);
          client = cql.Client.fromHostList(
            ['$serverHost:$serverPort'],
            poolConstructor: constructor,
            poolConfig: cql.PoolConfiguration(
              autoDiscoverNodes: false,
              connectionsPerHost: 2,
              connectionTimeout: const Duration(milliseconds: 200),
              reconnectWaitTime: const Duration(milliseconds: 100),
            ),
          );

          bool firstRun = true;

          void streamCallback(Map<String, Object?> row) {
            if (firstRun) {
              firstRun = false;
              server.disconnectClient(0);
            }
          }

          await for (final row in client!.stream(
            cql.Query('SELECT * FROM test.page_view_counts'),
            pageSize: 4,
          )) {
            streamCallback(row);
          }

          await client?.shutdown(drain: false);
        });
      });
      test('connection lost; single connection', () async {
        // @note This always fails on the simple connection pool. That pool doesn't
        // handle this scenario
        server.setReplayList([
          'stream_v2_1of3.dump',
          'stream_v2_2of3.dump',
          'stream_v2_3of3.dump'
        ]);
        client = cql.Client.fromHostList(
          ['$serverHost:$serverPort'],
          poolConstructor: constructor,
          poolConfig: cql.PoolConfiguration(
            autoDiscoverNodes: false,
            connectionTimeout: const Duration(milliseconds: 200),
            reconnectWaitTime: const Duration(milliseconds: 100),
          ),
        );

        bool firstRun = true;

        void streamCallback(Map<String, Object?> row) {
          if (firstRun) {
            firstRun = false;
            server.disconnectClient(0);
          }
        }

        await for (final row in client!.stream(
          cql.Query('SELECT * FROM test.page_view_counts'),
          pageSize: 4,
        )) {
          streamCallback(row);
        }

        await client?.shutdown(drain: false);
      });

      group('prepared queries:', () {
        test('prepare and execute query (V2)', () async {
          server.setReplayList(['prepare_v2.dump', 'void_result_v2.dump']);
          client = cql.Client.fromHostList(
            ['$serverHost:$serverPort'],
            poolConstructor: constructor,
            poolConfig: cql.PoolConfiguration(autoDiscoverNodes: false),
          );

          final cql.Query query = cql.Query(
            '''
INSERT INTO test.type_test (
	ascii_type, bigint_type, decimal_type, bool_type,
	double_type, float_type, inet_type, int_type, list_type, map_type,
	set_type, text_type, timestamp_type, uuid_type, timeuuid_type,
	varchar_type, varint_type, blob_type
) VALUES (
  :ascii_type, :bigint_type, :decimal_type, :bool_type,
	:double_type, :float_type, :inet_type, :int_type, :list_type, :map_type,
	:set_type, :text_type, :timestamp_type, :uuid_type, :timeuuid_type,
	:varchar_type, :varint_type, :blob_type
)''',
            consistency: cql.Consistency.one,
            prepared: true,
          );

          query.bindings = {
            'ascii_type': '123',
            'bigint_type': 123451234,
            'decimal_type': 3.14,
            'bool_type': true,
            'double_type': 3.14,
            'float_type': 3.14,
            'inet_type': InternetAddress('192.168.169.101'),
            'int_type': 10,
            'list_type': [1, 2, 3],
            'map_type': {1: 'A', 2: 'BC'},
            'set_type': [1, 2, 3],
            'text_type': 'Long text',
            'timestamp_type': DateTime.now(),
            'uuid_type': cql.Uuid.simple(),
            'timeuuid_type': cql.Uuid.timeBased(),
            'varchar_type': 'test 123',
            'varint_type': BigInt.from(123456),
            'blob_type': Uint8List.fromList([
              0x8B,
              0xAD,
              0xF0,
              0x00,
              0x0D,
              0xF0,
              0x00,
              0x0C,
              0x00,
              0x0F,
              0xF3,
              0x0E
            ])
          };

          await client!.execute(query);
        });

        test(
            'prepare and execute query; fallback to other connection on same host (V2)',
            () async {
          server.setReplayList([
            'prepare_v2.dump', 'void_result_v2.dump',
            'void_result_v2.dump' // 2nd attempt
          ]);
          client = cql.Client.fromHostList(
            ['$serverHost:$serverPort'],
            poolConstructor: constructor,
            poolConfig: cql.PoolConfiguration(
              autoDiscoverNodes: false,
              connectionsPerHost: 2,
            ),
          );

          final cql.Query query = cql.Query(
            '''
INSERT INTO test.type_test (
	ascii_type, bigint_type, decimal_type, bool_type,
	double_type, float_type, inet_type, int_type, list_type, map_type,
	set_type, text_type, timestamp_type, uuid_type, timeuuid_type,
	varchar_type, varint_type, blob_type
) VALUES (
  :ascii_type, :bigint_type, :decimal_type, :bool_type,
	:double_type, :float_type, :inet_type, :int_type, :list_type, :map_type,
	:set_type, :text_type, :timestamp_type, :uuid_type, :timeuuid_type,
	:varchar_type, :varint_type, :blob_type
)''',
            consistency: cql.Consistency.one,
            prepared: true,
          );

          query.bindings = {
            'ascii_type': '123',
            'bigint_type': 123451234,
            'decimal_type': 3.14,
            'bool_type': true,
            'double_type': 3.14,
            'float_type': 3.14,
            'inet_type': InternetAddress('192.168.169.101'),
            'int_type': 10,
            'list_type': [1, 2, 3],
            'map_type': {1: 'A', 2: 'BC'},
            'set_type': [1, 2, 3],
            'text_type': 'Long text',
            'timestamp_type': DateTime.now(),
            'uuid_type': cql.Uuid.simple(),
            'timeuuid_type': cql.Uuid.timeBased(),
            'varchar_type': 'test 123',
            'varint_type': BigInt.from(123456),
            'blob_type': Uint8List.fromList([
              0x8B,
              0xAD,
              0xF0,
              0x00,
              0x0D,
              0xF0,
              0x00,
              0x0C,
              0x00,
              0x0F,
              0xF3,
              0x0E
            ])
          };

          await client!.execute(query);

          server.disconnectClient(0);

          await client!.execute(query);
          await client!.shutdown();
        });

        test(
            'prepare and execute query; prepare on new host after server1 dies (V2)',
            () async {
          server.setReplayList(['prepare_v2.dump', 'void_result_v2.dump']);
          server2.setReplayList(['prepare_v2.dump', 'void_result_v2.dump']);
          client = cql.Client.fromHostList(
            ['$serverHost:$serverPort', '$serverHost:$server2Port'],
            poolConstructor: constructor,
            poolConfig: cql.PoolConfiguration(
              autoDiscoverNodes: false,
              connectionsPerHost: 2,
              connectionTimeout: const Duration(milliseconds: 200),
            ),
          );

          final cql.Query query = cql.Query(
            '''
INSERT INTO test.type_test (
	ascii_type, bigint_type, decimal_type, bool_type,
	double_type, float_type, inet_type, int_type, list_type, map_type,
	set_type, text_type, timestamp_type, uuid_type, timeuuid_type,
	varchar_type, varint_type, blob_type
) VALUES (
  :ascii_type, :bigint_type, :decimal_type, :bool_type,
	:double_type, :float_type, :inet_type, :int_type, :list_type, :map_type,
	:set_type, :text_type, :timestamp_type, :uuid_type, :timeuuid_type,
	:varchar_type, :varint_type, :blob_type
)''',
            consistency: cql.Consistency.one,
            prepared: true,
          );

          query.bindings = {
            'ascii_type': '123',
            'bigint_type': 123451234,
            'decimal_type': 3.14,
            'bool_type': true,
            'double_type': 3.14,
            'float_type': 3.14,
            'inet_type': InternetAddress('192.168.169.101'),
            'int_type': 10,
            'list_type': [1, 2, 3],
            'map_type': {1: 'A', 2: 'BC'},
            'set_type': [1, 2, 3],
            'text_type': 'Long text',
            'timestamp_type': DateTime.now(),
            'uuid_type': cql.Uuid.simple(),
            'timeuuid_type': cql.Uuid.timeBased(),
            'varchar_type': 'test 123',
            'varint_type': BigInt.from(123456),
            'blob_type': Uint8List.fromList([
              0x8B,
              0xAD,
              0xF0,
              0x00,
              0x0D,
              0xF0,
              0x00,
              0x0C,
              0x00,
              0x0F,
              0xF3,
              0x0E
            ])
          };

          bool firstResponse = true;

          Future done(cql.ResultMessage? msg) async {
            expect(msg, isA<cql.VoidResultMessage>());
            if (firstResponse) {
              firstResponse = false;
              // Kill 1st server so we prepare the query again on server2
              // on the second connection
              return server.shutdown();
            }
          }

          await done(await client!.execute(query));
          // Shutdown between calls.
          await done(await client!.execute(query));
          await client?.shutdown();
        });

        test(
            'prepare and execute query; NoHealthyConnections exception after server1 dies (V2)',
            () async {
          server.setReplayList(['prepare_v2.dump', 'void_result_v2.dump']);
          client = cql.Client.fromHostList(
            ['$serverHost:$serverPort'],
            poolConstructor: constructor,
            poolConfig: cql.PoolConfiguration(
              autoDiscoverNodes: false,
              connectionsPerHost: 2,
              connectionTimeout: const Duration(milliseconds: 200),
            ),
          );

          final cql.Query query = cql.Query(
            '''
INSERT INTO test.type_test (
	ascii_type, bigint_type, decimal_type, bool_type,
	double_type, float_type, inet_type, int_type, list_type, map_type,
	set_type, text_type, timestamp_type, uuid_type, timeuuid_type,
	varchar_type, varint_type, blob_type
) VALUES (
  :ascii_type, :bigint_type, :decimal_type, :bool_type,
	:double_type, :float_type, :inet_type, :int_type, :list_type, :map_type,
	:set_type, :text_type, :timestamp_type, :uuid_type, :timeuuid_type,
	:varchar_type, :varint_type, :blob_type
)''',
            consistency: cql.Consistency.one,
            serialConsistency: cql.Consistency.localSerial,
            prepared: true,
          );

          query.bindings = {
            'ascii_type': '123',
            'bigint_type': 123451234,
            'decimal_type': 3.14,
            'bool_type': true,
            'double_type': 3.14,
            'float_type': 3.14,
            'inet_type': InternetAddress('192.168.169.101'),
            'int_type': 10,
            'list_type': [1, 2, 3],
            'map_type': {1: 'A', 2: 'BC'},
            'set_type': [1, 2, 3],
            'text_type': 'Long text',
            'timestamp_type': DateTime.now(),
            'uuid_type': cql.Uuid.simple(),
            'timeuuid_type': cql.Uuid.timeBased(),
            'varchar_type': 'test 123',
            'varint_type': BigInt.from(123456),
            'blob_type': Uint8List.fromList([
              0x8B,
              0xAD,
              0xF0,
              0x00,
              0x0D,
              0xF0,
              0x00,
              0x0C,
              0x00,
              0x0F,
              0xF3,
              0x0E
            ])
          };

          await client!.execute(query);
          await server.shutdown();
          expect(
            () => client!.execute(query),
            throwsA(isA<cql.NoHealthyConnectionsException>()),
          );
        });

        test('prepare and execute query (V3)', () async {
          server.setReplayList(['prepare_v3.dump', 'void_result_v3.dump']);
          client = cql.Client.fromHostList(
            ['$serverHost:$serverPort'],
            poolConstructor: constructor,
            poolConfig: cql.PoolConfiguration(
              autoDiscoverNodes: false,
            ),
          );

          final cql.Query query = cql.Query(
            '''
INSERT INTO test.type_test (
	ascii_type, bigint_type, decimal_type, bool_type,
	double_type, float_type, inet_type, int_type, list_type, map_type,
	set_type, text_type, timestamp_type, uuid_type, timeuuid_type,
	varchar_type, varint_type, blob_type
) VALUES (
  :ascii_type, :bigint_type, :decimal_type, :bool_type,
	:double_type, :float_type, :inet_type, :int_type, :list_type, :map_type,
	:set_type, :text_type, :timestamp_type, :uuid_type, :timeuuid_type,
	:varchar_type, :varint_type, :blob_type
)''',
            consistency: cql.Consistency.one,
            prepared: true,
          );

          query.bindings = {
            'ascii_type': '123',
            'bigint_type': 123451234,
            'decimal_type': 3.14,
            'bool_type': true,
            'double_type': 3.14,
            'float_type': 3.14,
            'inet_type': InternetAddress('192.168.169.101'),
            'int_type': 10,
            'list_type': [1, 2, 3],
            'map_type': {1: 'A', 2: 'BC'},
            'set_type': [1, 2, 3],
            'text_type': 'Long text',
            'timestamp_type': DateTime.now(),
            'uuid_type': cql.Uuid.simple(),
            'timeuuid_type': cql.Uuid.timeBased(),
            'varchar_type': 'test 123',
            'varint_type': BigInt.from(123456),
            'blob_type': Uint8List.fromList([
              0x8B,
              0xAD,
              0xF0,
              0x00,
              0x0D,
              0xF0,
              0x00,
              0x0C,
              0x00,
              0x0F,
              0xF3,
              0x0E
            ])
          };

          await client!.execute(query);
        });
      });

      group('server events:', () {
        group('STATUS_CHANGE (V2):', () {
          test(
              'server2 host up event while server1 suddenly dies; automatically connect to server2',
              () async {
            server2.setReplayList(['set_keyspace_v2.dump']);

            final cql.PoolConfiguration poolConfig =
                cql.PoolConfiguration(reconnectWaitTime: Duration.zero);
            client = cql.Client.fromHostList(
              ['$serverHost:$serverPort'],
              poolConstructor: constructor,
              poolConfig: poolConfig,
            );

            void handleResult(cql.ResultMessage message) {
              expect(message, isA<cql.SetKeyspaceResultMessage>());
              expect(
                (message as cql.SetKeyspaceResultMessage).keyspace,
                equals('test'),
              );
            }

            await client!.connectionPool.connect();
            await Future.delayed(
              const Duration(milliseconds: 100),
              () => server.replayFile(0, 'event_status_up_v2.dump'),
            );
            await Future.delayed(
              const Duration(milliseconds: 100),
              () => server.shutdown(),
            );
            final result = await Future.delayed(
              const Duration(milliseconds: 200),
              () => client!.execute(cql.Query('USE test')),
            );
            handleResult(result!);
          });

          test(
              'server2 host down event; pending queries to server2 should automatically fail',
              () async {
            server2.setReplayList([
              // Intentionally empty so the client gets stuck waiting for the server to reply
            ]);

            final cql.PoolConfiguration poolConfig = cql.PoolConfiguration(
              reconnectWaitTime: const Duration(
                milliseconds: 1,
              ) // Keep reconnect time low for our test
              ,
              connectionTimeout: const Duration(milliseconds: 200),
            );
            client = cql.Client.fromHostList(
              ['$serverHost:$server2Port'],
              poolConstructor: constructor,
              poolConfig: poolConfig,
            );

            void handleError(e) {
              expect(e, isA<cql.NoHealthyConnectionsException>());
            }

            try {
              await client!.connectionPool.connect();
              await Future.delayed(
                const Duration(milliseconds: 20),
                () => server2.replayFile(0, 'event_status_down_v2.dump'),
              );
              await Future.delayed(
                const Duration(milliseconds: 100),
                () => client!.execute(cql.Query('USE test')),
              );
            } catch (error) {
              handleError(error);
            }
          });

          test('server2 host down then host up event', () async {
            server2.setReplayList(['set_keyspace_v2.dump']);

            final cql.PoolConfiguration poolConfig = cql.PoolConfiguration(
              reconnectWaitTime: Duration.zero,
            );
            client = cql.Client.fromHostList(
              ['$serverHost:$serverPort', '$serverHost:$server2Port'],
              poolConstructor: constructor,
              poolConfig: poolConfig,
            );

            await client!.connectionPool.connect();
            Timer(
              const Duration(milliseconds: 100),
              () => server.replayFile(0, 'event_status_down_v2.dump'),
            );
            Timer(
              const Duration(milliseconds: 200),
              () => server.replayFile(0, 'event_status_up_v2.dump'),
            );
            await Future.delayed(
              const Duration(milliseconds: 400),
              () => server.shutdown(),
            );
            final message = await client!.execute(cql.Query('USE test'));
            expect(message, isA<cql.SetKeyspaceResultMessage>());
            expect(
              (message! as cql.SetKeyspaceResultMessage).keyspace,
              equals('test'),
            );
          });
        });

        group('TOPOLOGY CHANGE (V3):', () {
          test('server2 leaves cluster then re-joins', () async {
            server2.setReplayList(['set_keyspace_v2.dump']);

            final cql.PoolConfiguration poolConfig = cql.PoolConfiguration(
              reconnectWaitTime: Duration.zero,
            );
            client = cql.Client.fromHostList(
              ['$serverHost:$serverPort', '$serverHost:$server2Port'],
              poolConstructor: constructor,
              poolConfig: poolConfig,
            );

            await client!.connectionPool.connect();
            // Wait for event registration message to be received and then reply the event message
            Timer(
              const Duration(milliseconds: 100),
              () => server.replayFile(0, 'event_removed_node_v3.dump'),
            );
            Timer(
              const Duration(milliseconds: 200),
              () => server.replayFile(0, 'event_new_node_v3.dump'),
            );
            await Future.delayed(
              const Duration(milliseconds: 300),
              () => server.shutdown(),
            );
            final message = await client!.execute(cql.Query('USE test'));

            expect(message, isA<cql.SetKeyspaceResultMessage>());
            expect(
              (message! as cql.SetKeyspaceResultMessage).keyspace,
              equals('test'),
            );
          });
        });

        group('SCHEMA CHANGE:', () {
          group('V2:', () {
            group('KEYSPACE:', () {
              test('created', () async {
                final cql.PoolConfiguration poolConfig = cql.PoolConfiguration(
                  reconnectWaitTime: Duration.zero,
                  protocolVersion: cql.ProtocolVersion.v2,
                );
                client = cql.Client.fromHostList(
                  ['$serverHost:$serverPort'],
                  poolConstructor: constructor,
                  poolConfig: poolConfig,
                );

                void handleMessage(cql.EventMessage message) {
                  expect(
                    message.type,
                    equals(cql.EventRegistrationType.schemaChange),
                  );
                  expect(message.subType, equals(cql.EventType.schemaCreated));
                  expect(message.keyspace, equals('test'));
                  expect(message.changedTable, isNull);
                  expect(message.changedType, isNull);
                  expect(message.address, isNull);
                  expect(message.port, isNull);
                }

                client!.connectionPool.listenForServerEvents(
                  [cql.EventRegistrationType.schemaChange],
                ).listen(
                  expectAsync1(handleMessage),
                );

                await client!.connectionPool.connect();
                await Future.delayed(
                  const Duration(milliseconds: 100),
                  () => server.replayFile(
                    0,
                    'event_schema_created_keyspace_v2.dump',
                  ),
                );
              });

              test('dropped', () async {
                final cql.PoolConfiguration poolConfig = cql.PoolConfiguration(
                  reconnectWaitTime: Duration.zero,
                  protocolVersion: cql.ProtocolVersion.v2,
                );
                client = cql.Client.fromHostList(
                  ['$serverHost:$serverPort'],
                  poolConstructor: constructor,
                  poolConfig: poolConfig,
                );

                void handleMessage(cql.EventMessage message) {
                  expect(
                    message.type,
                    equals(cql.EventRegistrationType.schemaChange),
                  );
                  expect(message.subType, equals(cql.EventType.schemaDropped));
                  expect(message.keyspace, equals('test'));
                  expect(message.changedTable, isNull);
                  expect(message.changedType, isNull);
                  expect(message.address, isNull);
                  expect(message.port, isNull);
                }

                client!.connectionPool.listenForServerEvents([
                  cql.EventRegistrationType.schemaChange
                ]).listen(expectAsync1(handleMessage));

                await client!.connectionPool.connect();
                await Future.delayed(
                  const Duration(milliseconds: 100),
                  () => server.replayFile(
                    0,
                    'event_schema_dropped_keyspace_v2.dump',
                  ),
                );
              });
            });

            group('TABLE:', () {
              test('created', () async {
                final cql.PoolConfiguration poolConfig = cql.PoolConfiguration(
                  reconnectWaitTime: Duration.zero,
                  protocolVersion: cql.ProtocolVersion.v2,
                );
                client = cql.Client.fromHostList(
                  ['$serverHost:$serverPort'],
                  poolConstructor: constructor,
                  poolConfig: poolConfig,
                );

                void handleMessage(cql.EventMessage message) {
                  expect(
                    message.type,
                    equals(cql.EventRegistrationType.schemaChange),
                  );
                  expect(message.subType, equals(cql.EventType.schemaCreated));
                  expect(message.keyspace, equals('test'));
                  expect(message.changedTable, 'type_test');
                  expect(message.changedType, isNull);
                  expect(message.address, isNull);
                  expect(message.port, isNull);
                }

                client!.connectionPool.listenForServerEvents(
                  [cql.EventRegistrationType.schemaChange],
                ).listen(
                  expectAsync1(handleMessage),
                );

                await client!.connectionPool.connect();
                await Future.delayed(
                  const Duration(milliseconds: 100),
                  () => server.replayFile(
                    0,
                    'event_schema_created_table_v2.dump',
                  ),
                );
              });

              test('dropped', () async {
                final cql.PoolConfiguration poolConfig = cql.PoolConfiguration(
                  reconnectWaitTime: Duration.zero,
                );
                client = cql.Client.fromHostList(
                  ['$serverHost:$serverPort'],
                  poolConstructor: constructor,
                  poolConfig: poolConfig,
                );

                void handleMessage(cql.EventMessage message) {
                  expect(
                    message.type,
                    equals(cql.EventRegistrationType.schemaChange),
                  );
                  expect(message.subType, equals(cql.EventType.schemaDropped));
                  expect(message.keyspace, equals('test'));
                  expect(message.changedTable, 'type_test');
                  expect(message.changedType, isNull);
                  expect(message.address, isNull);
                  expect(message.port, isNull);
                }

                client!.connectionPool.listenForServerEvents(
                  [cql.EventRegistrationType.schemaChange],
                ).listen(
                  expectAsync1(handleMessage),
                );

                await client!.connectionPool.connect();
                await Future.delayed(
                  const Duration(milliseconds: 100),
                  () => server.replayFile(
                    0,
                    'event_schema_dropped_table_v2.dump',
                  ),
                );
              });

              test('updated', () async {
                final cql.PoolConfiguration poolConfig = cql.PoolConfiguration(
                  reconnectWaitTime: Duration.zero,
                  protocolVersion: cql.ProtocolVersion.v2,
                );
                client = cql.Client.fromHostList(
                  ['$serverHost:$serverPort'],
                  poolConstructor: constructor,
                  poolConfig: poolConfig,
                );

                void handleMessage(cql.EventMessage message) {
                  expect(
                    message.type,
                    equals(cql.EventRegistrationType.schemaChange),
                  );
                  expect(message.subType, equals(cql.EventType.schemaUpdated));
                  expect(message.keyspace, equals('test'));
                  expect(message.changedTable, 'type_test');
                  expect(message.changedType, isNull);
                  expect(message.address, isNull);
                  expect(message.port, isNull);
                }

                client!.connectionPool.listenForServerEvents([
                  cql.EventRegistrationType.schemaChange
                ]).listen(expectAsync1(handleMessage));

                await client!.connectionPool.connect();
                await Future.delayed(
                  const Duration(milliseconds: 100),
                  () => server.replayFile(
                    0,
                    'event_schema_updated_table_v2.dump',
                  ),
                );
              });
            });
          });
        });

        group('V3:', () {
          group('KEYSPACE:', () {
            test('created', () async {
              final cql.PoolConfiguration poolConfig = cql.PoolConfiguration(
                reconnectWaitTime: Duration.zero,
              );
              client = cql.Client.fromHostList(
                ['$serverHost:$serverPort'],
                poolConstructor: constructor,
                poolConfig: poolConfig,
              );

              void handleMessage(cql.EventMessage message) {
                expect(
                  message.type,
                  equals(cql.EventRegistrationType.schemaChange),
                );
                expect(message.subType, equals(cql.EventType.schemaCreated));
                expect(message.keyspace, equals('test'));
                expect(message.changedTable, isNull);
                expect(message.changedType, isNull);
                expect(message.address, isNull);
                expect(message.port, isNull);
              }

              client!.connectionPool.listenForServerEvents(
                [cql.EventRegistrationType.schemaChange],
              ).listen(
                expectAsync1(handleMessage),
              );

              await client!.connectionPool.connect();
              await Future.delayed(
                const Duration(milliseconds: 100),
                () => server.replayFile(
                  0,
                  'event_schema_created_keyspace_v3.dump',
                ),
              );
            });

            test('dropped', () async {
              final cql.PoolConfiguration poolConfig = cql.PoolConfiguration(
                reconnectWaitTime: Duration.zero,
              );
              client = cql.Client.fromHostList(
                ['$serverHost:$serverPort'],
                poolConstructor: constructor,
                poolConfig: poolConfig,
              );

              void handleMessage(cql.EventMessage message) {
                expect(
                  message.type,
                  equals(cql.EventRegistrationType.schemaChange),
                );
                expect(message.subType, equals(cql.EventType.schemaDropped));
                expect(message.keyspace, equals('test'));
                expect(message.changedTable, isNull);
                expect(message.changedType, isNull);
                expect(message.address, isNull);
                expect(message.port, isNull);
              }

              client!.connectionPool.listenForServerEvents(
                [cql.EventRegistrationType.schemaChange],
              ).listen(
                expectAsync1(handleMessage),
              );

              await client!.connectionPool.connect();
              await Future.delayed(
                const Duration(milliseconds: 100),
                () => server.replayFile(
                  0,
                  'event_schema_dropped_keyspace_v3.dump',
                ),
              );
            });
          });

          group('TABLE:', () {
            test('created', () async {
              final cql.PoolConfiguration poolConfig = cql.PoolConfiguration(
                reconnectWaitTime: Duration.zero,
              );
              client = cql.Client.fromHostList(
                ['$serverHost:$serverPort'],
                poolConstructor: constructor,
                poolConfig: poolConfig,
              );

              void handleMessage(cql.EventMessage message) {
                expect(
                  message.type,
                  equals(cql.EventRegistrationType.schemaChange),
                );
                expect(message.subType, equals(cql.EventType.schemaCreated));
                expect(message.keyspace, equals('test'));
                expect(message.changedTable, 'type_test');
                expect(message.changedType, isNull);
                expect(message.address, isNull);
                expect(message.port, isNull);
              }

              client!.connectionPool.listenForServerEvents(
                [cql.EventRegistrationType.schemaChange],
              ).listen(
                expectAsync1(handleMessage),
              );

              await client!.connectionPool.connect();
              await Future.delayed(
                const Duration(milliseconds: 100),
                () => server.replayFile(
                  0,
                  'event_schema_created_table_v3.dump',
                ),
              );
            });

            test('dropped', () async {
              final cql.PoolConfiguration poolConfig = cql.PoolConfiguration(
                reconnectWaitTime: Duration.zero,
              );
              client = cql.Client.fromHostList(
                ['$serverHost:$serverPort'],
                poolConstructor: constructor,
                poolConfig: poolConfig,
              );

              void handleMessage(cql.EventMessage message) {
                expect(
                  message.type,
                  equals(cql.EventRegistrationType.schemaChange),
                );
                expect(message.subType, equals(cql.EventType.schemaDropped));
                expect(message.keyspace, equals('test'));
                expect(message.changedTable, 'type_test');
                expect(message.changedType, isNull);
                expect(message.address, isNull);
                expect(message.port, isNull);
              }

              client!.connectionPool.listenForServerEvents(
                [cql.EventRegistrationType.schemaChange],
              ).listen(
                expectAsync1(handleMessage),
              );

              await client!.connectionPool.connect();
              await Future.delayed(
                const Duration(milliseconds: 100),
                () => server.replayFile(
                  0,
                  'event_schema_dropped_table_v3.dump',
                ),
              );
            });

            test('updated', () async {
              final cql.PoolConfiguration poolConfig = cql.PoolConfiguration(
                reconnectWaitTime: Duration.zero,
              );
              client = cql.Client.fromHostList(
                ['$serverHost:$serverPort'],
                poolConstructor: constructor,
                poolConfig: poolConfig,
              );

              void handleMessage(cql.EventMessage message) {
                expect(
                  message.type,
                  equals(cql.EventRegistrationType.schemaChange),
                );
                expect(message.subType, equals(cql.EventType.schemaUpdated));
                expect(message.keyspace, equals('test'));
                expect(message.changedTable, 'type_test');
                expect(message.changedType, isNull);
                expect(message.address, isNull);
                expect(message.port, isNull);
              }

              client!.connectionPool.listenForServerEvents([
                cql.EventRegistrationType.schemaChange
              ]).listen(expectAsync1(handleMessage));

              await client!.connectionPool.connect();
              await Future.delayed(
                const Duration(milliseconds: 100),
                () => server.replayFile(
                  0,
                  'event_schema_updated_table_v3.dump',
                ),
              );
            });
          });

          group('TYPE:', () {
            test('created', () async {
              final cql.PoolConfiguration poolConfig = cql.PoolConfiguration(
                reconnectWaitTime: Duration.zero,
              );
              client = cql.Client.fromHostList(
                ['$serverHost:$serverPort'],
                poolConstructor: constructor,
                poolConfig: poolConfig,
              );

              void handleMessage(cql.EventMessage message) {
                expect(
                  message.type,
                  equals(cql.EventRegistrationType.schemaChange),
                );
                expect(message.subType, equals(cql.EventType.schemaCreated));
                expect(message.keyspace, equals('test'));
                expect(message.changedTable, isNull);
                expect(message.changedType, 'phone');
                expect(message.address, isNull);
                expect(message.port, isNull);
              }

              client!.connectionPool.listenForServerEvents(
                [cql.EventRegistrationType.schemaChange],
              ).listen(
                expectAsync1(handleMessage),
              );

              await client!.connectionPool.connect();
              await Future.delayed(
                const Duration(milliseconds: 100),
                () => server.replayFile(
                  0,
                  'event_schema_created_type_v3.dump',
                ),
              );
            });

            test('dropped', () async {
              final cql.PoolConfiguration poolConfig = cql.PoolConfiguration(
                reconnectWaitTime: Duration.zero,
              );
              client = cql.Client.fromHostList(
                ['$serverHost:$serverPort'],
                poolConstructor: constructor,
                poolConfig: poolConfig,
              );

              void handleMessage(cql.EventMessage message) {
                expect(
                  message.type,
                  equals(cql.EventRegistrationType.schemaChange),
                );
                expect(message.subType, equals(cql.EventType.schemaDropped));
                expect(message.keyspace, equals('test'));
                expect(message.changedTable, isNull);
                expect(message.changedType, 'phone');
                expect(message.address, isNull);
                expect(message.port, isNull);
              }

              client!.connectionPool.listenForServerEvents(
                [cql.EventRegistrationType.schemaChange],
              ).listen(
                expectAsync1(handleMessage),
              );

              await client!.connectionPool.connect();
              await Future.delayed(
                const Duration(milliseconds: 100),
                () => server.replayFile(
                  0,
                  'event_schema_dropped_type_v3.dump',
                ),
              );
            });

            test('updated', () async {
              final cql.PoolConfiguration poolConfig = cql.PoolConfiguration(
                reconnectWaitTime: Duration.zero,
              );
              client = cql.Client.fromHostList(
                ['$serverHost:$serverPort'],
                poolConstructor: constructor,
                poolConfig: poolConfig,
              );

              void handleMessage(cql.EventMessage message) {
                expect(
                  message.type,
                  equals(cql.EventRegistrationType.schemaChange),
                );
                expect(message.subType, equals(cql.EventType.schemaUpdated));
                expect(message.keyspace, equals('test'));
                expect(message.changedTable, isNull);
                expect(message.changedType, 'phone');
                expect(message.address, isNull);
                expect(message.port, isNull);
              }

              client!.connectionPool.listenForServerEvents(
                [cql.EventRegistrationType.schemaChange],
              ).listen(
                expectAsync1(handleMessage),
              );

              await client!.connectionPool.connect();
              await Future.delayed(
                const Duration(milliseconds: 100),
                () => server.replayFile(
                  0,
                  'event_schema_updated_type_v3.dump',
                ),
              );
            });
          });
        });
      });
    });
  }
}
