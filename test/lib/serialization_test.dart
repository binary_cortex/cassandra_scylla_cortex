library cassandra_scylla_cortex.tests.serialization;

import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
import 'package:cassandra_scylla_cortex/src/stream.dart';
import 'package:cassandra_scylla_cortex/src/types.dart';
import 'package:test/test.dart';
import 'mocks/custom.dart' as custom;
import 'mocks/mocks.dart' as mock;

void main({bool enableLogger = false}) {
  if (enableLogger) {
    mock.initLogger();
  }

  final custom.CustomJson customJsonInstance = custom.CustomJson({});

  group('Serialization', () {
    late TypeEncoder encoder;
    late SizeType size;

    group('Exceptions:', () {
      setUp(() {
        encoder = TypeEncoder(ProtocolVersion.v3);
        size = SizeType.long;
      });

      tearDown(() {
        unregisterCodec(customJsonInstance.customTypeClass);
      });

      group('TypeSpec:', () {
        test('Missing key/valueSubTYpe', () {
          expect(
            () => TypeSpec(DataType.map),
            throwsA(
              predicate(
                (dynamic e) =>
                    e is ArgumentError &&
                    e.message ==
                        'MAP type should specify TypeSpec instances for both its keys and values',
              ),
            ),
          );

          expect(
            () => TypeSpec(DataType.map, keySubType: TypeSpec(DataType.ascii)),
            throwsA(
              predicate(
                (dynamic e) =>
                    e is ArgumentError &&
                    e.message ==
                        'MAP type should specify TypeSpec instances for both its keys and values',
              ),
            ),
          );

          expect(
            () =>
                TypeSpec(DataType.map, valueSubType: TypeSpec(DataType.ascii)),
            throwsA(
              predicate(
                (dynamic e) =>
                    e is ArgumentError &&
                    e.message ==
                        'MAP type should specify TypeSpec instances for both its keys and values',
              ),
            ),
          );
        });

        test('Missing valueSubType', () {
          expect(
            () => TypeSpec(DataType.list),
            throwsA(
              predicate(
                (dynamic e) =>
                    e is ArgumentError &&
                    e.message ==
                        'LIST type should specify a TypeSpec instance for its values',
              ),
            ),
          );

          expect(
            () => TypeSpec(DataType.set),
            throwsA(
              predicate(
                (dynamic e) =>
                    e is ArgumentError &&
                    e.message ==
                        'SET type should specify a TypeSpec instance for its values',
              ),
            ),
          );
        });
      });

      test('Not instance of DateTime', () {
        const Object input = 'foo';
        final TypeSpec type = TypeSpec(DataType.timestamp);
        expect(
          () => encoder.writeTypedValue(
            'test',
            input,
            typeSpec: type,
            size: size,
          ),
          throwsA(
            predicate(
              (dynamic e) =>
                  e is ArgumentError &&
                  e.message ==
                      "Expected value for field 'test' of type timestamp to be an instance of DateTime",
            ),
          ),
        );
      });

      test('Not instance of Uint8List or CustomType', () {
        const Object input = 'foo';
        TypeSpec type = TypeSpec(DataType.custom);
        expect(
          () => encoder.writeTypedValue(
            'test',
            input,
            typeSpec: type,
            size: size,
          ),
          throwsA(
            predicate(
              (dynamic e) =>
                  e is ArgumentError &&
                  e.message ==
                      "Expected value for field 'test' of type custom to be an instance of Uint8List OR an instance of CustomType with a registered type handler",
            ),
          ),
        );

        expect(
          () => encoder.writeTypedValue(
            'test',
            Uint16List.fromList([]),
            typeSpec: type,
            size: size,
          ),
          throwsA(
            predicate(
              (dynamic e) =>
                  e is ArgumentError &&
                  e.message ==
                      "Expected value for field 'test' of type custom to be an instance of Uint8List OR an instance of CustomType with a registered type handler",
            ),
          ),
        );

        unregisterCodec(customJsonInstance.customTypeClass);
        expect(
          () => encoder.writeTypedValue(
            'test',
            customJsonInstance,
            typeSpec: type,
            size: size,
          ),
          throwsA(
            predicate(
              (dynamic e) =>
                  e is ArgumentError &&
                  e.message ==
                      'No custom type handler codec registered for custom type: ${customJsonInstance.customTypeClass}',
            ),
          ),
        );

        type = TypeSpec(DataType.blob);
        expect(
          () => encoder.writeTypedValue(
            'test',
            input,
            typeSpec: type,
            size: size,
          ),
          throwsA(
            predicate(
              (dynamic e) =>
                  e is ArgumentError &&
                  e.message ==
                      "Expected value for field 'test' of type blob to be an instance of Uint8List",
            ),
          ),
        );
      });

      test('Not instance of InternetAddress', () {
        const Object input = 'foo';
        final TypeSpec type = TypeSpec(DataType.inet);
        expect(
          () => encoder.writeTypedValue(
            'test',
            input,
            typeSpec: type,
            size: size,
          ),
          throwsA(
            predicate(
              (dynamic e) =>
                  e is ArgumentError &&
                  e.message ==
                      "Expected value for field 'test' of type inet to be an instance of InternetAddress",
            ),
          ),
        );
      });

      test('Not instance of Iterable', () {
        const Object input = 'foo';
        TypeSpec type =
            TypeSpec(DataType.list, valueSubType: TypeSpec(DataType.ascii));
        expect(
          () => encoder.writeTypedValue(
            'test',
            input,
            typeSpec: type,
            size: size,
          ),
          throwsA(
            predicate(
              (dynamic e) =>
                  e is ArgumentError &&
                  e.message ==
                      "Expected value for field 'test' of type list to implement Iterable",
            ),
          ),
        );
        type = TypeSpec(DataType.set, valueSubType: TypeSpec(DataType.ascii));
        expect(
          () => encoder.writeTypedValue(
            'test',
            input,
            typeSpec: type,
            size: size,
          ),
          throwsA(
            predicate(
              (dynamic e) =>
                  e is ArgumentError &&
                  e.message ==
                      "Expected value for field 'test' of type set to implement Iterable",
            ),
          ),
        );
      });

      test('Not instance of Map', () {
        const Object input = 'foo';
        TypeSpec type = TypeSpec(
          DataType.map,
          keySubType: TypeSpec(DataType.ascii),
          valueSubType: TypeSpec(DataType.ascii),
        );
        expect(
          () => encoder.writeTypedValue(
            'test',
            input,
            typeSpec: type,
            size: size,
          ),
          throwsA(
            predicate(
              (dynamic e) =>
                  e is ArgumentError &&
                  e.message ==
                      "Expected value for field 'test' of type map to implement Map",
            ),
          ),
        );

        type = TypeSpec(DataType.udt);
        expect(
          () => encoder.writeTypedValue(
            'test',
            input,
            typeSpec: type,
            size: size,
          ),
          throwsA(
            predicate(
              (dynamic e) =>
                  e is ArgumentError &&
                  e.message ==
                      "Expected value for field 'test' of type udt to implement Map",
            ),
          ),
        );
      });

      test('Not instance of Tuple', () {
        const Object input = ['foo'];
        final TypeSpec type = TypeSpec(DataType.tuple);
        expect(
          () => encoder.writeTypedValue(
            'test',
            input,
            typeSpec: type,
            size: size,
          ),
          throwsA(
            predicate(
              (dynamic e) =>
                  e is ArgumentError &&
                  e.message ==
                      "Expected value for field 'test' of type tuple to be an instance of Tuple",
            ),
          ),
        );
      });
    });

    group('Internal types:', () {
      setUp(() {
        encoder = TypeEncoder(ProtocolVersion.v2);
        size = SizeType.short;
      });

      test('Consistency', () {
        const Consistency input = Consistency.localOne;
        final int value = ConsistencyExt().encode(input);
        encoder.writer!.addLast(Uint8List.fromList([0x00, value]));
        final Object output = mock.createDecoder(encoder).readConsistency();

        expect(output, equals(input));
      });

      test('String list', () {
        final List<String> input = ['a', 'foo', 'f33dfAce'];
        encoder.writeStringList(input, size);
        final Object output = mock.createDecoder(encoder).readStringList(size);
        expect(output, equals(input));
      });

      test('String map', () {
        final input = <String, String>{'foo': 'bar', 'baz00ka': 'f33df4ce'};
        encoder.writeStringMap(input, size);
        final Object output = mock.createDecoder(encoder).readStringMap(size);
        expect(output, equals(input));
      });

      test('String multimap', () {
        final input = <String, List<String>>{
          'foo': ['bar', 'baz'],
          'baz00ka': ['f33df4ce']
        };
        encoder.writeStringMultiMap(input, size);
        final Object output =
            mock.createDecoder(encoder).readStringMultiMap(size);
        expect(output, equals(input));
      });
    });

    group('(protocol V2):', () {
      setUp(() {
        encoder = TypeEncoder(ProtocolVersion.v2);
        size = SizeType.short;
      });

      test('UTF-8 STRING', () {
        const Object input = 'Test 123 AbC !@#ΤΕΣΤ';
        final TypeSpec type = TypeSpec(DataType.text);
        encoder.writeTypedValue('test', input, typeSpec: type, size: size);
        final Object? output =
            mock.createDecoder(encoder).readTypedValue(type, size: size);

        expect(output, equals(input));
      });

      test('ASCII STRING', () {
        const Object input = 'Test 123 AbC';
        final TypeSpec type = TypeSpec(DataType.ascii);
        encoder.writeTypedValue('test', input, typeSpec: type, size: size);
        final Object? output =
            mock.createDecoder(encoder).readTypedValue(type, size: size);

        expect(output, equals(input));
      });

      test('UUID', () {
        final Object input = Uuid.simple();
        final TypeSpec type = TypeSpec(DataType.uuid);
        encoder.writeTypedValue('test', input, typeSpec: type, size: size);
        final Object? output =
            mock.createDecoder(encoder).readTypedValue(type, size: size);

        expect(output, equals(input));
      });

      test('TIMEUUID', () {
        final Object input = Uuid.timeBased();
        final TypeSpec type = TypeSpec(DataType.timeuuid);
        encoder.writeTypedValue('test', input, typeSpec: type, size: size);
        final Object? output =
            mock.createDecoder(encoder).readTypedValue(type, size: size);

        expect(output, equals(input));
      });

      group('CUSTOM:', () {
        test('without type handler', () {
          final Object input = Uint8List.fromList(
            List<int>.generate(10, (int index) => index * 2),
          );
          final TypeSpec type = TypeSpec(DataType.custom)
            ..customTypeClass = customJsonInstance.customTypeClass;
          encoder.writeTypedValue('test', input, typeSpec: type, size: size);
          final Object? output =
              mock.createDecoder(encoder).readTypedValue(type, size: size);

          expect(output, equals(input));
        });

        test('with type handler', () {
          // Register custom type handler
          registerCodec(
            'com.achilleasa.cassandra.cqltypes.Json',
            custom.CustomJsonCodec(),
          );

          customJsonInstance.payload = {
            'foo': {'bar': 'baz'}
          };

          final TypeSpec type = TypeSpec(DataType.custom)
            ..customTypeClass = customJsonInstance.customTypeClass;

          encoder.writeTypedValue(
            'test',
            customJsonInstance,
            typeSpec: type,
            size: size,
          );
          final Object? output =
              mock.createDecoder(encoder).readTypedValue(type, size: size);
          expect(output, isA<custom.CustomJson>());
          expect(
            (output as custom.CustomJson?)?.payload,
            equals(customJsonInstance.payload),
          );
        });
      });

      test('BLOB', () {
        final Object input = Uint8List.fromList(
          List<int>.generate(10, (int index) => index * 2),
        );
        final TypeSpec type = TypeSpec(DataType.blob);
        encoder.writeTypedValue('test', input, typeSpec: type, size: size);
        final Object? output =
            mock.createDecoder(encoder).readTypedValue(type, size: size);

        expect(output, equals(input));
      });

      group('COUNTER', () {
        test('(positive)', () {
          const Object input = 9223372036854775807;
          final TypeSpec type = TypeSpec(DataType.counter);
          encoder.writeTypedValue('test', input, typeSpec: type, size: size);
          final Object? output =
              mock.createDecoder(encoder).readTypedValue(type, size: size);

          expect(output, equals(input));
        });

        test('(negative)', () {
          const Object input = -1;
          final TypeSpec type = TypeSpec(DataType.counter);
          encoder.writeTypedValue('test', input, typeSpec: type, size: size);
          final Object? output =
              mock.createDecoder(encoder).readTypedValue(type, size: size);

          expect(output, equals(input));
        });
      });

      test('TIMESTAMP', () {
        final Object input = DateTime.fromMillisecondsSinceEpoch(1455746327000);
        final TypeSpec type = TypeSpec(DataType.timestamp);
        encoder.writeTypedValue('test', input, typeSpec: type, size: size);
        final Object? output =
            mock.createDecoder(encoder).readTypedValue(type, size: size);

        expect(output, equals(input));
      });

      group('BOOLEAN', () {
        test('(true)', () {
          const Object input = true;
          final TypeSpec type = TypeSpec(DataType.boolean);
          encoder.writeTypedValue('test', input, typeSpec: type, size: size);
          final Object? output =
              mock.createDecoder(encoder).readTypedValue(type, size: size);

          expect(output, equals(input));
        });

        test('(false)', () {
          const Object input = false;
          final TypeSpec type = TypeSpec(DataType.boolean);
          encoder.writeTypedValue('test', input, typeSpec: type, size: size);
          final Object? output =
              mock.createDecoder(encoder).readTypedValue(type, size: size);

          expect(output, equals(input));
        });
      });

      group('BOOLEAN', () {
        test('(true)', () {
          const Object input = true;
          final TypeSpec type = TypeSpec(DataType.boolean);
          encoder.writeTypedValue('test', input, typeSpec: type, size: size);
          final Object? output =
              mock.createDecoder(encoder).readTypedValue(type, size: size);

          expect(output, equals(input));
        });

        test('(false)', () {
          const Object input = false;
          final TypeSpec type = TypeSpec(DataType.boolean);
          encoder.writeTypedValue('test', input, typeSpec: type, size: size);
          final Object? output =
              mock.createDecoder(encoder).readTypedValue(type, size: size);

          expect(output, equals(input));
        });
      });

      group('INET:', () {
        test('(ipv4)', () {
          final Object input = InternetAddress('192.168.169.101');
          final TypeSpec type = TypeSpec(DataType.inet);
          encoder.writeTypedValue('test', input, typeSpec: type, size: size);
          final Object? output =
              mock.createDecoder(encoder).readTypedValue(type, size: size);

          expect(output, equals(input));
        });

        test('(ipv6)', () {
          final Object input =
              InternetAddress('2001:0db8:85a3:0000:0000:8a2e:0370:7334');
          final TypeSpec type = TypeSpec(DataType.inet);
          encoder.writeTypedValue('test', input, typeSpec: type, size: size);
          final Object? output =
              mock.createDecoder(encoder).readTypedValue(type, size: size);

          expect(output, equals(input));
        });
      });

      group('NUMBERS:', () {
        group('INT', () {
          test('(positive)', () {
            const Object input = 2147483647;
            final TypeSpec type = TypeSpec(DataType.int);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(output, equals(input));
          });

          test('(negative)', () {
            const Object input = -21474836;
            final TypeSpec type = TypeSpec(DataType.int);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(output, equals(input));
          });
        });

        group('SMALLINT', () {
          test('(positive)', () {
            final SmallInt input = SmallInt(32768);
            final TypeSpec type = TypeSpec(DataType.smallint);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(output, equals(input));
          });

          test('(negative)', () {
            final SmallInt input = SmallInt(-32768);
            final TypeSpec type = TypeSpec(DataType.smallint);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(output, equals(input));
          });
        });

        group('TINYINT', () {
          test('(positive)', () {
            final TinyInt input = TinyInt(128);
            final TypeSpec type = TypeSpec(DataType.tinyint);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(output, equals(input));
          });

          test('(negative)', () {
            final TinyInt input = TinyInt(-128);
            final TypeSpec type = TypeSpec(DataType.tinyint);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(output, equals(input));
          });
        });

        group('BIGINT', () {
          test('(positive)', () {
            const Object input = 9223372036854775807;
            final TypeSpec type = TypeSpec(DataType.bigint);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(output, equals(input));
          });

          test('(negative)', () {
            const Object input = -922036854775807;
            final TypeSpec type = TypeSpec(DataType.bigint);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(output, equals(input));
          });
        });

        group('FLOAT', () {
          test('(positive)', () {
            const Object input = 3.141516;
            final TypeSpec type = TypeSpec(DataType.float);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(output, closeTo(input as num, 0.000001));
          });

          test('(negative)', () {
            const Object input = -3.12345;
            final TypeSpec type = TypeSpec(DataType.float);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(output, closeTo(input as num, 0.000001));
          });
        });

        group('DOUBLE', () {
          test('(positive)', () {
            const Object input = 3.141516;
            final TypeSpec type = TypeSpec(DataType.double);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(output, closeTo(input as num, 0.000001));
          });

          test('(negative)', () {
            const Object input = -3.12345;
            final TypeSpec type = TypeSpec(DataType.double);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(output, closeTo(input as num, 0.000001));
          });
        });

        group('DECIMAL [fraction digits = $decimalFractionDigits]', () {
          test('(positive)', () {
            const Object input = 3.123451234512345;
            final TypeSpec type = TypeSpec(DataType.decimal);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(
              output,
              closeTo(input as num, pow(10, -decimalFractionDigits)),
            );
          });

          test('(negative)', () {
            const Object input = -3.123451234512345;
            final TypeSpec type = TypeSpec(DataType.decimal);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(
              output,
              closeTo(input as num, pow(10, -decimalFractionDigits)),
            );
          });
        });

        group('VARINT', () {
          test('(positive)', () {
            final input = BigInt.parse('12345678901234567890123');
            final TypeSpec type = TypeSpec(DataType.varint);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(output, equals(input));
          });

          test('(negative)', () {
            final input = BigInt.parse('-987677654324167384628746291873912873');
            final TypeSpec type = TypeSpec(DataType.varint);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(output, equals(input));
          });
        });
      });

      group('COLLECTIONS:', () {
        test('SET', () {
          final Object input = {-2, -1, 0, 1, 2};
          final TypeSpec type =
              TypeSpec(DataType.set, valueSubType: TypeSpec(DataType.int));
          encoder.writeTypedValue('test', input, typeSpec: type, size: size);
          final Object? output =
              mock.createDecoder(encoder).readTypedValue(type, size: size);

          expect(output, equals(input));
        });

        test('LIST', () {
          final Object input = [
            DateTime.fromMillisecondsSinceEpoch(1455746327000),
            DateTime.fromMillisecondsSinceEpoch(1455746327000)
          ];
          final TypeSpec type = TypeSpec(
            DataType.list,
            valueSubType: TypeSpec(DataType.timestamp),
          );
          encoder.writeTypedValue('test', input, typeSpec: type, size: size);
          final Object? output =
              mock.createDecoder(encoder).readTypedValue(type, size: size);

          expect(output, equals(input));
        });

        test('MAP', () {
          final Object input = {
            'foo': DateTime.fromMillisecondsSinceEpoch(1455746327000),
            'bar': DateTime.fromMillisecondsSinceEpoch(1455746327000)
          };
          final TypeSpec type = TypeSpec(
            DataType.map,
            keySubType: TypeSpec(DataType.text),
            valueSubType: TypeSpec(DataType.timestamp),
          );
          encoder.writeTypedValue('test', input, typeSpec: type, size: size);
          final Object? output =
              mock.createDecoder(encoder).readTypedValue(type, size: size);

          expect(output, equals(input));
        });
      });
    });

    group('(protocol V3):', () {
      setUp(() {
        encoder = TypeEncoder(ProtocolVersion.v3);
        size = SizeType.long;
      });
      test('UTF-8 STRING', () {
        const Object input = 'Test 123 AbC !@#ΤΕΣΤ';
        final TypeSpec type = TypeSpec(DataType.text);
        encoder.writeTypedValue('test', input, typeSpec: type, size: size);
        final Object? output =
            mock.createDecoder(encoder).readTypedValue(type, size: size);

        expect(output, equals(input));
      });

      test('ASCII STRING', () {
        const Object input = 'Test 123 AbC';
        final TypeSpec type = TypeSpec(DataType.ascii);
        encoder.writeTypedValue('test', input, typeSpec: type, size: size);
        final Object? output =
            mock.createDecoder(encoder).readTypedValue(type, size: size);

        expect(output, equals(input));
      });

      test('UUID', () {
        final Object input = Uuid.simple();
        final TypeSpec type = TypeSpec(DataType.uuid);
        encoder.writeTypedValue('test', input, typeSpec: type, size: size);
        final Object? output =
            mock.createDecoder(encoder).readTypedValue(type, size: size);

        expect(output, equals(input));
      });

      test('TIMEUUID', () {
        final Object input = Uuid.timeBased();
        final TypeSpec type = TypeSpec(DataType.timeuuid);
        encoder.writeTypedValue('test', input, typeSpec: type, size: size);
        final Object? output =
            mock.createDecoder(encoder).readTypedValue(type, size: size);

        expect(output, equals(input));
      });

      test('CUSTOM', () {
        final Object input = Uint8List.fromList(
          List<int>.generate(10, (int index) => index * 2),
        );
        final TypeSpec type = TypeSpec(DataType.custom);
        encoder.writeTypedValue('test', input, typeSpec: type, size: size);
        final Object? output =
            mock.createDecoder(encoder).readTypedValue(type, size: size);

        expect(output, equals(input));
      });

      test('BLOB', () {
        final Object input = Uint8List.fromList(
          List<int>.generate(10, (int index) => index * 2),
        );
        final TypeSpec type = TypeSpec(DataType.blob);
        encoder.writeTypedValue('test', input, typeSpec: type, size: size);
        final Object? output =
            mock.createDecoder(encoder).readTypedValue(type, size: size);

        expect(output, equals(input));
      });

      group('COUNTER', () {
        test('(positive)', () {
          const Object input = 9223372036854775807;
          final TypeSpec type = TypeSpec(DataType.counter);
          encoder.writeTypedValue('test', input, typeSpec: type, size: size);
          final Object? output =
              mock.createDecoder(encoder).readTypedValue(type, size: size);

          expect(output, equals(input));
        });

        test('(negative)', () {
          const Object input = -1;
          final TypeSpec type = TypeSpec(DataType.counter);
          encoder.writeTypedValue('test', input, typeSpec: type, size: size);
          final Object? output =
              mock.createDecoder(encoder).readTypedValue(type, size: size);

          expect(output, equals(input));
        });
      });

      test('TIMESTAMP', () {
        final Object input = DateTime.fromMillisecondsSinceEpoch(1455746327000);
        final TypeSpec type = TypeSpec(DataType.timestamp);
        encoder.writeTypedValue('test', input, typeSpec: type, size: size);
        final Object? output =
            mock.createDecoder(encoder).readTypedValue(type, size: size);

        expect(output, equals(input));
      });

      group('BOOLEAN', () {
        test('(true)', () {
          const Object input = true;
          final TypeSpec type = TypeSpec(DataType.boolean);
          encoder.writeTypedValue('test', input, typeSpec: type, size: size);
          final Object? output =
              mock.createDecoder(encoder).readTypedValue(type, size: size);

          expect(output, equals(input));
        });

        test('(false)', () {
          const Object input = false;
          final TypeSpec type = TypeSpec(DataType.boolean);
          encoder.writeTypedValue('test', input, typeSpec: type, size: size);
          final Object? output =
              mock.createDecoder(encoder).readTypedValue(type, size: size);

          expect(output, equals(input));
        });
      });

      group('BOOLEAN', () {
        test('(true)', () {
          const Object input = true;
          final TypeSpec type = TypeSpec(DataType.boolean);
          encoder.writeTypedValue('test', input, typeSpec: type, size: size);
          final Object? output =
              mock.createDecoder(encoder).readTypedValue(type, size: size);

          expect(output, equals(input));
        });

        test('(false)', () {
          const Object input = false;
          final TypeSpec type = TypeSpec(DataType.boolean);
          encoder.writeTypedValue('test', input, typeSpec: type, size: size);
          final Object? output =
              mock.createDecoder(encoder).readTypedValue(type, size: size);

          expect(output, equals(input));
        });
      });

      group('INET:', () {
        test('(ipv4)', () {
          final Object input = InternetAddress('192.168.169.101');
          final TypeSpec type = TypeSpec(DataType.inet);
          encoder.writeTypedValue('test', input, typeSpec: type, size: size);
          final Object? output =
              mock.createDecoder(encoder).readTypedValue(type, size: size);

          expect(output, equals(input));
        });

        test('(ipv6)', () {
          final Object input =
              InternetAddress('2001:0db8:85a3:0000:0000:8a2e:0370:7334');
          final TypeSpec type = TypeSpec(DataType.inet);
          encoder.writeTypedValue('test', input, typeSpec: type, size: size);
          final Object? output =
              mock.createDecoder(encoder).readTypedValue(type, size: size);

          expect(output, equals(input));
        });
      });

      group('NUMBERS:', () {
        group('INT', () {
          test('(positive)', () {
            const Object input = 2147483647;
            final TypeSpec type = TypeSpec(DataType.int);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(output, equals(input));
          });

          test('(negative)', () {
            const Object input = -21474836;
            final TypeSpec type = TypeSpec(DataType.int);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(output, equals(input));
          });
        });

        group('BIGINT', () {
          test('(positive)', () {
            const Object input = 9223372036854775807;
            final TypeSpec type = TypeSpec(DataType.bigint);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(output, equals(input));
          });

          test('(negative)', () {
            const Object input = -922036854775807;
            final TypeSpec type = TypeSpec(DataType.bigint);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(output, equals(input));
          });
        });

        group('FLOAT', () {
          test('(positive)', () {
            const Object input = 3.141516;
            final TypeSpec type = TypeSpec(DataType.float);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(output, closeTo(input as num, 0.000001));
          });

          test('(negative)', () {
            const Object input = -3.12345;
            final TypeSpec type = TypeSpec(DataType.float);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(output, closeTo(input as num, 0.000001));
          });
        });

        group('DOUBLE', () {
          test('(positive)', () {
            const Object input = 3.141516;
            final TypeSpec type = TypeSpec(DataType.double);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(output, closeTo(input as num, 0.000001));
          });

          test('(negative)', () {
            const Object input = -3.12345;
            final TypeSpec type = TypeSpec(DataType.double);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(output, closeTo(input as num, 0.000001));
          });
        });

        group('DECIMAL [fraction digits = $decimalFractionDigits]', () {
          test('(positive)', () {
            const Object input = 3.123451234512345;
            final TypeSpec type = TypeSpec(DataType.decimal);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(
              output,
              closeTo(input as num, pow(10, -decimalFractionDigits)),
            );
          });

          test('(negative)', () {
            const Object input = -3.123451234512345;
            final TypeSpec type = TypeSpec(DataType.decimal);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(
              output,
              closeTo(input as num, pow(10, -decimalFractionDigits)),
            );
          });
        });

        group('VARINT', () {
          test('(positive)', () {
            final Object input = BigInt.parse('12345678901234567890123');
            final TypeSpec type = TypeSpec(DataType.varint);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(output, equals(input));
          });

          test('(negative)', () {
            final Object input =
                BigInt.parse('-987677654324167384628746291873912873');
            final TypeSpec type = TypeSpec(DataType.varint);
            encoder.writeTypedValue('test', input, typeSpec: type, size: size);
            final Object? output =
                mock.createDecoder(encoder).readTypedValue(type, size: size);

            expect(output, equals(input));
          });
        });
      });

      group('COLLECTIONS:', () {
        test('SET', () {
          final Object input = {-2, -1, 0, 1, 2};
          final TypeSpec type =
              TypeSpec(DataType.set, valueSubType: TypeSpec(DataType.int));
          encoder.writeTypedValue('test', input, typeSpec: type, size: size);
          final Object? output =
              mock.createDecoder(encoder).readTypedValue(type, size: size);

          expect(output, equals(input));
        });

        test('LIST', () {
          final Object input = [
            DateTime.fromMillisecondsSinceEpoch(1455746327000),
            DateTime.fromMillisecondsSinceEpoch(1455746327000)
          ];
          final TypeSpec type = TypeSpec(
            DataType.list,
            valueSubType: TypeSpec(DataType.timestamp),
          );
          encoder.writeTypedValue('test', input, typeSpec: type, size: size);
          final Object? output =
              mock.createDecoder(encoder).readTypedValue(type, size: size);

          expect(output, equals(input));
        });

        test('MAP', () {
          final Object input = {
            'foo': DateTime.fromMillisecondsSinceEpoch(1455746327000),
            'bar': DateTime.fromMillisecondsSinceEpoch(1455746327000)
          };
          final TypeSpec type = TypeSpec(
            DataType.map,
            keySubType: TypeSpec(DataType.text),
            valueSubType: TypeSpec(DataType.timestamp),
          );
          encoder.writeTypedValue('test', input, typeSpec: type, size: size);
          final Object? output =
              mock.createDecoder(encoder).readTypedValue(type, size: size);

          expect(output, equals(input));
        });
      });

      test('UDT (nested)', () {
        final Object input = {
          'address': 'Elm street',
          'phones': [
            {'prefix': 30, 'phone': '123456789'},
            {'prefix': 1, 'phone': '800180023'}
          ],
          'tags': {
            'home': {
              'when': DateTime.fromMillisecondsSinceEpoch(1455746327000),
              'labels': ['red', 'green', 'blue']
            }
          }
        };
        final TypeSpec intType = TypeSpec(DataType.int);
        final TypeSpec dateType = TypeSpec(DataType.timestamp);
        final TypeSpec stringType = TypeSpec(DataType.text);
        final TypeSpec phoneType = TypeSpec(DataType.udt)
          ..udtFields['prefix'] = intType
          ..udtFields['phone'] = stringType;
        final TypeSpec tagType = TypeSpec(DataType.udt)
          ..udtFields['when'] = dateType
          ..udtFields['labels'] =
              TypeSpec(DataType.list, valueSubType: stringType);

        final TypeSpec type = TypeSpec(DataType.udt)
          ..udtFields['address'] = stringType
          ..udtFields['phones'] =
              TypeSpec(DataType.list, valueSubType: phoneType)
          ..udtFields['tags'] = TypeSpec(
            DataType.map,
            keySubType: stringType,
            valueSubType: tagType,
          );

        encoder.writeTypedValue('test', input, typeSpec: type, size: size);
        final Object? output =
            mock.createDecoder(encoder).readTypedValue(type, size: size);

        expect(output, equals(input));
      });

      test('TUPLE', () {
        final Object input = Tuple.fromIterable(
          ['Test', 3.14, DateTime.fromMillisecondsSinceEpoch(1455746327000)],
        );
        final TypeSpec type = TypeSpec(DataType.tuple)
          ..tupleFields.add(TypeSpec(DataType.text))
          ..tupleFields.add(TypeSpec(DataType.double))
          ..tupleFields.add(TypeSpec(DataType.timestamp));

        encoder.writeTypedValue('test', input, typeSpec: type, size: size);
        final Object? output =
            mock.createDecoder(encoder).readTypedValue(type, size: size);

        expect(output, equals(input));
      });
    });
  });
}
