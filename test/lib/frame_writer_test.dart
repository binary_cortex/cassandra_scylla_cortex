library cassandra_scylla_cortex.tests.frame_writer;

import 'package:cassandra_scylla_cortex/src/exceptions.dart';
import 'package:cassandra_scylla_cortex/src/protocol.dart';
import 'package:cassandra_scylla_cortex/src/stream.dart';
import 'package:cassandra_scylla_cortex/src/types.dart';
import 'package:test/test.dart';
import 'mocks/mocks.dart' as mock;

void main({bool enableLogger = false}) {
  if (enableLogger) {
    mock.initLogger();
  }

  late FrameWriter frameWriter;
  final mock.MockChunkedOutputWriter mockOutputWriter =
      mock.MockChunkedOutputWriter();

  group('Frame writer:', () {
    setUp(() {
      frameWriter = FrameWriter(
        0,
        ProtocolVersion.v2,
        withEncoder:
            TypeEncoder(ProtocolVersion.v2, withWriter: mockOutputWriter),
      );
    });

    test('InvalidFrame exception (illegal length)', () {
      Exception? error;
      try {
        mockOutputWriter.forcedLengthInBytes = FrameHeader.maxLengthInBytes + 1;
        frameWriter.writeMessage(StartupMessage(), null);
      } on Exception catch (e) {
        error = e;
      }
      expect(error, isA<DriverException>());
      expect(
        (error as DriverException?)?.message,
        equals(
          'Frame size cannot be larger than ${FrameHeader.maxLengthInBytes} bytes. Attempted to write ${FrameHeader.maxLengthInBytes + 1} bytes',
        ),
      );
    });
  });
}
