## 1.0.7 (April 6, 2022)
- Fixing race conditions
- Adding support for unprepared query error message responses from the server

## 1.0.6 (March 7, 2022)
- Reconnecting after a connection loss
- Revised connection pool

## 1.0.5 (March 6, 2022)
- Fixing encoding of enums in register message

## 1.0.4 (March 6, 2022)
- Small refactor in simple connection pool
- Exposing loggers so client apps can set logging if desired

## 1.0.3 (Feburary 24, 2022)
- Async refactor. Getting rid of .this() in preference to async/await

## 1.0.2 (Feburary 19, 2022)
- Migrating enums to the new enum architecture

## 1.0.1 (Feburary 18, 2022)
- Cleaning all the lint warnings from the new lint rules

## 1.0.0 (Feburary 18, 2022)
- Official intial stable in new repo

## 1.0.0-rc8 (Feburary 18, 2022)
- Moving repository
- Adjusting files/links based on repository move
- Adding lint rules
- Some lint cleanup with new lint rules

## 1.0.0-rc7 (Feburary 17, 2022)
- Adding smallint/tinyint support

## 1.0.0-rc5 (June 19, 2021)
- Connection pool : no deadlock

## 1.0.0-rc4 (June 13, 2021)
- Async cleanup

## 1.0.0-rc3 (May 25, 2021)
- Non-nullable column names

## 1.0.0-rc2 (May 22, 2021)
- Updating the testing and coverage scripts for modern usage patterns.
- Updating package dependencies to require higher versions based on packages support for dart 2.12 null safety.

## 1.0.0-rc1 (May 20, 2021)
- First candidate supporting dart 2.12 and null safety. This is a large refactor affecting
the entire library and must undergo extensive testing.

## 0.3.1 (May 17, 2021)
Fixing pubspec package name changed in 0.3.0. 0.3.0 is broken, use 0.3.1 instead.

## 0.3.0 (May 17, 2021)
Upgrading to use uuid version ^3.0.0

## 0.1.5 (Feb 18, 2016)
Fixed another socket flush race condition ([#2](https://github.com/achilleasa/cassandra_scylla_cortex/pull/2))

## 0.1.4 (April 17, 2015)
Added the **preferBiggerTcpPackets** option (defaults to false). When enabled, the driver will
join together protocol frame chunks before piping them to the underlying TCP socket. This option
will improve performance at the expense of slightly higher memory consumption.

## 0.1.3 (December 20, 2014)
Improved support for compression codecs
Driver is now compatible with [dart_lz4](https://github.com/achilleasa/dart_lz4)

## 0.1.2 (December 6, 2014)
Renamed lib/driver to lib/src so that docgen works
Fixed race condition while flushing data to sockets ([#1](https://github.com/achilleasa/cassandra_scylla_cortex/issues/1))

## 0.1.1 (November 26, 2014)
Restructured folders to align with pub requirements

## 0.1.0 (November 24, 2014)
Initial release
