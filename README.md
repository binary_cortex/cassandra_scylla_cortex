# Dart Driver for Apache Cassandra and ScyllaDB

Dart driver for [Apache Cassandra](https://cassandra.apache.org) that supports Cassandra Query Language version [3.0+](https://docs.datastax.com/en/landing_page/doc/landing_page/current.html) (CQL3).
[ScyllaDB](https://www.scylladb.com)

The driver has a small dependency tree and implements Cassandra binary protocol (versions 2 and 3) for communicating with Cassandra servers. The protocol and CQL versions to be used are both configurable by the user.

# Installation
Until the package gets published to [pub.dev](https://pub.dev),
add the following dependency to install this plugin:
```yaml
dependency:
  cassandra_scylla_cortex:
    git: https://gitlab.com/binarycortex/cassandra_scylla_cortex
```

# Features
 - Asynchronous API based on [Future](https://api.dartlang.org/apidocs/channels/stable/dartdoc-viewer/dart:async.Future) and [Streams](https://api.dartlang.org/apidocs/channels/stable/dartdoc-viewer/dart-async.Stream)
 - Connection management via connection pools
 - Connection load-balancing and failover
 - Server event handling (node/topology/schema change events)
 - Query multiplexing on each connection
 - Batch and prepared queries with either positional or named placeholders
 - Query result streaming
 - Support for all Cassandra types including [user defined types](http://www.datastax.com/dev/blog/cql-in-2-1) (UDT),  [tuples](http://www.datastax.com/documentation/developer/java-driver/2.1/java-driver/reference/tupleTypes.html) and custom types (via user-defined Codecs)
 - Dart null safety (2.12+)

# Quick start

```dart
import "dart:async";
import 'package:cassandra_scylla_cortex/cql.dart' as cql;

void main() async {
  // Create a client for connecting to our cluster using native
  // protocol V3 and sensible defaults. The client will setup
  // a connection pool for you and connect automatically when
  // you execute a query.
  cql.Client client = cql.Client.fromHostList([
      "10.0.0.1:9042",
      "10.0.0.2:9042",
  ]);

  // Perform a select with positional bindings
  final Iterable<Map<String, Object?>> rows = await client.query(
      cql.Query("SELECT * from test.type_test WHERE id=?", bindings : [123])
  );

  // Perform an prepared insert with named bindings, a time-based uuid and tuneable consistency
  final cql.ResultMessage res = await client.execute(
      cql.Query("INSERT INTO test.type_test (id, uuid_value) VALUES (:id, :uuid)", bindings : {
          "id" : 1
          , "uuid" : cql.Uuid.timeBased()
      }, consistency : cql.Consistency.LOCAL_QUORUM
       , prepared : true)
  );

  // Perform a batch insert query, (future chaining syntax)
  client.execute(
      cql.BatchQuery()
        ..add(
          cql.Query("INSERT INTO test.type_test (id, uuid_value) VALUES (:id, :uuid)", bindings : {
              "id" : 1
              , "uuid" : cql.Uuid.timeBased()
          })
      )
        ..add(
          cql.Query("INSERT INTO test.type_test (id, uuid_value) VALUES (:id, :uuid)", bindings : {
              "id" : 2
              , "uuid" : cql.Uuid.timeBased()
          })
      )
        ..consistency = cql.Consistency.TWO
  ).then((cql.ResultMessage res) {
    // ...
  }).catchError((e) {
    // Handle errors
  });

  // Stream (paginated) query
  StreamSubscription sub;
  sub = client.stream(
      cql.Query("SELECT * from test.type_test"),
      pageSize : 200,
  ).listen((Map<String, Object?> row) {
    // Handle incoming row
    print("Next row: ${row}");
    // ... or manipulate stream
    sub.cancel();
  });
}
```

# Api

See the [Api documentation](https://gitlab.com/binary_cortex/cassandra_scylla_cortex/-/blob/master/API.md).


# Contributing

See the [Contributing Guide](https://gitlab.com/binary_cortex/cassandra_scylla_cortex/-/blob/master/CONTRIBUTING.md).

# Acknowledgements

- Original branch forked from [Vimtekken's cassandra_scylla_cortex](https://github.com/Vimtekken/cassandra_scylla_cortex)

From previous authors:
- The design and implementation of this driver borrows lots of ideas from [node-cassandra-cql](https://github.com/jorgebay/node-cassandra-cql/). 
- The varint and decimal type decoders have been ported from the [DataStax cpp driver](https://github.com/datastax/cpp-driver).

# License

cassandra\_scylla\_cortex is distributed under the [MIT license](https://gitlab.com/binary_cortex/cassandra_scylla_cortex/-/blob/master/LICENSE).
