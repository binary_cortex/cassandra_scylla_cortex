# Contributing Guide

cassandra\_scylla\_cortex is of course Open Source! Feel free to contribute!

## Getting Started

- Make sure you have a [GitLab Account](https://gitlab.com).
- Make sure the [Dart SDK](https://www.dartlang.org/tools/sdk/) is installed on your system.
- Make sure you have [Git](http://git-scm.com/) installed on your system.
- Fork the repository.

## Making Changes

 - Create a branch for your changes.
 - Commit your code for each logical change (see [tips for creating better commit messages](http://robots.thoughtbot.com/5-useful-tips-for-a-better-commit-message)).
 - Push your change to your fork.
 - Create a Merge Request on GitLab for your change.

## Code Style

When submitting pull requests try to follow the [Dart Style Guide](https://www.dartlang.org/articles/style-guide/). Additionally there is a linter that loads rules from the [analysis_options](https://gitlab.com/binary_cortex/cassandra_scylla_cortex/-/BLOB/master/analysis_options.yaml) file that
can guide you on the style/syntax rules.

## Unit tests

Please make sure you submit a unit-test together with your pull request. All unit tests should be placed inside the ```test/lib``` folder. You can add you own unit-tests to the existing test suites or create a new one. If you need to create a new test suite for your PR then make sure you also update the main test runner which is located at ```tests/run_all.dart```.

The project ships with a the ```test/test_coverage.sh``` script for generating html code coverage reports using [dart-lang/coverage](https://github.com/dart-lang/coverage) and LCOV.
