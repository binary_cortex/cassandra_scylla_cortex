part of cassandra_scylla_cortex.query;

class BatchQuery extends QueryInterface {
  List<Query> queryList = [];

  Consistency consistency;
  Consistency? serialConsistency;
  BatchType type;

  BatchQuery({
    this.consistency = Consistency.quorum,
    this.serialConsistency,
    this.type = BatchType.logged,
  });

  // Add a new [Query] to the batch
  void add(Query query) {
    queryList.add(query);
  }
}
