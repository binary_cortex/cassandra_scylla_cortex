library cassandra_scylla_cortex.exception;

import 'package:cassandra_scylla_cortex/src/protocol.dart';

part 'exception/authentication_exception.dart';
part 'exception/cassandra_exception.dart';
part 'exception/connection_failed_exception.dart';
part 'exception/connection_lost_exception.dart';
part 'exception/driver_exception_exception.dart';
part 'exception/error_message_exceptions.dart';
part 'exception/no_healthy_connections_exception.dart';
part 'exception/stream_reservation_exception.dart';
