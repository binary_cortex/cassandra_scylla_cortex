part of cassandra_scylla_cortex.connection;

class _ConnectionPoolWaitlist {
  final List<Completer<Connection>> _waitingForConnection = [];
  final Map<String, List<Completer<Connection>>> _waitingForConnectionForHost =
      {};
  Duration timeout = const Duration(seconds: 10);

  Future<Connection> addWaiting() {
    final Completer<Connection> completer = Completer<Connection>();
    _waitingForConnection.add(completer);
    return completer.future.timeout(
      timeout,
      onTimeout: () {
        _waitingForConnection.remove(completer);
        throw NoHealthyConnectionsException('No Healthy Connections');
      },
    );
  }

  Future<Connection> addWaitingForHost(String hostKey) {
    final Completer<Connection> completer = Completer<Connection>();
    _waitingForConnectionForHost.putIfAbsent(hostKey, () => []).add(completer);
    return completer.future.timeout(
      timeout,
      onTimeout: () {
        _waitingForConnectionForHost[hostKey]?.remove(completer);
        throw NoHealthyConnectionsException('No Healthy Connections');
      },
    );
  }

  void completeWaiting(Connection connection) {
    for (final completer in _waitingForConnection) {
      completer.complete(connection);
    }
    _waitingForConnection.clear();
    if (_waitingForConnectionForHost.containsKey(connection.hostKey)) {
      for (final completer
          in _waitingForConnectionForHost[connection.hostKey]!) {
        completer.complete(connection);
      }
      _waitingForConnectionForHost[connection.hostKey]!.clear();
    }
  }

  void errorWaiting(Exception err) {
    for (final completer in _waitingForConnection) {
      completer.completeError(err);
    }
    _waitingForConnection.clear();
  }

  void errorWaitingForHost(Exception err, String hostKey) {
    if (_waitingForConnectionForHost.containsKey(hostKey)) {
      for (final completer in _waitingForConnectionForHost[hostKey]!) {
        completer.completeError(err);
      }
      _waitingForConnectionForHost[hostKey]!.clear();
    }
  }

  void errorWaitingAll(Exception err) {
    errorWaiting(err);
    for (final hostList in _waitingForConnectionForHost.keys) {
      errorWaitingForHost(err, hostList);
    }
  }
}

class RevisedConnectionPool extends ConnectionPool {
  final Map<String, List<Connection>> _activeOnHost =
      <String, List<Connection>>{};
  final List<Connection> _activeConnections = <Connection>[];
  final List<Connection> _connections = <Connection>[];
  final Map<String, List<Connection>> _connectionsForHost =
      <String, List<Connection>>{};
  final String? defaultKeyspace;
  final _ConnectionPoolWaitlist _waitlist = _ConnectionPoolWaitlist();
  final Random _random = Random();
  Completer<void>? _connectCompleter;

  /// Server event listeners
  Connection? _eventSubscriber;
  StreamSubscription? _eventSubscription;

  RevisedConnectionPool.fromHostList(
    List<String> hosts,
    PoolConfiguration poolConfiguration, {
    this.defaultKeyspace,
  }) {
    poolLogger.fine('Creating pool to $hosts');
    poolConfig = poolConfiguration;
    _waitlist.timeout = poolConfiguration.connectionTimeout;

    for (final hostString in hosts) {
      _createPoolForHostFromString(hostString);
    }

    if (poolConfiguration.autoDiscoverNodes) {
      listenForServerEvents([
        EventRegistrationType.statusChange,
        EventRegistrationType.topologyChange
      ]).listen(_handleEventMessage);
    }
    poolLogger.info(
      'Created ${_connections.length} connections to ${hosts.length} hosts',
    );
  }

  static String hostKey(String? host, int? port) {
    return '$host:$port';
  }

  void _connectionActive(Connection connection) {
    poolLogger.fine('Connection to ${connection.connId} is becoming active');
    _activeConnections.add(connection);
    final hostConnections =
        _activeOnHost.putIfAbsent(connection.hostKey, () => <Connection>[]);
    hostConnections.add(connection);

    _waitlist.completeWaiting(connection);

    // Notify connect wait future if needed.
    if (_connectCompleter != null && !_connectCompleter!.isCompleted) {
      _connectCompleter!.complete();
    }

    // If we need an event subscriber, just take this one
    if (_eventSubscriber == null) {
      _registerEventListener(connection);
    }
  }

  void _connectionInactive(Connection connection) {
    poolLogger.fine('Connection to ${connection.connId} is no longer active');
    _activeConnections.remove(connection);
    final hostConnections = _activeOnHost[connection.hostKey];
    hostConnections?.remove(connection);
  }

  @override
  Future<Connection> getConnection() async {
    await connect();
    if (_activeConnections.isNotEmpty) {
      return _activeConnections[_random.nextInt(_activeConnections.length)];
    } else {
      return _waitlist.addWaiting();
    }
  }

  @override
  Future<Connection> getConnectionToHost(String? host, int? port) async {
    await connect();
    final onHost = _activeOnHost['$host:$port'];
    if (onHost?.isNotEmpty ?? false) {
      return onHost![_random.nextInt(onHost.length)];
    } else {
      return _waitlist
          .addWaitingForHost(RevisedConnectionPool.hostKey(host, port));
    }
  }

  @override
  Future<void> connect() async {
    void timeout() {
      throw NoHealthyConnectionsException('No Healthy connection');
    }

    if (_connectCompleter == null) {
      _connectCompleter = Completer<void>();

      for (final conn in _connections) {
        // Catch error because we want to not have uncaught exceptions
        // And this is being detacked from the current calling context
        // As we don't want to wait for these open calls to complete before
        // continuing. If it errors on open, nothing we do here. The call
        // to conn.onDisconnect will handle that.
        conn.open().catchError((_) {});
      }
    }

    return _connectCompleter?.future
        .timeout(poolConfig!.connectionTimeout, onTimeout: timeout);
  }

  @override
  Future<void> disconnect({
    bool drain = true,
    Duration drainTimeout = const Duration(seconds: 5),
  }) async {
    _connectCompleter = null;
    _eventSubscription?.cancel();
    _eventSubscription = null;
    _eventSubscriber = null;

    // @todo update exception
    final disconnectException = Exception('Pool disconnected');
    _waitlist.errorWaitingAll(disconnectException);

    // Close connections in parallel
    await Future.wait(
      _connections
          .map((conn) => conn.close(drain: drain, drainTimeout: drainTimeout)),
    );
  }

  void _createPoolForHostFromString(String hostString) {
    final List<String> hostParts = hostString.split(':');
    final int port = hostParts.length > 1
        ? int.parse(hostParts[1])
        : ConnectionPool.defaultPort;

    _createPoolForHost(hostParts[0], port);
  }

  void _createPoolForHost(String host, int? port) {
    for (final poolIndex in List<int>.generate(
      poolConfig!.connectionsPerHost,
      (int input) => input++,
    )) {
      final Connection conn = Connection(
        '$host:$port-$poolIndex',
        host,
        port,
        config: poolConfig,
        defaultKeyspace: defaultKeyspace,
      );

      conn.onConnected.stream.listen(_connectionActive);
      conn.onDisconnected.stream.listen(_connectionInactive);
      conn.onAuthenticate.stream.listen((error) {
        poolLogger.info('Connection failed to authenticate');
        _connectCompleter?.completeError(error);
      });

      _connections.add(conn);
      _connectionsForHost.putIfAbsent(conn.hostKey, () => []).add(conn);
    }
  }

  void _nodeUpped(String host, int port) {
    final hostKey = RevisedConnectionPool.hostKey(host, port);

    if (!_connectionsForHost.containsKey(hostKey)) {
      if (!poolConfig!.autoDiscoverNodes) {
        // Do not recognized new nodes when auto discover is off
        return;
      }
      _createPoolForHost(host, port);
    }

    if (_connectionsForHost.containsKey(hostKey)) {
      for (final conn in _connectionsForHost[hostKey]!) {
        conn.open();
      }
    }
  }

  void _nodeDowned(String host, int port, EventType? subType) {
    final hostKey = RevisedConnectionPool.hostKey(host, port);
    final hostConnections = _connectionsForHost[hostKey];

    if (hostConnections == null) {
      // No connections, no change needed.
      return;
    } else if (subType == EventType.nodeRemoved) {
      poolLogger.info(
        'Node [$hostKey] removed from cluster. Purging any existing open connections from pool',
      );
      _connectionsForHost.remove(hostKey);
      final exception = NoHealthyConnectionsException('No healthy connection');
      _waitlist.errorWaitingForHost(exception, hostKey);
    } else {
      poolLogger.info(
        'Node [$hostKey] went offline. Closing any existing open connections',
      );
      final exception = NoHealthyConnectionsException('No healthy connection');
      _waitlist.errorWaitingForHost(exception, hostKey);
    }

    for (final conn in hostConnections) {
      conn.close(drain: false);
    }

    if (hostConnections.contains(_eventSubscriber)) {
      _eventSubscription?.cancel();
      _eventSubscription = null;
      _eventSubscriber = null;
      getConnection().then(_registerEventListener).catchError((_) {});
    }
  }

  void _handleEventMessage(EventMessage message) {
    poolLogger.fine(
      'Received eventMessage ${message.type?.name}:${message.subType?.name} ${message.type == EventRegistrationType.schemaChange ? ('${message.keyspace}${message.changedTable!.isNotEmpty ? '.${message.changedTable}' : ''}') : '${message.address}:${message.port}'}',
    );

    switch (message.subType) {
      case EventType.nodeAdded:
      case EventType.nodeUp:
        _nodeUpped(message.address!.host, message.port!);
        break;
      case EventType.nodeRemoved:
      case EventType.nodeDown:
        _nodeDowned(message.address!.host, message.port!, message.subType);
        break;
      case EventType.schemaCreated:
      case EventType.schemaDropped:
      case EventType.schemaUpdated:
      case null:
        // No-op on thse events
        break;
    }
  }

  // basically forwards connection events out to a listener if they are looking
  // for these events.
  void _registerEventListener(Connection conn) {
    // List for server events and add them to the stream controller
    _eventSubscriber = conn;
    _eventSubscription = conn.listenForEvents([
      EventRegistrationType.statusChange,
      EventRegistrationType.topologyChange,
      EventRegistrationType.schemaChange
    ]).listen(_eventStreamController.add);

    poolLogger.info('Listening for server events on [${conn.connId}]');
  }
}
