part of cassandra_scylla_cortex.connection;

class SimpleConnectionPool extends ConnectionPool {
  /// The list of all pool connections
  final List<Connection> _pool = [];

  /// The list of maintained connections per host:port combination
  final Map<String, Set<Connection>> _poolPerHost =
      HashMap<String, Set<Connection>>();

  /// Pending list of reconnect attempts
  final Map<String, Future> _pendingReconnects = HashMap<String, Future>();

  /// Server event listeners
  Connection? _eventSubscriber;
  // @todo Handle closing _eventSubscription
  StreamSubscription? _eventSubscription;

  String? defaultKeyspace;
  Completer<void>? _poolConnected;
  bool _closing = false;

  /// Create the connection pool by spawning [_config.poolSize] connections
  /// to the input list of [hosts]
  SimpleConnectionPool.fromHostList(
    List<String> hosts,
    PoolConfiguration poolConfig, {
    this.defaultKeyspace,
  }) {
    if (hosts.isEmpty) {
      throw ArgumentError('Host list cannot be empty');
    }

    this.poolConfig = poolConfig;

    for (final hostString in hosts) {
      _createPoolForHostFromString(hostString);
    }

    // If node auto-discovery is enabled listen for those server events
    if (poolConfig.autoDiscoverNodes) {
      listenForServerEvents([
        EventRegistrationType.statusChange,
        EventRegistrationType.topologyChange
      ]).listen(_handleEventMessage);
    }
    poolLogger
        .info('Created ${_pool.length} connections to ${hosts.length} hosts');
  }

  /// Establish connections to the pool nodes and return a [Future] to be successfully completed when
  /// at least one connection is successfully established. The returned [Future] will fail if an
  /// [AuthenticationException] occurs or if all connection attempts fail.
  @override
  Future<void> connect() async {
    _closing = false;
    // Already connected/connecting
    if (_poolConnected != null) {
      return _poolConnected!.future;
    }

    // Setup a future to be completed when our connections are set up
    _poolConnected = Completer();
    poolLogger.info('Initializing pool connections');

    int activeConnections = 0;
    int remainingConnections = _pool.length;

    Future<void> openConnection(Connection conn) async {
      try {
        await conn.open();

        // Select the first open connection as our stream listener
        if (_eventSubscriber == null) {
          _registerEventListener(conn);
        }

        ++activeConnections;
        _poolConnected!.complete();
      } catch (err) {
        poolLogger.severe(err);

        --remainingConnections;

        // If we get an authentication error, report it directly to the client
        if (err is AuthenticationException) {
          _poolConnected!.completeError(err);
        }

        // No more connections remaining to be opened
        if (remainingConnections == 0 && !_poolConnected!.isCompleted) {
          // All connections failed
          if (activeConnections == 0) {
            _poolConnected!.completeError(
              NoHealthyConnectionsException(
                'Could not connect to any of the supplied hosts',
              ),
            );
            // Setting this to null allows a new connection processes to start. After a delay.
            Future.delayed(
              const Duration(seconds: 1),
              () => _poolConnected = null,
            );
          } else {
            // At least one connection has been established
            _poolConnected!.complete();
          }
        }
      }
    }

    for (final Connection conn in _pool) {
      openConnection(conn);
    }

    void timeout() {
      throw NoHealthyConnectionsException('No Healthy connection');
    }

    return _poolConnected!.future.timeout(
      poolConfig?.connectionTimeout ?? const Duration(seconds: 10),
      onTimeout: timeout,
    );
  }

  /// Disconnect all pool connections. If the [drain] flag is set to true, all pool connections
  /// will be drained prior to being disconnected and a [Future] will be returned that will complete
  /// when all connections are drained or when the [drainTimeout] expires. If [drain] is false then
  /// the returned [Future] will already be completed.
  @override
  Future<void> disconnect({
    bool drain = true,
    Duration drainTimeout = const Duration(seconds: 5),
  }) async {
    _closing = true;
    await Future.wait(
      _pool.map(
        (Connection conn) =>
            conn.close(drain: drain, drainTimeout: drainTimeout),
      ),
    );
  }

  /// Get back an active [Connection] from the pool.
  @override
  Future<Connection> getConnection() {
    return _findOneMatchingFilter(
      (Connection conn) => conn.healthy && conn.inService,
    );
  }

  /// Get back an active [Connection] from the pool to the specified [host] and [port].
  @override
  Future<Connection> getConnectionToHost(String? host, int? port) {
    return _findOneMatchingFilter(
      (Connection conn) =>
          conn.healthy &&
          conn.inService &&
          conn.host == host &&
          conn.port == port,
    );
  }

  Future<Connection> _findOneMatchingFilter(
    bool Function(Connection) filter,
  ) async {
    await connect();

    // Prefer healthry connections matching the filter predicate that have available
    // query multiplexing slots so we do not need to wait
    Connection? healthyConnection = _pool.firstWhereOrNull(
      (Connection conn) => conn.hasAvailableStreams && filter(conn),
    );

    // Otherwise try to fetch the first healthy connection from the pool matching the filter predicate
    healthyConnection ??= healthyConnection = _pool.firstWhereOrNull(filter);

    if (healthyConnection == null) {
      throw NoHealthyConnectionsException('No healthy connections available');
    } else {
      // Remove the connection and append it to the end of the list
      // so we can round-robin our connections
      _pool
        ..remove(healthyConnection)
        ..add(healthyConnection);
    }
    return healthyConnection;
  }

  // basically forwards connection events out to a listener if they are looking
  // for these events.
  void _registerEventListener(Connection conn) {
    // List for server events and add them to the stream controller
    _eventSubscriber = conn;
    _eventSubscription = conn.listenForEvents([
      EventRegistrationType.statusChange,
      EventRegistrationType.topologyChange,
      EventRegistrationType.schemaChange
    ]).listen(_eventStreamController.add);

    poolLogger.info('Listening for server events on [${conn.connId}]');
  }

  String getHostKey(String host, int? port) {
    return '$host:$port';
  }

  void _addPending(String host, int port) {
    final String hostKey = getHostKey(host, port);
    // If we are already processing an UP event for this host, ignore
    if (_pendingReconnects.containsKey(hostKey)) {
      return;
    }

    // If this is a new node setup a host pool for it
    if (!_poolPerHost.containsKey(hostKey)) {
      // If auto-discover is off, ignore this event
      if (!poolConfig!.autoDiscoverNodes) {
        return;
      }

      poolLogger.info('Discovered new node [$hostKey]. Adding to pool');
      _createPoolForHost(host, port);
    } else {
      poolLogger.info('Node [$hostKey] went online. Attempting to reconnect');
    }

    // According to the protocol spec, it might take some time for the node
    // to begin accepting connections so we need to defer our connection attempts.
    _pendingReconnects[hostKey] =
        Future.delayed(poolConfig!.reconnectWaitTime, () {
      for (final conn in _poolPerHost[hostKey]!) {
        conn.open();
      }
      _pendingReconnects.remove(hostKey);
    });
  }

  void _removePending(String host, int port, EventType? subType) {
    final String hostKey = getHostKey(host, port);
    final Set<Connection>? hostConnections = _poolPerHost[hostKey];

    // If this is an unknown node, we dont need to do anything
    if (hostConnections == null) {
      return;
    } else if (subType == EventType.nodeRemoved) {
      poolLogger.info(
        'Node [$hostKey] removed from cluster. Purging any existing open connections from pool',
      );
      _poolPerHost.remove(hostKey);
      _pendingReconnects.remove(hostKey);
    } else {
      poolLogger.info(
        'Node [$hostKey] went offline. Closing any existing open connections',
      );
      _pendingReconnects.remove(hostKey);
    }

    // Force-close any existing connections
    for (final conn in hostConnections) {
      conn.close(drain: false);
    }

    // If we lost the connection registered for server events, pick a new one
    if (!_closing && hostConnections.contains(_eventSubscriber)) {
      _eventSubscription!.cancel();
      _eventSubscription = null;
      _eventSubscriber = null;
      () async {
        try {
          final conn = await getConnection();
          _registerEventListener(conn);
        } catch (e) {
          // Eat error
        }
      }();
    }
  }

  void _handleEventMessage(EventMessage message) {
    poolLogger.fine(
      'Received eventMessage ${message.type?.name}:${message.subType?.name} ${message.type == EventRegistrationType.schemaChange ? ('${message.keyspace}${message.changedTable!.isNotEmpty ? '.${message.changedTable}' : ''}') : '${message.address}:${message.port}'}',
    );

    switch (message.subType) {
      case EventType.nodeAdded:
      case EventType.nodeUp:
        _addPending(message.address!.host, message.port!);
        break;
      case EventType.nodeRemoved:
      case EventType.nodeDown:
        _removePending(message.address!.host, message.port!, message.subType);
        break;
      case EventType.schemaCreated:
      case EventType.schemaDropped:
      case EventType.schemaUpdated:
      case null:
        // No-op on thse events
        break;
    }
  }

  void _createPoolForHostFromString(String hostString) {
    final List<String> hostParts = hostString.split(':');
    final int port = hostParts.length > 1
        ? int.parse(hostParts[1])
        : ConnectionPool.defaultPort;

    _createPoolForHost(hostParts[0], port);
  }

  void _createPoolForHost(String host, int? port) {
    final String hostKey = getHostKey(host, port);
    _poolPerHost[hostKey] = HashSet<Connection>();

    // Allocate poolConfig.connectionsPerHost connections
    for (int poolIndex = 0;
        poolIndex < poolConfig!.connectionsPerHost;
        poolIndex++) {
      final Connection conn = Connection(
        '$hostKey-$poolIndex',
        host,
        port,
        config: poolConfig,
        defaultKeyspace: defaultKeyspace,
      );

      _poolPerHost[hostKey]!.add(conn);
      _pool.add(conn);
    }
  }
}
