part of cassandra_scylla_cortex.connection;

class Connection {
  // Configuration options
  late PoolConfiguration _poolConfig;
  String? defaultKeyspace;
  String connId;
  String host;
  int? port;

  // Flag indicating whether the connected host is healthy or not
  bool healthy = false;

  // Flag indicating whether this connection can process client requests
  bool inService = false;

  // The connection to the server
  Socket? _socket;

  // A pool of frame writers for each multiplexed stream
  AsyncQueue<FrameWriter?>? _frameWriterPool;
  final Map<int, FrameWriter?> _reservedFrameWriters = <int, FrameWriter?>{};

  // Tracked futures/streams
  late Map<int, Completer<Message?>> _pendingResponses;
  Completer<void>? _connected;
  Completer? _drained;
  late Future _socketFlushed;
  bool _closing = false;

  final StreamController<Connection> onConnected =
      StreamController<Connection>.broadcast();
  final StreamController<Connection> onDisconnected =
      StreamController<Connection>.broadcast();
  final StreamController<Exception> onAuthenticate =
      StreamController<Exception>.broadcast();

  Connection(
    this.connId,
    this.host,
    this.port, {
    PoolConfiguration? config,
    this.defaultKeyspace,
  }) {
    // If no config is specified, use the default one
    _poolConfig = config ?? PoolConfiguration();

    // Initialize pending futures list
    _pendingResponses = <int, Completer<Message?>>{};
  }

  String get hostKey {
    return '$host:$port';
  }

  final StreamController<EventMessage> _eventController =
      StreamController<EventMessage>();

  /// Abort any pending requests with [reason] as the error and clean up.
  /// Returns a [Future] to be completed when the client socket has been successfully closed
  Future<void> _abortRequestsAndCleanup(reason) async {
    connectionLogger.fine('$connId abort and cleanup $reason');
    // Clear healthy flag
    healthy = false;
    onDisconnected.add(this);

    // Fail all pending requests and cleanup
    for (final completer in _pendingResponses.values) {
      connectionLogger.fine(
          '$host:$port Pending responses count ${_pendingResponses.length} $reason');
      if (!completer.isCompleted) {
        completer.completeError(reason as Object);
      }
    }
    _pendingResponses.clear();

    // Cleanup
    _connected = null;

    // Kill socket
    await _socket?.close();
    _socket = null;
  }

  /// Attempt to reconnect to the server. If the attempt fails, it will be retried after
  /// [reconnectWaitTime] ms up to [maxConnectionAttempts] times. If all connection attempts
  /// fail, then the [_connected] [Future] returned by a call to [open] will also fail
  Future<void> _reconnect() async {
    if (_connected != null) {
      return _connected!.future;
    }

    _connected = Completer<void>();
    final connFuture = _connected!.future;

    connectionLogger.info(
      '[$connId] Trying to connect to $host:$port [attempt x/${_poolConfig.maxConnectionAttempts}]',
    );

    try {
      final Socket s = await Socket.connect(host, port!);
      _socket = s;
      _socketFlushed = Future.value(true);

      // Initialize our writer pool and set the reservation timeout
      _reservedFrameWriters.clear();
      _frameWriterPool = AsyncQueue<FrameWriter?>.from(
        List<FrameWriter>.generate(
          _poolConfig.streamsPerConnection,
          (int id) => FrameWriter(
            id,
            _poolConfig.protocolVersion,
            preferBiggerTcpPackets: _poolConfig.preferBiggerTcpPackets,
          ),
        ),
      );
      _frameWriterPool!.reservationTimeout =
          _poolConfig.streamReservationTimeout;

      // Bind processors and initiate handshake
      _socket!
          .transform(FrameParser().transformer)
          .transform(FrameDecompressor(_poolConfig.compression).transformer)
          .transform(FrameReader().transformer)
          .listen(
        _onMessage
        // Mute socket errors; they will be caught by _writeMessage
        ,
        onError: (_) {},
        onDone: () async {
          connectionLogger.severe('[$connId] Server has closed the connection');
          if (_socket != null) {
            _socket?.destroy();
            _socket = null;
          }

          if (!_closing) {
            await _abortRequestsAndCleanup(
              ConnectionLostException('Server closed the connection'),
            );
            _reconnect();
          }
        },
      );

      // Handshake with the server
      _handshake();
    } catch (error, trace) {
      // Retry after reconnectWaitTime ms
      Timer(_poolConfig.reconnectWaitTime, _reconnect);
    }

    return connFuture;
  }

  Future<Message> _authenticate(AuthenticateMessage authMessage) {
    // Check if an authenticator is specified
    if (_poolConfig.authenticator == null) {
      final exception = AuthenticationException(
        "Server requested '${authMessage.authenticatorClass}' authenticator but no authenticator specified",
      );
      onAuthenticate.add(exception);
      throw exception;
    } else if (authMessage.authenticatorClass !=
        _poolConfig.authenticator!.authenticatorClass) {
      final exception = AuthenticationException(
        "Server requested '${authMessage.authenticatorClass}' authenticator but a '${_poolConfig.authenticator!.authenticatorClass}' authenticator was specified instead",
      );
      onAuthenticate.add(exception);
      throw exception;
    }

    // Run through challenge response till we get back a ready message from the server
    Future<Message> answerChallenge(Message? result) async {
      if (result is AuthenticateMessage || result is AuthChallengeMessage) {
        final AuthResponseMessage response = AuthResponseMessage()
          ..responsePayload = _poolConfig.authenticator!.answerChallenge(
            result is AuthenticateMessage
                ? result.challengePayload
                : (result! as AuthChallengeMessage).challengePayload,
          );

        try {
          return await answerChallenge(await _writeMessage(response));
        } on CassandraException catch (err, trace) {
          final exception = AuthenticationException(err.message, trace);
          onAuthenticate.add(exception);
          throw exception;
        } catch (err) {
          rethrow;
        }
      } else if (result is AuthSuccessMessage) {
        return result;
      } else {
        // This shouldn't be entered.
        throw CassandraException(
          'Unhandled message $result in answerChallenge',
        );
      }
    }

    return answerChallenge(authMessage);
  }

  /// Perform handshake, authenticate with the server and optionally select [defaultKeyspace]
  void _handshake() {
    final StartupMessage message = StartupMessage()
      ..cqlVersion = _poolConfig.cqlVersion
      ..compression = _poolConfig.compression;

    () async {
      try {
        final response = await _writeMessage(message);
        if (response is AuthenticateMessage) {
          await _authenticate(response);
        }
        // if default keyspace is specified, run a query here.
        // Note since the connection is not yet *open* we cannot invoke execute() here
        if (defaultKeyspace != null) {
          final Query query = Query('USE $defaultKeyspace');
          final QueryMessage message = QueryMessage()
            ..query = query.expandedQuery
            ..bindings = null
            ..consistency = query.consistency
            ..serialConsistency = query.serialConsistency;

          await _writeMessage(message);
        }

        connectionLogger.info(
          '[$connId] Connected [attempt x/${_poolConfig.maxConnectionAttempts}]',
        );

        healthy = true;
        inService = true;
        _drained = null;
        _connected?.complete();
        onConnected.add(this);
      } catch (err, trace) {
        connectionLogger.severe('Failed to connect to $connId', err, trace);
        healthy = false;
        inService = false;
        _drained = null;
        _connected?.completeError(err, trace);
        onDisconnected.add(this);
      }
    }();
  }

  /// Encode and send a [message] to the server. Returns a [Future] to be
  /// completed with the query results or with an error if one occurs
  Future<Message> _writeMessage(RequestMessage message) {
    final reply = Completer<Message>();

    _socketFlushed = () async {
      try {
        await _socketFlushed;
        final writer = await _frameWriterPool!.reserve();
        _reservedFrameWriters[writer!.getStreamId()] = writer;
        _pendingResponses[writer.getStreamId()] = reply;
        connectionLogger.fine(
          '[$connId] [stream #${writer.getStreamId()}] Sending message of type ${message.opcode?.name} (${message.opcode}) $message',
        );
        writer.writeMessage(
          message,
          _socket,
          compression: _poolConfig.compression,
        );
        await _socket?.flush();
      } on SocketException {
        // The below code is original. It was removed because it would async destroy
        // The new connect futures that get spawned on error. Do not know if there
        // Is an edge case to handle something here etc.
        // _abortRequestsAndCleanup(ConnectionLostException('Lost connection'));
      } catch (err) {
        reply.completeError(err);
      }
    }();

    return reply.future;
  }

  /// Handle an incoming server [message]
  void _onMessage(Message message) {
    connectionLogger.fine(
      '[$connId] [stream #${message.streamId}] Received message of type ${message.opcode?.name} (${message.opcode}) $message',
    );

    // Fetch our response completer
    final responseCompleter = _pendingResponses[message.streamId!];

    // Release streamId back to the pool unless its -1 (server event message)
    if (message.streamId != -1) {
      //connectionLogger.fine("[${connId}] Releasing writer for stream #${message.streamId}");

      if (responseCompleter != null) {
        _pendingResponses.remove(message.streamId);
        final FrameWriter? writer =
            _reservedFrameWriters.remove(message.streamId);
        _frameWriterPool!.release(writer);
      }

      _checkForDrainedRequests();
    }

    // If the frame-grabber caught an exception, report it through the completer
    if (message is ExceptionMessage) {
      if (responseCompleter != null) {
        responseCompleter.completeError(
          message.exception as Object,
          message.stackTrace,
        );
      }
      return;
    } else if (message.opcode != Opcode.event && responseCompleter == null) {
      // Connection has probably been aborted before we got to process this message; ignore
      return;
    }

    switch (message.opcode) {
      case Opcode.ready:
        responseCompleter!.complete(VoidResultMessage());
        break;
      case Opcode.authenticate:
      case Opcode.authChallenge:
      case Opcode.authSuccess:
        responseCompleter!.complete(message);
        break;
      case Opcode.result:
        if (message is PreparedResultMessage) {
          final PreparedResultMessage resMsg = message;

          // Fill in our host and port
          resMsg.host = host;
          resMsg.port = port;
        }

        responseCompleter!.complete(message);
        break;
      case Opcode.event:
        if (_eventController.hasListener && !_eventController.isPaused) {
          _eventController.add(message as EventMessage);
        }
        break;
      case Opcode.error:
        final ErrorMessage errorMessage = message as ErrorMessage;
        connectionLogger.severe(errorMessage.message);
        if (responseCompleter?.isCompleted == true) {
          connectionLogger
              .info('onMessage opCode error completer call twice blocked');
          break;
        }
        if (errorMessage.code == ErrorCode.unprepared) {
          responseCompleter!.completeError(UnpreparedQueryException());
        } else {
          responseCompleter!
              .completeError(CassandraException(errorMessage.message));
        }
        break;
      case Opcode.register:
      case Opcode.execute:
      case Opcode.options:
      case Opcode.prepare:
      case Opcode.supported:
      case Opcode.authResponse:
      case Opcode.startup:
      case Opcode.batch:
      case Opcode.query:
      case null:
        break;
    }
  }

  /// Check if we are draining active requests and no more
  /// pending requests are available
  void _checkForDrainedRequests() {
    if (_drained != null && _pendingResponses.isEmpty) {
      _drained!.complete(true);
      _socket?.close();
      _socket = null;
      _connected = null;
    }
  }

  /// Open a working connection to the server using [config.cqlVersion] and optionally select
  /// keyspace [defaultKeyspace]. Returns a [Future] to be completed on a successful protocol handshake
  Future<void> open() {
    void timeout() {
      throw ConnectionFailedException('Connection Failed');
    }

    _closing = false;

    return _reconnect()
        .timeout(_poolConfig.connectionTimeout, onTimeout: timeout);
  }

  /// Close the connection and set the [inService] flag to false so no new
  /// requests are sent to this connection. If the [drain] flag is set then the socket
  /// will be disconnected when all pending requests finish or when [drainTimeout] expires.
  /// Otherwise, the socket will be immediately disconnected and any pending requests will
  /// automatically fail.
  ///
  /// Once the connection is closed, any further invocations of close() will immediately
  /// succeed.
  ///
  /// This method returns a [Future] to be completed when the connection is shut down
  Future<void> close({
    bool drain = true,
    Duration drainTimeout = const Duration(seconds: 5),
  }) async {
    connectionLogger.info('[$connId] Closing (drain = $drain)');

    _closing = true;
    onDisconnected.add(this);

    // Already closed
    if (_socket == null) {
      return;
    }

    // Prevent clients from submitting new requests to us
    inService = false;

    if (drain) {
      inService = false;
      if (_drained == null) {
        _drained = Completer();
        _checkForDrainedRequests();

        Future.delayed(drainTimeout, () async {
          if (_drained != null && !_drained!.isCompleted) {
            await _abortRequestsAndCleanup(
              ConnectionLostException('Connection drain timeout'),
            );
            _drained?.complete();
          }
        });
      }

      return _drained!.future;
    } else {
      return _abortRequestsAndCleanup(
        ConnectionLostException('Connection closed'),
      );
    }
  }

  Future<PreparedResultMessage?> prepare(Query query) async {
    await open();

    // V3 version of the protocol does not support named placeholders. We need to convert them
    // to positional ones before preparing the statements
    final PrepareMessage message = PrepareMessage()
      ..query = _poolConfig.protocolVersion == ProtocolVersion.v3
          ? query.positionalQuery
          : query.query;

    return _cast<PreparedResultMessage>(await _writeMessage(message));
  }

  /// Execute a single prepared or unprepared [query]. In the case of a prepared query,
  /// the optional [preparedResult] argument needs to be specified. To page through
  /// queries, the [pageSize] and [pagingState] fields should be supplied for the initial and
  /// each consecutive invocation.
  ///
  /// This method will returns [ResultMessage] with the query result
  Future<ResultMessage?> execute(
    Query query, {
    PreparedResultMessage? preparedResult,
    int? pageSize,
    Uint8List? pagingState,
  }) async {
    await open();

    // Simple unprepared query
    if (preparedResult == null) {
      final QueryMessage message = QueryMessage()
        ..query = query.expandedQuery
        ..bindings = null
        ..consistency = query.consistency
        ..serialConsistency = query.serialConsistency
        ..resultPageSize = pageSize
        ..pagingState = pagingState;

      return _cast<ResultMessage>(await _writeMessage(message));
    } else {
      // Prepared query. V3 of the protocol does not support named bindings so we need to
      // map them to positional ones
      final ExecuteMessage message = ExecuteMessage()
        ..queryId = preparedResult.queryId
        ..bindings = _poolConfig.protocolVersion == ProtocolVersion.v3
            ? query.namedToPositionalBindings
            : query.bindings
        ..bindingTypes = preparedResult.metadata!.colSpec
        ..consistency = query.consistency
        ..serialConsistency = query.serialConsistency
        ..resultPageSize = pageSize
        ..pagingState = pagingState;

      return _cast<ResultMessage>(await _writeMessage(message));
    }
  }

  /// Execute the supplied batch [query]
  Future<VoidResultMessage?> executeBatch(BatchQuery query) async {
    await open();

    final BatchMessage message = BatchMessage()
      ..type = query.type
      ..consistency = query.consistency
      ..serialConsistency = query.serialConsistency
      ..queryList = query.queryList;

    return _cast<VoidResultMessage>(await _writeMessage(message));
  }

  /// Request server notifications for each [EventRegistrationType] in [eventTypes]
  /// and return a [Stream<EventMessage>] for handling incoming events
  Stream<EventMessage> listenForEvents(
    Iterable<EventRegistrationType> eventTypes,
  ) {
    final RegisterMessage message = RegisterMessage()
      ..eventTypes = eventTypes.toList();

    () async {
      try {
        await open();
        await _writeMessage(message);
      } catch (err) {
        _eventController.addError(err);
      }
    }();

    return _eventController.stream;
  }

  /// Check if the connection has available stream slots for multiplexing additional queries
  bool get hasAvailableStreams => _frameWriterPool?.hasAvailableSlots ?? false;

  T? _cast<T>(x) => x is T ? x : null;
}
