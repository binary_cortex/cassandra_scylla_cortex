part of cassandra_scylla_cortex.protocol;

class QueryMessage extends Message implements RequestMessage {
  late Consistency consistency;
  Consistency? serialConsistency;
  String? query;
  Object? bindings;
  int? resultPageSize;
  Uint8List? pagingState;

  QueryMessage() : super(Opcode.query);

  void _writeBindings(TypeEncoder encoder) {
    // Non-prepared query messages automatically expand their arguments inside the query
    // string so this is a NO-OP.
  }

  void _writeQueryParameters(TypeEncoder encoder) {
    final bool emptyBindings = (bindings == null) ||
        (bindings is Map && (bindings! as Map).isEmpty) ||
        (bindings is List && (bindings! as List).isEmpty);

    int flags = 0;
    if (!emptyBindings) {
      flags |= QueryFlagExt().encode(QueryFlag.theValues);
    }
    if (resultPageSize != null) {
      flags |= QueryFlagExt().encode(QueryFlag.pageSize);
    }
    if (pagingState != null) {
      flags |= QueryFlagExt().encode(QueryFlag.withPagingState);
    }
    if (serialConsistency != null) {
      flags |= QueryFlagExt().encode(QueryFlag.withSerialConsistency);
    }

    encoder
      ..writeUInt16(ConsistencyExt().encode(consistency))
      ..writeUint8(flags);

    if (!emptyBindings) {
      _writeBindings(encoder);
    }

    if (resultPageSize != null) {
      encoder.writeUInt32(resultPageSize!);
    }

    if (pagingState != null) {
      encoder.writeBytes(pagingState, SizeType.long);
    }
    if (serialConsistency != null) {
      encoder.writeUInt16(ConsistencyExt().encode(serialConsistency!));
    }
  }

  @override
  void write(TypeEncoder? encoder) {
    // Write query
    encoder!.writeString(query, SizeType.long);

    // Write query parameters
    _writeQueryParameters(encoder);
  }
}
