part of cassandra_scylla_cortex.protocol;

class RegisterMessage extends Message implements RequestMessage {
  List<EventRegistrationType>? eventTypes;

  RegisterMessage() : super(Opcode.register);

  @override
  void write(TypeEncoder? encoder) {
    encoder!.writeStringList(
      eventTypes?.map((et) => EventRegistrationTypeExt().encode(et)).toList(),
      SizeType.short,
    );
  }
}
