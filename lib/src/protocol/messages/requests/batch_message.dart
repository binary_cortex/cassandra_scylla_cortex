part of cassandra_scylla_cortex.protocol;

class BatchMessage extends Message implements RequestMessage {
  late BatchType type;
  late Consistency consistency;
  Consistency? serialConsistency;
  late List<Query> queryList;

  BatchMessage() : super(Opcode.batch);

  @override
  void write(TypeEncoder? encoder) {
    // Write batch type and number of queries
    encoder!.writeUint8(BatchTypeExt().encode(type));

    // V3 includes a flags byte
    if (encoder.protocolVersion == ProtocolVersion.v3) {
      encoder.writeUint8(serialConsistency != null ? 0x10 : 0x00);
    }

    encoder.writeUInt16(queryList.length);

    // Write each query
    for (final Query query in queryList) {
      // Not prepared
      encoder.writeUint8(0);

      // Write expanded query
      encoder.writeString(query.expandedQuery, SizeType.long);

      // As the query is expanded we have 0 args to supply
      encoder.writeUInt16(0);
    }

    // Write consistency level
    encoder.writeUInt16(ConsistencyExt().encode(consistency));

    // V3 includes serial_consistency
    if (serialConsistency != null) {
      encoder.writeUInt16(ConsistencyExt().encode(serialConsistency!));
    }
  }
}
