part of cassandra_scylla_cortex.protocol;

class ExecuteMessage extends QueryMessage implements RequestMessage {
  Uint8List? queryId;
  Map<String, TypeSpec>? bindingTypes;

  ExecuteMessage() : super() {
    opcode = Opcode.execute;
  }

  /// Write the bindings for the prepared statement using the
  /// binding type data we received when we prepared it as
  /// hints to the encoder
  @override
  void _writeBindings(TypeEncoder encoder) {
    if (bindings is Map<String, Object?>) {
      final Map<String, Object?> bindingsMap =
          bindings! as Map<String, Object?>;
      encoder.writeUInt16(bindingsMap.length);
      bindingsMap.forEach((String? arg, Object? value) {
        encoder.writeTypedValue(arg, value, typeSpec: bindingTypes![arg]);
      });
    } else {
      final Iterable<TypeSpec> bindingTypeList = bindingTypes!.values;
      final Iterable bindingsList = bindings! as Iterable;
      encoder.writeUInt16(bindingsList.length);

      int arg = 0;
      for (final value in bindingsList) {
        encoder.writeTypedValue(
          '$arg',
          value,
          typeSpec: bindingTypeList.elementAt(arg++),
        );
      }
    }
  }

  @override
  void write(TypeEncoder? encoder) {
    // Write queryId
    encoder!.writeBytes(queryId, SizeType.short);

    // Write query params
    _writeQueryParameters(encoder);
  }
}
