part of cassandra_scylla_cortex.protocol;

class PrepareMessage extends Message implements RequestMessage {
  String? query;

  PrepareMessage() : super(Opcode.prepare);

  @override
  void write(TypeEncoder? encoder) {
    // Send the query as a long string
    encoder!.writeString(query, SizeType.long);
  }
}
