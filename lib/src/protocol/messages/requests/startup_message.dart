part of cassandra_scylla_cortex.protocol;

class StartupMessage extends Message implements RequestMessage {
  String? cqlVersion;
  Compression? compression;

  StartupMessage() : super(Opcode.startup);

  @override
  void write(TypeEncoder? encoder) {
    // Write message contents
    final Map<String, String?> params = {'CQL_VERSION': cqlVersion};
    if (compression != null) {
      params['COMPRESSION'] = CompressionExt().encode(compression!);
    }

    encoder!.writeStringMap(params, SizeType.short);
  }
}
