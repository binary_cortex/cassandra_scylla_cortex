part of cassandra_scylla_cortex.protocol;

class AuthResponseMessage extends Message implements RequestMessage {
  Uint8List? responsePayload;

  AuthResponseMessage() : super(Opcode.authResponse);

  @override
  void write(TypeEncoder? encoder) {
    encoder!.writeBytes(responsePayload, SizeType.long);
  }
}
