part of cassandra_scylla_cortex.protocol;

/// The [ExceptionMessage] is used for reporting exceptions
/// caught by the [FrameReader]
class ExceptionMessage extends Message {
  dynamic exception;
  StackTrace? stackTrace;

  ExceptionMessage(this.exception, this.stackTrace) : super(Opcode.error);
}
