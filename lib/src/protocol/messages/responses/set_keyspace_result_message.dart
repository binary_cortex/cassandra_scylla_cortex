part of cassandra_scylla_cortex.protocol;

class SetKeyspaceResultMessage extends ResultMessage {
  String? keyspace;

  SetKeyspaceResultMessage.parse(TypeDecoder decoder) {
    keyspace = decoder.readString(SizeType.short);
  }
}
