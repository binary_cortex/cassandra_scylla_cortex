part of cassandra_scylla_cortex.protocol;

class AuthenticateMessage extends Message {
  String? authenticatorClass;

  AuthenticateMessage.parse(TypeDecoder decoder) : super(Opcode.authenticate) {
    authenticatorClass = decoder.readString(SizeType.short);
  }

  Uint8List? get challengePayload => null;
}
