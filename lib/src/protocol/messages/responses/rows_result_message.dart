part of cassandra_scylla_cortex.protocol;

class RowsResultMessage extends ResultMessage {
  RowsResultMessage.parse(TypeDecoder decoder) {
    // Parse metadata
    metadata = _parseMetadata(decoder);

    // Parse rows
    final int rowCount = decoder.readUInt();
    rows = List<Map<String, Object?>>.generate(rowCount, (int rowIndex) {
      final Map<String, Object?> row = {};
      metadata!.colSpec!.forEach((String colName, TypeSpec typeSpec) {
        row[colName] = decoder.readTypedValue(typeSpec, size: SizeType.long);
      });
      return row;
    });
  }
}
