part of cassandra_scylla_cortex.protocol;

abstract class ResultMessage extends Message {
  ResultMetadata? metadata;
  List<Map<String, Object?>> rows = <Map<String, Object?>>[];

  ResultMessage() : super(Opcode.result);

  factory ResultMessage.parse(TypeDecoder decoder) {
    // Read message type
    final ResultType type = ResultTypeExt().decode(decoder.readUInt());
    switch (type) {
      case ResultType.voidType:
        return VoidResultMessage();
      case ResultType.rows:
        //decoder.dumpToFile("frame-response-rows.dump");
        return RowsResultMessage.parse(decoder);
      case ResultType.setKeyspace:
        return SetKeyspaceResultMessage.parse(decoder);
      case ResultType.prepared:
        //decoder.dumpToFile("frame-response-prepared.dump");
        return PreparedResultMessage.parse(decoder);
      case ResultType.schemaChange:
        return SchemaChangeResultMessage.parse(decoder);
    }
  }

  ResultMetadata _parseMetadata(TypeDecoder decoder) {
    final ResultMetadata metadata = ResultMetadata();

    final int flags = metadata.flags = decoder.readUInt();
    final int tableSpecValue =
        RowResultFlagExt().encode(RowResultFlag.globalTableSpec);
    final int hasMorePagesValue =
        RowResultFlagExt().encode(RowResultFlag.hasMorePages);
    final bool globalTableSpec = (flags & tableSpecValue) == tableSpecValue;
    final bool hasMorePages = (flags & hasMorePagesValue) == hasMorePagesValue;
    //bool noMetadata = (flags & RowResultFlag.NO_METADATA.value) == RowResultFlag.NO_METADATA.value;
    final int colCount = decoder.readUInt();

    // Parse paging state
    if (hasMorePages) {
      metadata.pagingState = decoder.readBytes(SizeType.long);
    }

    // Skip over global table spec (<keyspace><table name>)
    if (globalTableSpec) {
      decoder.skipString(SizeType.short);
      decoder.skipString(SizeType.short);
    }

    // Parse column specs
    metadata.colSpec = <String, TypeSpec>{};
    for (int colIndex = colCount; colIndex > 0; colIndex--) {
      // Skip over col-specific table spec (<keyspace><table name>)
      if (!globalTableSpec) {
        decoder.skipString(SizeType.short);
        decoder.skipString(SizeType.short);
      }

      // Parse column name and type
      final String? columnName = decoder.readString(SizeType.short);
      if (columnName != null) {
        metadata.colSpec![columnName] = decoder.readTypeOption();
      }
    }

    return metadata;
  }
}
