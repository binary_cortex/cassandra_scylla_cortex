part of cassandra_scylla_cortex.protocol;

class PreparedResultMessage extends ResultMessage {
  Uint8List? queryId;

  // Extra data for looking up connections to the node that prepared this query
  String? host;
  int? port;

  PreparedResultMessage.parse(TypeDecoder decoder) {
    queryId = decoder.readBytes(SizeType.short);
    metadata = _parseMetadata(decoder);
  }
}
