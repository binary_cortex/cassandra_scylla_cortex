part of cassandra_scylla_cortex.protocol;

class EventMessage extends Message {
  EventRegistrationType? type;
  EventType? subType;

  // Filled in for topology/status messages
  InternetAddress? address;
  int? port;

  // Filled in for schema messages
  String? keyspace;
  String? changedTable;

  // V3-only
  String? changedType;

  EventMessage.parse(TypeDecoder decoder) : super(Opcode.event) {
    type =
        EventRegistrationTypeExt().decode(decoder.readString(SizeType.short));
    subType = EventTypeExt().decode(decoder.readString(SizeType.short));

    switch (type) {
      case EventRegistrationType.topologyChange:
      case EventRegistrationType.statusChange:
        address = decoder.readTypedValue(
          TypeSpec(DataType.inet),
          size: SizeType.byte,
        ) as InternetAddress?;
        port = decoder.readInt();
        break;
      case EventRegistrationType.schemaChange:
        switch (decoder.protocolVersion) {
          case ProtocolVersion.v2:
            keyspace = decoder.readString(SizeType.short);

            // According to the spec, this should be an empty string if only the keyspace changed
            final String? tableName = decoder.readString(SizeType.short);
            changedTable =
                tableName == null || tableName.isEmpty ? null : tableName;

            break;
          case ProtocolVersion.v3:
            final String? target = decoder.readString(SizeType.short);
            keyspace = decoder.readString(SizeType.short);

            switch (target) {
              case 'TABLE':
                changedTable = decoder.readString(SizeType.short);
                break;
              case 'TYPE':
                changedType = decoder.readString(SizeType.short);
                break;
            }
        }
        break;
    }
  }
}
