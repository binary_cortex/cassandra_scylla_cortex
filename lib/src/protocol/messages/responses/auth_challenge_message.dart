part of cassandra_scylla_cortex.protocol;

class AuthChallengeMessage extends Message {
  Uint8List? challengePayload;

  AuthChallengeMessage.parse(TypeDecoder decoder)
      : super(Opcode.authChallenge) {
    challengePayload = decoder.readBytes(SizeType.long);
  }
}
