part of cassandra_scylla_cortex.protocol;

class AuthSuccessMessage extends Message {
  Uint8List? token;

  AuthSuccessMessage.parse(TypeDecoder decoder) : super(Opcode.authSuccess) {
    token = decoder.readBytes(SizeType.long);
  }
}
