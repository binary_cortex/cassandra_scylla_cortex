part of cassandra_scylla_cortex.protocol;

class ResultMetadata {
  int? flags;
  Uint8List? pagingState;
  Map<String, TypeSpec>? colSpec;
}
