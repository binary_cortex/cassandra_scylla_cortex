part of cassandra_scylla_cortex.protocol;

class ErrorMessage extends Message {
  ErrorCode? code;

  String message = '';

  ErrorMessage.parse(TypeDecoder decoder) : super(Opcode.error) {
    // Parse message common
    code = ErrorCodeExt().decode(decoder.readUInt());
    message = decoder.readString(SizeType.short) ?? '';

    // Parse additional error-specific details
    // @todo Implement missing cases
    switch (code) {
      case ErrorCode.unavailable:
        final String consistency = decoder.readConsistency().name;
        final int required = decoder.readUInt();
        final int alive = decoder.readUInt();
        message +=
            'Not enough nodes available to answer the query with $consistency consistency. Required $required nodes but found $alive alive';
        break;
      case ErrorCode.writeTimeout:
        final String consistency = decoder.readConsistency().name;
        final int received = decoder.readUInt();
        final int required = decoder.readUInt();
        final String? writeType = decoder.readString(SizeType.short);
        message +=
            'Timeout during a write request with $consistency consistency. Received $received/$required ACK for a $writeType write';
        break;
      case ErrorCode.readTimeout:
        final String consistency = decoder.readConsistency().name;
        final int received = decoder.readUInt();
        final int required = decoder.readUInt();
        final bool dataPresent = decoder.readByte() != 0;
        message +=
            'Timeout during a read request with $consistency consistency. Received $received/$required responses. The replica asked for the data ${dataPresent ? 'HAS' : 'has NOT'} responded';
        break;
      case ErrorCode.alreadyExists:
        final String? keyspace = decoder.readString(SizeType.short);
        final String table = decoder.readString(SizeType.short)!;

        message += table.isEmpty
            ? 'Keyspace $keyspace already exists'
            : 'Table $table in keyspace $keyspace already exists';
        break;
      case ErrorCode.unprepared:
        final int id = decoder.readShort();
        message += 'Unknown prepared query with id $id';
        break;
    }
  }

  @override
  String toString() {
    return 'Error Message : $code : $message';
  }
}
