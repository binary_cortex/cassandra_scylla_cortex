part of cassandra_scylla_cortex.protocol;

class SchemaChangeResultMessage extends ResultMessage {
  String? change;
  String? keyspace;
  String? table;

  SchemaChangeResultMessage.parse(TypeDecoder decoder) {
    change = decoder.readString(SizeType.short);
    keyspace = decoder.readString(SizeType.short);
    table = decoder.readString(SizeType.short);
  }
}
