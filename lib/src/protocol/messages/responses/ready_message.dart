part of cassandra_scylla_cortex.protocol;

class ReadyMessage extends Message {
  ReadyMessage() : super(Opcode.ready);
}
