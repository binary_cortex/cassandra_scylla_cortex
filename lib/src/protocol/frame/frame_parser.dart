part of cassandra_scylla_cortex.protocol;

class FrameParser {
  final ChunkedInputReader _inputBuffer = ChunkedInputReader();
  FrameHeader? _parsedHeader;
  Uint8List? _bodyData;
  int _bodyWriteOffset = 0;
  int? _headerSizeInBytes;
  ProtocolVersion? _protocolVersion;

  void handleData(Uint8List? chunk, EventSink<Frame> sink) {
    try {
      // Append incoming chunk to input buffer
      if (chunk != null) {
        _inputBuffer.add(chunk);
      }

      // Are we extracting header bytes?
      if (_parsedHeader == null) {
        if (_headerSizeInBytes == null) {
          // Peek the first byte to figure out the version
          final int? version = _inputBuffer.peekNextByte();
          if (version == HeaderVersionExt().encode(HeaderVersion.responseV2) ||
              version == HeaderVersionExt().encode(HeaderVersion.requestV2)) {
            _headerSizeInBytes = FrameHeader.sizeInBytesV2;
            _protocolVersion = ProtocolVersion.v2;
          } else if (version ==
                  HeaderVersionExt().encode(HeaderVersion.responseV3) ||
              version == HeaderVersionExt().encode(HeaderVersion.requestV3)) {
            _headerSizeInBytes = FrameHeader.sizeInBytesV3;
            _protocolVersion = ProtocolVersion.v3;
          }
        }

        // Not enough bytes to parse header; wait till we get more
        if (_headerSizeInBytes == null ||
            _inputBuffer.length < _headerSizeInBytes!) {
          return;
        }

        // Extract header bytes and parse them
        Uint8List? headerBytes = Uint8List(_headerSizeInBytes!);
        _inputBuffer.read(headerBytes, _headerSizeInBytes!);
        //dump = File('${DateTime.now()}.dump');
        //dump.writeAsBytesSync(headerBytes);
        _parsedHeader = TypeDecoder.fromBuffer(
          ByteData.view(headerBytes.buffer),
          _protocolVersion,
        ).readHeader();
        headerBytes = null;

        if (_parsedHeader!.length! > FrameHeader.maxLengthInBytes) {
          throw DriverException(
            'Frame size cannot be larger than ${FrameHeader.maxLengthInBytes} bytes. Attempted to read ${_parsedHeader!.length} bytes',
          );
        }

        // Allocate buffer for body and reset write offset
        _bodyData = Uint8List(_parsedHeader!.length!);
        _bodyWriteOffset = 0;
      } else {
        // Copy pending body data
        _bodyWriteOffset += _inputBuffer.read(
          _bodyData,
          _parsedHeader!.length! - _bodyWriteOffset,
          _bodyWriteOffset,
        );
      }

      // If we are done emit the frame to the next pipeline stage and cleanup
      if (_bodyWriteOffset == _parsedHeader!.length) {
        // Ignore messages with unknown opcodes
        if (_parsedHeader!.opcode != null) {
          //dump.writeAsBytesSync(_bodyData, mode : FileMode.APPEND);
          sink.add(
            Frame.fromParts(_parsedHeader, ByteData.view(_bodyData!.buffer)),
          );
        } else {
          throw DriverException(
            'Unknown frame with opcode 0x${_parsedHeader!.unknownOpcodeValue.toRadixString(16)} and payload size 0x${_parsedHeader!.length}',
          );
        }
        _parsedHeader = null;
        _headerSizeInBytes = null;
        _protocolVersion = null;
        _bodyData = null;
      }

      // If we have not exhausted our data buffers, trigger this method
      // again to process the remaining data before resuming processing
      if (_inputBuffer.length > 0) {
        handleData(null, sink);
      }
    } catch (e, trace) {
      // / Emit an exception message
      final ExceptionMessage message = ExceptionMessage(
        e,
        e is DriverException && e.stackTrace != null ? e.stackTrace : trace,
      );
      message.streamId = _parsedHeader!.streamId;

      _parsedHeader = null;
      _headerSizeInBytes = null;
      _protocolVersion = null;
      _bodyData = null;

      sink.addError(message);
    }
  }

  void handleDone(EventSink<Frame> sink) {
    sink.close();
  }

  void handleError(Object error, StackTrace stackTrace, EventSink<Frame> sink) {
    sink.addError(error, stackTrace);
  }

  StreamTransformer<Uint8List, Frame> get transformer =>
      StreamTransformer<Uint8List, Frame>.fromHandlers(
        handleData: handleData,
        handleDone: handleDone,
        handleError: handleError,
      );
}
