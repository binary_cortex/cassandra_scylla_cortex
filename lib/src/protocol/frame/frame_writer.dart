part of cassandra_scylla_cortex.protocol;

class FrameWriter {
  bool preferBiggerTcpPackets;
  TypeEncoder? _typeEncoder;
  final FrameHeader _header = FrameHeader();

  FrameWriter(
    int streamId,
    ProtocolVersion protocolVersion, {
    TypeEncoder? withEncoder,
    this.preferBiggerTcpPackets = false,
  }) {
    _header
      ..version = protocolVersion == ProtocolVersion.v2
          ? HeaderVersion.requestV2
          : HeaderVersion.requestV3
      ..flags = 0
      ..streamId = streamId;

    _typeEncoder = withEncoder ?? TypeEncoder(protocolVersion);
  }

  int getStreamId() {
    return _header.streamId;
  }

  void writeMessage(
    RequestMessage message,
    Sink? targetSink, {
    Compression? compression,
  }) {
    // Buffer message so we can measure its length
    message.write(_typeEncoder);

    // If compression is enabled, compress payload before filling the header
    // According to the spec, compression does not apply to the initial STARTUP message
    _header.flags = 0;
    if (compression != null && message.opcode != Opcode.startup) {
      final Codec<Object, Uint8List?>? compressionCodec =
          getCodec(CompressionExt().encode(compression));
      if (compressionCodec == null) {
        throw DriverException(
          "A compression codec needs to be registered via registerCodec() for type '$compression'",
        );
      }

      // Concatenate all writer blocks into a single chunk and then pass it through the compression codec
      // Catch and wrap any codec exceptions
      Uint8List? compressedData;
      try {
        compressedData =
            compressionCodec.encode(_typeEncoder!.writer!.joinChunks());
      } catch (e, trace) {
        throw DriverException(
          "An error occurred while invoking '$compression' codec (compression): $e",
          trace,
        );
      }

      // Replace writer blocks with compressed output and enable the compression flag for the header
      _header.flags |= HeaderFlagExt().encode(HeaderFlag.compression);
      _typeEncoder!.writer!.clear();
      _typeEncoder!.writer!.addLast(compressedData);
    }

    // Check for max payload size
    if (_typeEncoder!.writer!.lengthInBytes > FrameHeader.maxLengthInBytes) {
      _typeEncoder!.writer!.clear();
      throw DriverException(
        'Frame size cannot be larger than ${FrameHeader.maxLengthInBytes} bytes. Attempted to write ${_typeEncoder!.writer!.lengthInBytes} bytes',
      );
    }

    // Allocate header buffer
    final Uint8List buf = Uint8List(
      _typeEncoder!.protocolVersion == ProtocolVersion.v2
          ? FrameHeader.sizeInBytesV2
          : FrameHeader.sizeInBytesV3,
    );
    final ByteData headerBytes = ByteData.view(buf.buffer);

    // Encode header
    int offset = 0;
    headerBytes
      ..setUint8(offset++, HeaderVersionExt().encode(_header.version!))
      ..setUint8(offset++, _header.flags);

    // Encode stream id (V2 uses a byte, V3 uses a short)
    if (_typeEncoder!.protocolVersion == ProtocolVersion.v2) {
      headerBytes.setInt8(offset++, _header.streamId);
    } else {
      headerBytes.setInt16(offset, _header.streamId);
      offset += 2;
    }

    // Encode remaining frame data
    headerBytes
      ..setUint8(offset++, OpcodeExt().encode(message.opcode!))
      ..setUint32(offset++, _typeEncoder!.writer!.lengthInBytes);

    // Prepend the header to the writer buffer queue
    _typeEncoder!.writer!.addFirst(buf);

    // Dump
    //_typeEncoder.dumpToFile("frame-out.dump");

    // Pipe everything to the sink
    _typeEncoder!.writer!
        .pipe(targetSink, preferBiggerTcpPackets: preferBiggerTcpPackets);
  }
}
