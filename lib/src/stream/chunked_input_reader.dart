part of cassandra_scylla_cortex.stream;

class ChunkedInputReader {
  final _bufferedChunks = ListQueue<List<int>>();
  int _usedHeadBytes = 0;

  /// Add a [chunk] to the buffer queue
  void add(Uint8List? chunk) {
    if (chunk != null) _bufferedChunks.add(chunk);
  }

  /// Clear buffer
  void clear() => _bufferedChunks.clear();

  /// Get the total available bytes in all chunk buffers (excluding bytes already de-queued from head buffer).
  int get length => _bufferedChunks.fold(
        -_usedHeadBytes,
        (int count, List<int> el) => count + el.length,
      );

  /// Return the next byte in the buffer without modifying the read pointer.
  /// Returns the [int] value of the next byte or null if no data is available
  int? peekNextByte() {
    // No data available
    if (_bufferedChunks.isEmpty || length < 1) {
      return null;
    }

    return _bufferedChunks.first[_usedHeadBytes];
  }

  /// Try to read [count] bytes into [destination] at the specified [offset].
  /// This method will automatically de-queue exhausted head buffers from the queue
  /// and will return the total number of bytes written
  int read(List<int?>? destination, int count, [int offset = 0]) {
    int writeOffset = offset;
    int current = count;
    while (current > 0) {
      // If we ran out of buffers we are done
      if (_bufferedChunks.isEmpty) {
        break;
      }

      // If we ran out of bytes to copy we are also done
      final int remainingHeadBytes =
          _bufferedChunks.first.length - _usedHeadBytes;
      if (remainingHeadBytes == 0) {
        break;
      }

      // If the remaining head buffer can fill the destination entirely, copy it and de-queue head
      if (remainingHeadBytes <= current) {
        destination!.setRange(
          writeOffset,
          writeOffset + remainingHeadBytes,
          _bufferedChunks.removeFirst(),
          _usedHeadBytes,
        );
        _usedHeadBytes = 0;
        current -= remainingHeadBytes;
        writeOffset += remainingHeadBytes;
      } else {
        // Copy as much as we can skipping any already dequeued bytes
        destination!.setRange(
          writeOffset,
          writeOffset + count,
          _bufferedChunks.first,
          _usedHeadBytes,
        );
        _usedHeadBytes += current;
        writeOffset += current;
        current = 0;
      }
    }

    return writeOffset - offset;
  }

  /// Skip [count] bytes from the input stream.
  /// This method will automatically de-queue exhausted head buffers from the queue
  /// and will return the total number of bytes skipped
  int skip(int count) {
    int current = count;
    int skippedBytesCount = 0;
    while (current > 0) {
      // If we ran out of buffers we are done
      if (_bufferedChunks.isEmpty) {
        break;
      }

      // If we ran out of bytes to copy we are also done
      final int remainingHeadBytes =
          _bufferedChunks.first.length - _usedHeadBytes;
      if (remainingHeadBytes == 0) {
        break;
      }

      // If the remaining head buffer can fill the destination entirely, de-queue head
      if (remainingHeadBytes <= current) {
        _bufferedChunks.removeFirst();
        _usedHeadBytes = 0;
        current -= remainingHeadBytes;
        skippedBytesCount += remainingHeadBytes;
      } else {
        // Just update the used bytes counter for the bytes we can copy
        _usedHeadBytes += current;
        skippedBytesCount += current;
        current = 0;
      }
    }

    return skippedBytesCount;
  }
}
