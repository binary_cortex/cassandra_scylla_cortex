part of cassandra_scylla_cortex.exception;

class CassandraException extends DriverException {
  CassandraException(String? message, [StackTrace? stackTrace])
      : super(message, stackTrace);

  @override
  String toString() {
    return 'CassandraException: $message';
  }
}
