part of cassandra_scylla_cortex.exception;

class DriverException implements Exception {
  String? message;
  StackTrace? stackTrace;

  DriverException(this.message, [this.stackTrace]);

  @override
  String toString() {
    return 'DriverException: $message';
  }
}
