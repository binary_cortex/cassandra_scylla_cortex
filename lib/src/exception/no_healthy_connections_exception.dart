part of cassandra_scylla_cortex.exception;

class NoHealthyConnectionsException extends DriverException {
  NoHealthyConnectionsException(String message, [StackTrace? stackTrace])
      : super(message, stackTrace);

  @override
  String toString() {
    return 'NoHealthyConnectionsException: $message';
  }
}
