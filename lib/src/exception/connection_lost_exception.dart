part of cassandra_scylla_cortex.exception;

class ConnectionLostException extends DriverException {
  ConnectionLostException(String message, [StackTrace? stackTrace])
      : super(message, stackTrace);
}
