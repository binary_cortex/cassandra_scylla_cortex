part of cassandra_scylla_cortex.exception;

class ConnectionFailedException extends DriverException {
  ConnectionFailedException(String message, [StackTrace? stackTrace])
      : super(message, stackTrace);

  @override
  String toString() {
    return 'ConnectionFailedException: $message';
  }
}
