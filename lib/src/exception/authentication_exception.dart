part of cassandra_scylla_cortex.exception;

class AuthenticationException extends DriverException {
  AuthenticationException(String? message, [StackTrace? stackTrace])
      : super(message, stackTrace);

  @override
  String toString() {
    return 'AuthenticationException: $message';
  }
}
