part of cassandra_scylla_cortex.exception;

class StreamReservationException extends DriverException {
  StreamReservationException(String message, [StackTrace? stackTrace])
      : super(message, stackTrace);

  @override
  String toString() {
    return 'StreamReservationException: $message';
  }
}
