part of cassandra_scylla_cortex.exception;

class UnknownErrorMessageException extends DriverException {
  UnknownErrorMessageException(ErrorMessage msg, [StackTrace? stackTrace])
      : super(msg.message, stackTrace);

  @override
  String toString() {
    return 'UnknownErrorMessageException: $message';
  }
}

class UnpreparedQueryException extends DriverException {
  UnpreparedQueryException([StackTrace? stackTrace])
      : super('Unprepared Query', stackTrace);

  @override
  String toString() {
    return 'UnpreparedQueryException: $message';
  }
}
