part of cassandra_scylla_cortex.types;

class Frame {
  final FrameHeader? header;
  final ByteData body;

  const Frame.fromParts(this.header, this.body);

  ProtocolVersion? getProtocolVersion() {
    if (header == null) {
      return null;
    }

    if (header!.version == HeaderVersion.requestV2 ||
        header!.version == HeaderVersion.responseV2) {
      return ProtocolVersion.v2;
    } else if (header!.version == HeaderVersion.requestV3 ||
        header!.version == HeaderVersion.responseV3) {
      return ProtocolVersion.v3;
    }

    return null;
  }
}
