part of cassandra_scylla_cortex.types;

class FrameHeader {
  static const int sizeInBytesV2 = 8;
  static const int sizeInBytesV3 = 9;
  static const int maxLengthInBytes = 268435456; // 256MB (see spec)

  HeaderVersion? version;
  int flags = 0;
  int streamId = 0;
  Opcode? opcode;
  int? length;

  // If the message contains an unknown opcode that cannot be parsed
  // as an Opcode enum, we store it here for logging
  late int unknownOpcodeValue;
}
