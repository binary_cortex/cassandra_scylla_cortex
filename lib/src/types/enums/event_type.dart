part of cassandra_scylla_cortex.types;

enum EventType {
  nodeAdded,
  nodeRemoved,
  nodeUp,
  nodeDown,
  schemaCreated,
  schemaUpdated,
  schemaDropped,
}

class EventTypeExt extends EnumMap<EventType, String> {
  static final EventTypeExt _singleton = EventTypeExt._internal();

  factory EventTypeExt() {
    return _singleton;
  }

  EventTypeExt._internal()
      : super({
          EventType.nodeAdded: 'NEW_NODE',
          EventType.nodeRemoved: 'REMOVED_NODE',
          EventType.nodeUp: 'UP',
          EventType.nodeDown: 'DOWN',
          EventType.schemaCreated: 'CREATED',
          EventType.schemaUpdated: 'UPDATED',
          EventType.schemaDropped: 'DROPPED',
        });
}
