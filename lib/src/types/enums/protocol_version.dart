part of cassandra_scylla_cortex.types;

enum ProtocolVersion {
  v2,
  v3,
}

class ProtocolVersionExt extends EnumMap<ProtocolVersion, int> {
  static final ProtocolVersionExt _singleton = ProtocolVersionExt._internal();

  factory ProtocolVersionExt() {
    return _singleton;
  }

  ProtocolVersionExt._internal()
      : super({
          ProtocolVersion.v2: 0x02,
          ProtocolVersion.v3: 0x03,
        });
}
