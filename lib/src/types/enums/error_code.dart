part of cassandra_scylla_cortex.types;

enum ErrorCode {
  serverError,
  protocolError,
  badCredentials,
  unavailable,
  overloaded,
  isBootstrapping,
  truncateError,
  writeTimeout,
  readTimeout,
  syntaxError,
  unauthorized,
  invalid,
  configError,
  alreadyExists,
  unprepared,
}

class ErrorCodeExt extends EnumMap<ErrorCode, int> {
  static final ErrorCodeExt _singleton = ErrorCodeExt._internal();

  factory ErrorCodeExt() {
    return _singleton;
  }

  ErrorCodeExt._internal()
      : super({
          ErrorCode.serverError: 0x0000,
          ErrorCode.protocolError: 0x000A,
          ErrorCode.badCredentials: 0x0100,
          ErrorCode.unavailable: 0x1000,
          ErrorCode.overloaded: 0x1001,
          ErrorCode.isBootstrapping: 0x1002,
          ErrorCode.truncateError: 0x1003,
          ErrorCode.writeTimeout: 0x1100,
          ErrorCode.readTimeout: 0x1200,
          ErrorCode.syntaxError: 0x2000,
          ErrorCode.unauthorized: 0x2100,
          ErrorCode.invalid: 0x2200,
          ErrorCode.configError: 0x2300,
          ErrorCode.alreadyExists: 0x2400,
          ErrorCode.unprepared: 0x2500,
        });
}
