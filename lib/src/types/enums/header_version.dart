part of cassandra_scylla_cortex.types;

enum HeaderVersion {
  requestV2,
  requestV3,
  responseV2,
  responseV3,
}

class HeaderVersionExt extends EnumMap<HeaderVersion, int> {
  static final HeaderVersionExt _singleton = HeaderVersionExt._internal();

  factory HeaderVersionExt() {
    return _singleton;
  }

  HeaderVersionExt._internal()
      : super({
          HeaderVersion.requestV2: 0x02,
          HeaderVersion.requestV3: 0x03,
          HeaderVersion.responseV2: 0x82,
          HeaderVersion.responseV3: 0x83,
        });
}
