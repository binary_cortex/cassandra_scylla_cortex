part of cassandra_scylla_cortex.types;

enum QueryFlag {
  theValues,
  skipMetadata,
  pageSize,
  withPagingState,
  withSerialConsistency,
}

class QueryFlagExt extends EnumMap<QueryFlag, int> {
  static final QueryFlagExt _singleton = QueryFlagExt._internal();

  factory QueryFlagExt() {
    return _singleton;
  }

  QueryFlagExt._internal()
      : super({
          QueryFlag.theValues: 0x01,
          QueryFlag.skipMetadata: 0x02,
          QueryFlag.pageSize: 0x04,
          QueryFlag.withPagingState: 0x08,
          QueryFlag.withSerialConsistency: 0x10,
        });
}
