part of cassandra_scylla_cortex.types;

enum BatchType {
  logged,
  unlogged,
  counter,
}

class BatchTypeExt extends EnumMap<BatchType, int> {
  static final BatchTypeExt _singleton = BatchTypeExt._internal();

  factory BatchTypeExt() {
    return _singleton;
  }

  BatchTypeExt._internal()
      : super({
          BatchType.logged: 0x01,
          BatchType.unlogged: 0x02,
          BatchType.counter: 0x03,
        });
}
