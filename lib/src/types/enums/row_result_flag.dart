part of cassandra_scylla_cortex.types;

enum RowResultFlag {
  globalTableSpec,
  hasMorePages,
  noMetadata,
}

class RowResultFlagExt extends EnumMap<RowResultFlag, int> {
  static final RowResultFlagExt _singleton = RowResultFlagExt._internal();

  factory RowResultFlagExt() {
    return _singleton;
  }

  RowResultFlagExt._internal()
      : super({
          RowResultFlag.globalTableSpec: 0x01,
          RowResultFlag.hasMorePages: 0x02,
          RowResultFlag.noMetadata: 0x04,
        });
}
