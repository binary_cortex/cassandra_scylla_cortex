part of cassandra_scylla_cortex.types;

enum HeaderFlag {
  compression,
  tracing,
}

class HeaderFlagExt extends EnumMap<HeaderFlag, int> {
  static final HeaderFlagExt _singleton = HeaderFlagExt._internal();

  factory HeaderFlagExt() {
    return _singleton;
  }

  HeaderFlagExt._internal()
      : super({
          HeaderFlag.compression: 0x01,
          HeaderFlag.tracing: 0x02,
        });
}
