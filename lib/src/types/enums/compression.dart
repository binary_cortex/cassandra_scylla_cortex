part of cassandra_scylla_cortex.types;

enum Compression {
  lz4,
  snappy,
}

class CompressionExt extends EnumMap<Compression, String> {
  static final CompressionExt _singleton = CompressionExt._internal();

  factory CompressionExt() {
    return _singleton;
  }

  CompressionExt._internal()
      : super({
          Compression.lz4: 'LZ4',
          Compression.snappy: 'SNAPPY',
        });
}
