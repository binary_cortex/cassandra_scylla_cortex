part of cassandra_scylla_cortex.types;

enum Opcode {
  error,
  startup,
  ready,
  authenticate,
  options,
  supported,
  query,
  result,
  prepare,
  execute,
  register,
  event,
  batch,
  authChallenge,
  authResponse,
  authSuccess,
}

class OpcodeExt extends EnumMap<Opcode, int> {
  static final OpcodeExt _singleton = OpcodeExt._internal();

  factory OpcodeExt() {
    return _singleton;
  }

  OpcodeExt._internal()
      : super({
          Opcode.error: 0x00,
          Opcode.startup: 0x01,
          Opcode.ready: 0x02,
          Opcode.authenticate: 0x03,
          Opcode.options: 0x05,
          Opcode.supported: 0x06,
          Opcode.query: 0x07,
          Opcode.result: 0x08,
          Opcode.prepare: 0x09,
          Opcode.execute: 0x0A,
          Opcode.register: 0x0B,
          Opcode.event: 0x0C,
          Opcode.batch: 0x0D,
          Opcode.authChallenge: 0x0E,
          Opcode.authResponse: 0x0F,
          Opcode.authSuccess: 0x10,
        });
}
