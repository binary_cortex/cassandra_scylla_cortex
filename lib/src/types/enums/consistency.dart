part of cassandra_scylla_cortex.types;

enum Consistency {
  any,
  one,
  two,
  three,
  quorum,
  all,
  localQuorum,
  eachQuorum,
  serial,
  localSerial,
  localOne,
}

class ConsistencyExt extends EnumMap<Consistency, int> {
  static final ConsistencyExt _singleton = ConsistencyExt._internal();

  factory ConsistencyExt() {
    return _singleton;
  }

  ConsistencyExt._internal()
      : super({
          Consistency.any: 0x00,
          Consistency.one: 0x01,
          Consistency.two: 0x02,
          Consistency.three: 0x03,
          Consistency.quorum: 0x04,
          Consistency.all: 0x05,
          Consistency.localQuorum: 0x06,
          Consistency.eachQuorum: 0x07,
          Consistency.serial: 0x08,
          Consistency.localSerial: 0x09,
          Consistency.localOne: 0x0A,
        });
}
