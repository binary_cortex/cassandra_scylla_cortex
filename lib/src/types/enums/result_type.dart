part of cassandra_scylla_cortex.types;

enum ResultType {
  voidType,
  rows,
  setKeyspace,
  prepared,
  schemaChange,
}

class ResultTypeExt extends EnumMap<ResultType, int> {
  static final ResultTypeExt _singleton = ResultTypeExt._internal();

  factory ResultTypeExt() {
    return _singleton;
  }

  ResultTypeExt._internal()
      : super({
          ResultType.voidType: 0x01,
          ResultType.rows: 0x02,
          ResultType.setKeyspace: 0x03,
          ResultType.prepared: 0x04,
          ResultType.schemaChange: 0x05,
        });
}
