part of cassandra_scylla_cortex.types;

Map<V, K> invertMap<K, V>(Map<K, V> map) {
  return map.map((key, value) => MapEntry<V, K>(value, key));
}

class EnumMap<T, K> {
  final Map<T, K> _mainMap;
  final Map<K, T> _inverseMap;

  EnumMap(this._mainMap) : _inverseMap = invertMap(_mainMap);

  K encode(T value) {
    if (_mainMap.containsKey(value) && _mainMap[value] != null) {
      return _mainMap[value]!;
    } else {
      throw ArgumentError('Invalid $T $value');
    }
  }

  T decode(K? value) {
    if (value != null &&
        _inverseMap.containsKey(value) &&
        _inverseMap[value] != null) {
      return _inverseMap[value]!;
    } else {
      throw ArgumentError('Invalid $T from bytes $value');
    }
  }
}
