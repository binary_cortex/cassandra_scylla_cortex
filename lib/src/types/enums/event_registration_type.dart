part of cassandra_scylla_cortex.types;

enum EventRegistrationType {
  topologyChange,
  statusChange,
  schemaChange,
}

class EventRegistrationTypeExt extends EnumMap<EventRegistrationType, String> {
  static final EventRegistrationTypeExt _singleton =
      EventRegistrationTypeExt._internal();

  factory EventRegistrationTypeExt() {
    return _singleton;
  }

  EventRegistrationTypeExt._internal()
      : super({
          EventRegistrationType.topologyChange: 'TOPOLOGY_CHANGE',
          EventRegistrationType.statusChange: 'STATUS_CHANGE',
          EventRegistrationType.schemaChange: 'SCHEMA_CHANGE',
        });
}
