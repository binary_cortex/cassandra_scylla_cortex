part of cassandra_scylla_cortex.types;

// Helpers
_uuid_impl.Uuid _uuidFactory = const _uuid_impl.Uuid();

class Uuid {
  String? value;

  Uuid([this.value]);

  /// Construct a [Uuid] from a the input [bytes] list
  Uuid.fromBytes(List<int> bytes) : value = _uuid_impl.Uuid.unparse(bytes);

  /// Create a V4 (randomised) uuid
  factory Uuid.simple() {
    return Uuid(_uuidFactory.v4());
  }

  /// Create a V1 (time-based) uuid
  factory Uuid.timeBased() {
    return Uuid(_uuidFactory.v1());
  }

  @override
  String toString() {
    return value!;
  }

  @override
  bool operator ==(Object other) {
    if (other is! Uuid) return false;
    return value == other.value;
  }

  @override
  int get hashCode {
    return value.hashCode;
  }

  /// Convert uuid to a [Uint8List] byte list
  Uint8List get bytes => Uint8List.fromList(_uuid_impl.Uuid.parse(value!));
}
