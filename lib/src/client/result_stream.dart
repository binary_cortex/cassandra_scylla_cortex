part of cassandra_scylla_cortex.client;

typedef PagedQueryExecutor = Future<ResultMessage?> Function(
  Query query, {
  int? pageSize,
  Uint8List? pagingState,
});

class ResultStream {
  final PagedQueryExecutor _queryExecutor;
  late StreamController<Map<String, Object?>> _streamController;
  Uint8List? _pagingState;
  Queue<Map<String, Object?>>? _bufferedData;
  final Query _query;
  final int _pageSize;
  bool _buffering = false;

  void _bufferNextPage() {
    if (_buffering) {
      return;
    }
    _buffering = true;

    () async {
      try {
        final ResultMessage? odata = await _queryExecutor(
          _query,
          pageSize: _pageSize,
          pagingState: _pagingState,
        );
        final RowsResultMessage data = odata! as RowsResultMessage;
        // If the stream has been closed, clean up
        if (_streamController.isClosed) {
          return;
        }

        _buffering = false;

        // Append incoming rows to current result list and update our paging state
        _bufferedData = Queue.from(data.rows);
        data.rows = [];
        _pagingState = data.metadata!.pagingState;

        _emitRows();
      } on NoHealthyConnectionsException catch (err) {
        _streamController.addError(err);
      } catch (err) {
        _buffering = false;
        _bufferNextPage();
      }
    }();
  }

  void _emitRows() {
    // If stream is paused, do not emit any events
    if (_streamController.isPaused) {
      return;
    }

    // Emit each available row
    while (_bufferedData != null && _bufferedData!.isNotEmpty) {
      final Map<String, Object?> row = _bufferedData!.removeFirst();
      _streamController.add(row);

      // if after adding the row, we detect that the stream is paused or closed, stop streaming
      if (_streamController.isClosed || _streamController.isPaused) {
        break;
      }
    }

    // If our stream is active and we emitted all page rows, fetch the next row
    // or close the stream if we are done
    if (!_streamController.isClosed &&
        !_streamController.isPaused &&
        _bufferedData!.isEmpty) {
      if (_pagingState == null) {
        _streamController.close();
      } else {
        _bufferNextPage();
      }
    }
  }

  void _cleanup() {
    _bufferedData = null;
    _pagingState = null;
  }

  Stream<Map<String, Object?>> get stream => _streamController.stream;

  /// Create a new [ResultStream] by paging through [this._query] object with a page size of [this._pageSize].
  ResultStream(
    this._queryExecutor,
    this._query,
    this._pageSize,
  ) {
    _streamController = StreamController<Map<String, Object?>>(
      onListen: _bufferNextPage,
      onResume: _emitRows,
      onCancel: _cleanup,
      sync: true,
    );
  }
}
