part of cassandra_scylla_cortex.logger;

// Define hierarchical loggers
final Logger poolLogger = Logger('cassandra_scylla_cortex.ConnectionPool');
final Logger connectionLogger = Logger('cassandra_scylla_cortex.Connection');
final Logger clientLogger = Logger('cassandra_scylla_cortex.Client');
